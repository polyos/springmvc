package com.mam.mvc.utils;


import com.mam.mvc.model.AdminUser;

public class UserHolder {

    public static final String ADMIN_USER_SESSION_KEY = "_ADMIN_USER_SESSION_KEY";

    public static final String THEATER_USER_SESSION_KEY = "THEATER_USER_SESSION_KEY_";

    private static ThreadLocal<AdminUser> tl = new ThreadLocal<AdminUser>();

    public static AdminUser getAdminUser() {
        return tl.get();
    }

    public static void clear() {
        tl.remove();
    }

    public static void setAdminUser(AdminUser User) {
        tl.set(User);
    }


    /**
     * description: 获取当前用户类型
     *
     * @return 1：中心账号，2：剧场账号
     * @author don
     * @date 2016年8月30日 上午10:30:59
     */
    public static int getUserType() {
        if (getAdminUser() == null) {
            return 2;
        } else {
            return 1;
        }
    }
}
