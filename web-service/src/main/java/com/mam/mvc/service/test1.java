package com.mam.mvc.service;

import com.mam.mvc.service.HelloService;
import com.mam.mvc.service.impl.HelloServiceImpl;
import com.mam.mvc.service.impl.MessageServiceImpl;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * Created by Meng.ma on 5/18/2017.
 */


public class test1 {

	public static void main(String[] args) {
		/**
		 *DefaultListableBeanFactory
         * xmlBeanFactory 不建议用了
		 */
        ClassPathResource resource = new ClassPathResource("beans.xml");
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
        reader.loadBeanDefinitions(resource);

        HelloService person = (HelloService)factory.getBean("person");
       person.hello();


        /**
		 *
		 */
		//用配置文件来启动一个 ApplicationContext
		/*ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/spring-mvc.xml");
		System.out.println(" start ...");
		MessageServiceImpl bean = applicationContext.getBean(MessageServiceImpl.class);
		bean.sayHello();*/
	}
}
