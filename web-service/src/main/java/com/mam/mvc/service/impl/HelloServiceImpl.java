package com.mam.mvc.service.impl;

import com.mam.mvc.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * Created by Meng.ma on 5/18/2017.
 */

@Service
public class HelloServiceImpl implements HelloService {

    private String name;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void hello() {
        System.out.println(" hello====");
    }
}
