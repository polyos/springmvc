package com.mam.mvc.service;

/**
 * Created by Meng.ma on 5/18/2017.
 */
public interface HelloService {

    public void setName(String name);

    public void hello();
}
