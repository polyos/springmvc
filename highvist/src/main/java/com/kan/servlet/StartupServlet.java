package com.kan.servlet;

import com.kan.common.SpringContext;
import com.kan.common.mybatis.EntityScan;
import com.kan.common.utils.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class StartupServlet extends DispatcherServlet {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected WebApplicationContext initWebApplicationContext() {
        WebApplicationContext ac = super.initWebApplicationContext();

        SpringContext.setApplicationContext(ac);

        //扫描Entity类
        EntityScan entityScan = ac.getBean(EntityScan.class);
        if(entityScan != null){
            entityScan.scan();
            //项目定制，强行修改mybatis配置
            entityScan.mybatisCustomizedInit();
        }

        Configuration.init("classpath*:config/highvist-*.properties");

        return ac;
    }

}
