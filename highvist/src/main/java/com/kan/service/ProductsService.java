package com.kan.service;

import com.kan.model.Category;
import com.kan.model.Products;
import com.kan.show.vo.CategoryVO;
import com.kan.show.vo.ProductsVO;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/9 17:11
 * @Description:
 */
public interface ProductsService {

	/**
	 * 查询单个设备详情
	 * @param productId
	 * @return
	 */
	ProductsVO findProductsVOByProductId(String productId);

	/**
	 * 通过ID 查询分类
	 * @param categoryId
	 * @return
	 */
	Category findCategoryById(String categoryId);

	/**
	 * 查询 设备分类
	 * @return
	 */
	List<Category> findCategoryList();

	/**
	 * 查询 所有设备
	 * @return
	 */
	//List<ProductsVO> findProductsList();

	List<Products> findProducts();

	/**
	 * 查询 分类和 其下的设备
	 * @return
	 */
	List<CategoryVO> findCategoryAndProductsList(List<Category> categoryList);

	/**
	 * 查询分类下的设备
	 * @param categoryId
	 * @return
	 */
	List<Products> findProductsListByCategoryId(String categoryId);
}
