package com.kan.service.Impl;

import com.kan.common.mybatis.WhereBuilder;
import com.kan.common.rest.RestClient;
import com.kan.model.*;
import com.kan.persistence.*;
import com.kan.service.ProductsService;
import com.kan.show.vo.CategoryVO;
import com.kan.show.vo.ProductsVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mameng
 * @Date 2019/3/9 17:12
 * @Description:
 */
@Service
public class ProductsServiceImpl implements ProductsService {

	@Autowired
	private RestClient restClient;
	@Autowired
	private ProductsMapper productsMapper;
	@Autowired
	private ProductsInfoMapper productsInfoMapper;
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private UploadFileMapper uploadFileMapper;
	@Autowired
	private GalleryMapper galleryMapper;

	@Override
	public ProductsVO findProductsVOByProductId(String productId) {
		ProductsVO vo = new ProductsVO();
		Products product = findProductByProductId(productId);
		if (product != null){
			BeanUtils.copyProperties(product,vo);

			List<ProductsInfo> productsInfoByProductId = findProductsInfoByProductId(productId);
			if (null != productsInfoByProductId && productsInfoByProductId.size() > 0){
				for (ProductsInfo info: productsInfoByProductId){
					if (info.getInfoType() ==1){
						vo.setBenefit(info.getDescription());
					}else if (info.getInfoType() ==2){
						vo.setTechnicalData(info.getDescription());
					}else {
						vo.setOverview(info.getDescription());
					}
				}
			}

			//gallery
			List<Gallery> galleryListByProductId = getGalleryListByProductId(productId);
			vo.setGalleryList(galleryListByProductId);
		}
		return vo;
	}

	private List<Gallery> getGalleryListByProductId(String productId){
		Gallery gallery = new Gallery();
		gallery.setProductId(productId);
		return galleryMapper.list(gallery, WhereBuilder.builder().and("productId").build(), null, null, null, null, Gallery.class);
	}

	private Products findProductByProductId(String productId){
		Map<String,String> productIdMap = new HashMap<>();
		productIdMap.put("productId",productId);
		Products products = productsMapper.getBy(productIdMap, WhereBuilder.builder().and("productId").build(), Products.class);
		/*if (products != null && products.getImgId() != null){
			products.setProductImg(findImgOfProducts(products.getImgId()));
		}*/
		return products;
	}

	private String findImgOfProducts(Integer imgId){
		UploadFile uploadFile = uploadFileMapper.get(imgId, UploadFile.class);
		if (uploadFile != null){
			return  uploadFile.getPath();
		}
		return "";
	}

	private List<ProductsInfo> findProductsInfoByProductId(String productId){
		Map<String,String> productIdMap = new HashMap<>();
		productIdMap.put("productId",productId);
		return productsInfoMapper.list(productIdMap,WhereBuilder.builder().and("productId").build(),
				null,null,null,null,ProductsInfo.class);
	}

	@Override
	public Category findCategoryById(String categoryId) {
		Category category = new Category();
		category.setCategoryId(categoryId);
		return categoryMapper.getBy(category, WhereBuilder.builder().and("categoryId").build(),Category.class);
	}

	@Override
	public List<Category> findCategoryList() {
		return categoryMapper.getCategoryList("",null,null);
	}

	@Override
	public List<Products> findProducts() {
		List<Products> productsList = productsMapper.getProductsList(null, null, null);
		/*if (productsList != null && productsList.size() > 0){
			for (Products p : productsList){
				if (StringUtils.isBlank(p.getProductImg())){
					if (p.getImgId() != null){
						String img = findImgOfProducts(p.getImgId());
						p.setProductImg(img);
					}
				}
			}
		}*/
		return productsList;
	}

	@Override
	public List<CategoryVO> findCategoryAndProductsList(List<Category> categoryList) {
		List<CategoryVO> categoryVOList = new ArrayList<>();
		if (null == categoryList){
			categoryList = findCategoryList();
		}
		List<Products> productsList = findProducts();
		Map<String,List<Products>> categoryIdAndProductsMap = new HashMap<>();
		if (null != categoryList && categoryList.size() > 0){
			if (null != productsList){
				for (Products vo : productsList){
					if (categoryIdAndProductsMap.containsKey(vo.getCategoryId())){
						List<Products> productsVOList = categoryIdAndProductsMap.get(vo.getCategoryId());
						productsVOList.add(vo);
					}else {
						List<Products> productsVOList = new ArrayList<>();
						productsVOList.add(vo);
						categoryIdAndProductsMap.put(vo.getCategoryId(),productsVOList);
					}
				}
			}
			for (Category category : categoryList){
				CategoryVO categoryVO = new CategoryVO();
				categoryVO.setCategoryId(category.getCategoryId());
				categoryVO.setCategoryName(category.getCategoryName());
				categoryVO.setProductsList(categoryIdAndProductsMap.get(category.getCategoryId()));
				categoryVOList.add(categoryVO);
			}
		}
		return categoryVOList;
	}

	@Override
	public List<Products> findProductsListByCategoryId(String categoryId) {
		return productsMapper.getProductsList(categoryId,null,null);
	}
}
