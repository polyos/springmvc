package com.kan.persistence;

import com.kan.common.mybatis.BaseMapper;
import com.kan.model.ProductsInfo;

/**
 * @author mameng
 * @Date 2019/3/9 9:46
 * @Description:
 */
public interface ProductsInfoMapper extends BaseMapper<ProductsInfo> {
}
