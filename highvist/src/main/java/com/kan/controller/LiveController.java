package com.kan.controller;

import com.kan.model.Category;
import com.kan.model.Products;
import com.kan.service.ProductsService;
import com.kan.show.vo.CategoryVO;
import com.kan.show.vo.ProductsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by mam on 2016/1/13.
 */

@Controller
public class LiveController {

    @Autowired
    private ProductsService productsService;

    //首页
    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);

        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/index";
    }

    //products 设备列表 页
    @RequestMapping(value = "/products.htm", method = RequestMethod.GET)
    public String products(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/products";
    }

    //about-us
    @RequestMapping(value = "/about-us.htm", method = RequestMethod.GET)
    public String aboutUs(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);

        return "html/about-us";
    }

    //愿景
    @RequestMapping(value = "/vision-mission.htm", method = RequestMethod.GET)
    public String visionMission(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/vision-mission";
    }

    //chairman speaks
    @RequestMapping(value = "/cmd.htm", method = RequestMethod.GET)
    public String cmd(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/cmd";
    }

    //management
    @RequestMapping(value = "/management.htm", method = RequestMethod.GET)
    public String management(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/management";
    }

    //brands
    @RequestMapping(value = "/brands.htm", method = RequestMethod.GET)
    public String brands(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/brands";
    }

    //rebar-processing-equipment 设备列表
    @RequestMapping(value = "/products-equipment.htm", method = RequestMethod.GET)
    public String rebarProcessingEquipment(@RequestParam(value = "categoryId",required = true)String categoryId,ModelMap modelMap) {
        Category categoryById = productsService.findCategoryById(categoryId);
        if (categoryById == null){
            modelMap.put("categoryName","No Equipment");
        }else {
            modelMap.put("categoryName",categoryById.getCategoryName());
        }
        List<Products> productsList = productsService.findProductsListByCategoryId(categoryId);
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("productsList",productsList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/products-equipment";
    }


    //bar-bending-machines 设备详情
    @RequestMapping(value = "/bar-bending-machines.htm", method = RequestMethod.GET)
    public String barBendingMachines(@RequestParam("productId")String productId,ModelMap modelMap) {
        ProductsVO vo = productsService.findProductsVOByProductId(productId);
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(null);
        modelMap.put("productVO",vo);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/bar-bending-machines";
    }

    //infrastructure
    @RequestMapping(value = "/infrastructure.htm", method = RequestMethod.GET)
    public String infrastructure(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/infrastructure";
    }

    //spares-services
    @RequestMapping(value = "/spares-services.htm", method = RequestMethod.GET)
    public String sparesServices(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/spares-services";
    }

    //quality
    @RequestMapping(value = "/quality.htm", method = RequestMethod.GET)
    public String quality(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/quality";
    }

    //iso certifications
    @RequestMapping(value = "/iso-certifications.htm", method = RequestMethod.GET)
    public String isoCertifications(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/iso-certifications";
    }

    //product-compliance-certifications
    @RequestMapping(value = "/product-compliance-certifications.htm", method = RequestMethod.GET)
    public String productCertifications(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/product-compliance-certifications";
    }

    //careers
    @RequestMapping(value = "/careers.htm", method = RequestMethod.GET)
    public String careers(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        return "html/careers";
    }

    //contact
    @RequestMapping(value = "/contact.htm", method = RequestMethod.GET)
    public String contact(ModelMap modelMap) {
        List<Category> categoryList = productsService.findCategoryList();
        List<CategoryVO> categoryVOList = productsService.findCategoryAndProductsList(categoryList);
        List<Products> products = productsService.findProducts();
        modelMap.put("categoryList",categoryList);
        modelMap.put("categoryVOList",categoryVOList);
        modelMap.put("products",products);
        return "html/contact";
    }


    public static String getStringByBit(String str, int byteLength) {
        try {
            if(str.equals(new String(str.getBytes("iso8859-1"), "iso8859-1")))
                str = new String(str.getBytes("iso8859-1"), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String strs[] = str.split("");
        StringBuffer stringBuffer = new StringBuffer();
        int hasRead = 0; // 已读取的字节数
        if (str.getBytes().length <= byteLength) {
            return str;
        } else {
            for (int i = 0; i < strs.length; i++) {
                if (strs[i].getBytes().length == 1) {
                    hasRead++;
                    str += strs[i];
                    stringBuffer.append(strs[i]);
                }
                if (strs[i].getBytes().length == 2) {
                    hasRead += 2;
                    str += strs[i];
                    stringBuffer.append(strs[i]);
                }
                if (hasRead >= byteLength)
                    break;
            }
            if (str.getBytes().length > byteLength) {
                stringBuffer.append("...");
            }
        }
        return stringBuffer.toString();
    }


}
