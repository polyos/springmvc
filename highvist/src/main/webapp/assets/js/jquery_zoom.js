$(document).ready(function(){
    jQuery("a[rel='zoominbox']").colorbox({maxWidth:'95%', maxHeight:'95%'});
    //Example of preserving a JavaScript event for inline calls.
    $("#click").click(function(){ 
    $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
    return false;
    });
});
/* Colorbox resize function */
var resizeTimer;
function resizeColorBox()
{
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
            if (jQuery('#cboxOverlay').is(':visible')) {
                    jQuery.colorbox.load(true);
            }
    }, 300)
}

// Resize Colorbox when resizing window or changing mobile device orientation
jQuery(window).resize(resizeColorBox);
window.addEventListener("orientationchange", resizeColorBox, false);
