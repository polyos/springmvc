/**
 * Created by Administrator on 2015/7/26.
 */

(function($) {

	$.fn.seatSortable = function(options) {
		var opts = $.extend({}, $.fn.seatSortable.defaults, options);
		if (opts.shoppingMode) {
			opts.moveMode = false;
			opts.displayType = 0;
		}
		var itemContainerName = opts.itemContainerName;
		var current = this;
		var itemContainer;
		var moveItems;
		var canUpdateItem = false;
		var selectWork = false;
		var blockStatistics = {};

		var rowNoMap = {};

		var init = function() {
			current.css("width", (opts.maxColumn * opts.blockWidth + 2) + opts.blockWidth + opts.blockPadding * 2 + "px").css("height", (opts.maxRow * opts.blockHeight + 2) + opts.blockHeight + opts.blockPadding * 2 + "px").css("position", "relative").css("moz-user-select", "none").css("-khtml-user-select", "none").css("user-select", "none");
			if (opts.navMode) {
				creatFramework();
			} else {
				createDefaultContainer();
			}
			itemContainerListener();
			$(document).mouseup(function(e) {
				if (canUpdateItem || selectWork) {
					itemContainer.mouseup();
				}
			});
			if (opts.data) {
				setData(opts.data);
			}

		}

		var updateItemAttr = function() {
			if (canUpdateItem) {
				var exist = false;
				$("#" + itemContainerName).children("div.selected").each(function() {
					var temp = $(this).position().left + 0.5;
					var y = parseInt(($(this).position().top + 0.5 - opts.blockPadding) / opts.blockHeight);
					var num = parseInt(temp / opts.blockWidth);
					num = num.toFixed(0);
					if (num >= opts.maxColumn) {
						num = opts.maxColumn - 1;
					}
					var items = $("#" + itemContainerName + ">div[y=" + parseInt(y) + "]").not(".selected");
					var item = items.filter("[x=" + parseInt(num) + "]");
					if (item.size() > 0) {
						exist = true;
					}
				});
				if (exist) {
					alert("不可移动到已有的座位上");
					for (var i = 0; i < moveItems.length; i++) {
						moveItems[i].obj.css("left", moveItems[i].left + "px");
					}
				} else {
					$("#" + itemContainerName).children("div.selected").each(function() {
						var temp = $(this).position().left + 0.5;
						var num = parseInt(temp / opts.blockWidth);
						num = num.toFixed(0);
						if (num >= opts.maxColumn) {
							num = opts.maxColumn - 1;
						}
						$(this).css("left", num * opts.blockWidth + opts.blockPadding + "px");
						var x = $(this).position().left;
						$(this).attr("x", parseInt((x - opts.blockPadding) / opts.blockWidth));
					});
				}
				canUpdateItem = false;
			}
		}
		// seat_no reserve price alias color is_sold
		var drawItem = function(x, y, width, height, data) {
			var item = $('<div style="width: ' + (opts.blockWidth - opts.blockPadding * 2) + 'px;height: ' + (opts.blockHeight - opts.blockPadding * 2) + 'px;padding: ' + opts.blockPadding + 'px;position: absolute;">' + '<div class="block" style="width: 100%;height: 100%;font-size:12px;line-height: ' + (opts.blockHeight - opts.blockPadding * 2) + 'px;"></div></div>');
			item.css("left", x + opts.blockPadding + "px").css("top", y + opts.blockPadding + "px").css("text-align", "center").css("color", "#fff");
			item.attr("x", x / opts.blockWidth);
			item.attr("y", y / opts.blockHeight);
			var yy = parseInt(y / opts.blockWidth);
			var rn = blockStatistics[yy];
			if (blockStatistics[yy] == null) {
				blockStatistics[yy] = 1;
			} else {
				blockStatistics[yy] = blockStatistics[yy] + 1;
			}
			if (data != null) {
				if (data.rowNo != null) {
					rowNoMap[yy + 1] = data.rowNo;
				}
				if (data.price != null) {
					item.attr("price", data.price);
				}
				if (data.reserve != null) {
					item.attr("reserve", data.reserve);
				}
				if (data.seatNo != null) {
					item.attr("seatNo", data.seatNo);
				}
				if (data.alias != null) {
					item.attr("alias", data.alias);
				}
				if (data.color != null) {
					item.attr("color", data.color);
					item.children().css("background-color", data.color);
				}
				if (data.isSold != null) {
					item.attr("isSold", data.isSold);
					item.children().css("background-color", data.color);
					item.children().html(data.seatNo);
					if (data.isSold != 0 && data.isSold != 3) {
						item.addClass("shoppingSold");
					}
				}
				// displayType 内容显示设置
				if (opts.displayType == 1) {
					if (data.seatNo != null) {
						item.children().html(data.seatNo);
					}
				} else if (opts.displayType == 2) {
					if (data.reserve != null) {
						item.children().html(data.reserve);
					}
				} else if (opts.displayType == 0) {
					item.children().html("");
				} else if (opts.displayType == 3) {
					if (data.reserve != null) {
						item.children().html(data.reserve);
					} else if (data.seatNo != null) {
						item.children().html(data.seatNo);
					}
				}
			}
			var isMoved = false;
			itemContainer.append(item);
			item.click(function(e) {
				e.stopPropagation();
				if (isMoved == false) {
					if (opts.shoppingMode) {
						if (!$(this).hasClass("shoppingSold")) {
							$(this).toggleClass("shoppingSelected");
						}
					} else {
						if (opts.displayType == 3) {
							var _attrIssold = item.attr("issold");
							if (!_attrIssold || _attrIssold == "0" || _attrIssold == "3") {
								$(this).toggleClass("selected");
							}
						} else {
							$(this).toggleClass("selected");
						}
					}
				} else {
					isMoved = false;
				}
			});
			if (opts.moveMode) {
				item.mousedown(function(e) {
					if ($(this).attr("class") == null || $(this).attr("class").indexOf("selected") < 0) {
						return;
					} else {
						// $("#itemContainer").children("div.selected")
						var x = e.pageX - itemContainer.offset().left;
						var y = e.pageY - itemContainer.offset().top;
						moveItems = new Array();
						var maxLeft = null;
						var minLeft = null;
						$("#" + itemContainerName).children("div.selected").each(function() {
							moveItems.push({
								obj : $(this),
								left : $(this).position().left,
								top : $(this).position().top
							});
							if (maxLeft == null) {
								maxLeft = $(this).position().left
							} else {
								if (maxLeft < $(this).position().left) {
									maxLeft = $(this).position().left;
								}
							}
							if (minLeft == null) {
								minLeft = $(this).position().left;
							} else {
								if (minLeft > $(this).position().left) {
									minLeft = $(this).position().left;
								}
							}
						});
						var rightWidth = (opts.maxColumn * opts.blockWidth - opts.blockWidth) - maxLeft + opts.blockPadding;
						var leftWidth = -minLeft + opts.blockPadding;
						itemContainer.bind("mousemove", function(ev) {
							var xx = ev.pageX - itemContainer.offset().left;
							var yy = ev.pageY - itemContainer.offset().top;
							var width = xx - x;
							if (width < leftWidth) {
								width = leftWidth;
							}
							if (width > rightWidth) {
								width = rightWidth;
							}
							for (var i = 0; i < moveItems.length; i++) {
								var obj = moveItems[i].obj;
								var left = moveItems[i].left;

								obj.css("left", left + width + "px");
							}
							isMoved = true;
							canUpdateItem = true;
						});
						e.stopPropagation();
					}
				}).mouseup(function(e) {
					// 在itemContainer mouseup完成
				});
			}
		};

		var creatFramework = function() {
			var left = '<div style="position: absolute; width:' + opts.blockWidth + 'px;">' + '<div style="height: ' + (opts.blockHeight + opts.blockPadding + 1) + 'px;width: ' + opts.blockWidth + 'px"></div>';
			for (var i = 0; i < opts.maxRow; i++) {
				left = left + '<div class="seatNav left_' + itemContainerName + '" style="cursor: pointer; height: ' + opts.blockHeight + 'px;text-align: center;font-size:12px;line-height: ' + opts.blockHeight + 'px;">' + (i + 1) + '</div>';
			}
			left = left + '</div>';
			var right = '<div style="margin-left: ' + opts.blockWidth + 'px">' + '<div style="height: ' + opts.blockHeight + 'px">';
			right = right + '<div style="float: left; height: ' + opts.blockHeight + 'px; width: ' + (opts.blockPadding + 1) + 'px; text-align: center;line-height: ' + opts.blockHeight + 'px;"></div>';
			for (i = 0; i < opts.maxColumn; i++) {
				right = right + '<div class="seatNav top_' + itemContainerName + '" style="cursor: pointer;float: left; height: ' + opts.blockHeight + 'px; width: ' + opts.blockWidth + 'px; text-align: center;font-size:12px;line-height: ' + opts.blockHeight + 'px;">' + (i + 1) + '</div>';
			}
			right = right + '</div><div id="' + itemContainerName + '" class="itemContainer" style="border: 1px solid ' + opts.containerBorderColor + '; width: ' + (opts.maxColumn * opts.blockWidth) + 'px; height: ' + (opts.maxRow * opts.blockHeight) + 'px;' + ' padding: ' + opts.blockPadding + 'px;position: relative;"></div></div>';
			current.append(left);
			current.append(right);
			// 行列选择
			$(".top_" + itemContainerName).click(function() {
				$("#" + itemContainerName).children("div.selected").removeClass("selected");
				$("#" + itemContainerName).children("[x=" + (parseInt($(this).text()) - 1) + "]:not([issold=1],[issold=2])").addClass("selected");
			});
			$(".left_" + itemContainerName).click(function() {
				$("#" + itemContainerName).children("div.selected").removeClass("selected");
				$("#" + itemContainerName).children("[y=" + (parseInt($(this).text()) - 1) + "]:not([issold=1],[issold=2])").addClass("selected");
			});
			itemContainer = $("#" + itemContainerName);
		};

		var createDefaultContainer = function() {
			var container = '<div id="' + itemContainerName + '" class="itemContainer" style="border: 1px solid ' + opts.containerBorderColor + '; width: ' + (opts.maxColumn * opts.blockWidth) + 'px; height: ' + (opts.maxRow * opts.blockHeight) + 'px;' + ' padding: ' + opts.blockPadding + 'px;position: relative;"></div>';
			current.append(container);
			itemContainer = $("#" + itemContainerName);
		}

		/* 选择实现 */
		var isOverlap = function() {
			var objOne = $("#selector");
			var objTwos = $("#" + itemContainerName + " > div");
			objTwos.each(function() {
				var objTwo = $(this);
				var offsetOne = objOne.offset(), offsetTwo = objTwo.offset(), topOne = offsetOne.top, topTwo = offsetTwo.top, leftOne = offsetOne.left, leftTwo = offsetTwo.left, widthOne = objOne.width(), widthTwo = objTwo.width(), heightOne = objOne.height(), heightTwo = objTwo.height();
				var leftTop = leftTwo > leftOne && leftTwo < leftOne + widthOne && topTwo > topOne && topTwo < topOne + heightOne, rightTop = leftTwo + widthTwo > leftOne && leftTwo + widthTwo < leftOne + widthOne && topTwo > topOne && topTwo < topOne + heightOne, leftBottom = leftTwo > leftOne && leftTwo < leftOne + widthOne && topTwo + heightTwo > topOne && topTwo + heightTwo < topOne + heightOne, rightBottom = leftTwo + widthTwo > leftOne && leftTwo + widthTwo < leftOne + widthOne && topTwo + heightTwo > topOne && topTwo + heightTwo < topOne + heightOne;
				if (leftTop || rightTop || leftBottom || rightBottom) {
					objTwo.each(function() {
						if (opts.shoppingMode) {
							if (!$(this).hasClass("shoppingSold")) {
								$(this).addClass("shoppingSelected");
							}
						} else {
							if (opts.displayType != 3) {
								$(this).addClass("selected");
							} else {
								// 人工选座模式，锁定、已售票不勾选
								if ($(this).attr("issold") != "1" && $(this).attr("issold") != "2") {
									$(this).addClass("selected");
								}
							}
						}
					});
				}
			});

		};

		var selectorStart = function(e) {
			var div = $("#selector");
			if (div.size() == 0) {
				div = $('<div id="selector" style="background: blue none repeat scroll 0% 0%; position: absolute; opacity: 0.2; left: 403px; top: 90px; width: 0px; height: 0px;"></div>')
				itemContainer.append(div);
			}
			var x = e.pageX - itemContainer.offset().left;
			var y = e.pageY - itemContainer.offset().top;

			itemContainer.bind("mousemove", function(ev) {
				var xx = ev.pageX - itemContainer.offset().left;
				var yy = ev.pageY - itemContainer.offset().top;
				var width = xx - x;
				var height = yy - y;
				var nx;
				var ny;
				if (width < 0) {
					nx = x + width;
				} else {
					nx = x;
				}
				if (height < 0) {
					ny = y + height;
				} else {
					ny = y;
				}
				div.css("left", nx + "px").css("top", ny + "px").css("width", Math.abs(width) + "px").css("height", Math.abs(height) + "px");
				selectWork = true;
			});
		};

		var selectorEnd = function(e) {
			isOverlap();
			var div = $("#selector");
			div.css("width", "0px").css("height", "0px");
			selectWork = false;
		};

		var itemContainerListener = function() {
			itemContainer.mousedown(function(e) {
				if (opts.selectMode) {
					selectorStart(e);
				}
			}).mouseup(function(e) {
				if (opts.selectMode) {
					selectorEnd(e);
				}
				updateItemAttr();
				itemContainer.unbind("mousemove");
				e.stopPropagation();
			}).click(function(e) {
				e.stopPropagation();
			});
		};

		var addItem = function(row) {
			if (row < 0 || row >= opts.maxRow) {
				alert("超出边界");
				return;
			}
			var items = $("#" + itemContainerName + " > div[y=" + row + "]");
			if (items.size() == opts.maxColumn) {
				alert("不能再添加座位");
				return;
			}
			for (var i = 0; i < opts.maxColumn; i++) {
				var item = items.filter("[x=" + parseInt(i) + "]");
				if (item.size() == 0) {
					drawItem(i * opts.blockWidth, row * opts.blockHeight, opts.blockWidth, opts.blockHeight)
					break;
				}
			}
		};

		var addItemXY = function(x, y) {
			if (y < 0 || y >= opts.maxRow) {
				alert("超出边界");
				return;
			}
			if (x < 0 || x >= opts.maxColumn) {
				alert("超出边界");
				return;
			}
			var items = $("#" + itemContainerName + " > div[y=" + parseInt(y) + "]");
			var item = items.filter("[x=" + parseInt(x) + "]");
			if (item.size() > 0) {
				// alert("该位置已存在");
				// 批量添加，去除弹窗提示，继续添加下一个
				return;
			}
			drawItem(x * opts.blockWidth, y * opts.blockHeight, opts.blockWidth, opts.blockHeight)
		};

		var removeItem = function() {
			var selected = $("#" + itemContainerName).children("div.selected");
			var y = parseInt(selected.attr("y"));
			var x = blockStatistics[y];
			var length = selected.size();
			if ((x - length) <= 0) {
				delete blockStatistics[y];
			} else {
				blockStatistics[y] = x - length;
			}
			selected.remove();
		};

		// 设置座位号
		var setItemNo = function(start, step, reverse) {

			var selected = $("#" + itemContainerName).children("div.selected");
			var temp = {};
			selected.each(function() {
				var x = $(this).attr("x");
				var y = $(this).attr("y");
				var row = temp[y];
				if (row != null) {
					row.push(parseInt(x));
				} else {
					temp[y] = new Array();
					temp[y].push(parseInt(x));
				}
			});

			// start 同排座位号不能重复
			var rowSortData = new Array();
			$("#" + itemContainerName).children().each(function() {
				var seatNum = parseInt($(this).attr("seatno"));
				if (!isNaN(seatNum)) {
					var row = parseInt($(this).attr("y"));
					if (!rowSortData[row]) {
						rowSortData[row] = new Array();
						rowSortData[row][seatNum] = 1;
					} else {
						rowSortData[row][seatNum] = 1;
					}
				}
			})
			// end

			for ( var row in temp) {
				var xs = temp[row];
				var y = row;
				if (reverse) {
					xs = xs.sort(function(a, b) {
						return a < b ? 1 : -1
					});
				} else {
					xs = xs.sort(function(a, b) {
						return a > b ? 1 : -1
					});
				}
				var n = start;
				for (var i = 0; i < xs.length; i++) {
					if (rowSortData[row] && rowSortData[row][n]) {
						return false;
					}
					var item = selected.filter("[x=" + parseInt(xs[i]) + "][y=" + parseInt(y) + "]");
					if (opts.displayType == 1) {
						item.children().html(n);
					} else if (opts.displayType == 3) {
						// 3 座位号显示
						if (item.attr("reserve") && item.attr("reserve").length > 0) {
						} else {
							item.children().html(n);
						}
					}
					item.attr("seatNo", n);
					n = n + step;
				}
			}
			return true;
		};

		var removeItemNo = function() {
			var selected = $("#" + itemContainerName).children("div.selected");
			selected.removeAttr("seatNo");
			if (opts.displayType == 1 || opts.displayType == 3) {
				selected.children().html("");
			}

		};

		var setStrategy = function(alias) {
			var selected = $("#" + itemContainerName).children("div.selected");
			var st = opts.priceStrategy[alias];
			if (st != null) {
				selected.attr("alias", alias);
				selected.attr("price", st.price);
				selected.attr("color", st.color);
				selected.children().css("background-color", st.color);
			}
		};

		var removeStrategy = function() {
			var selected = $("#" + itemContainerName).children("div.selected");
			selected.removeAttr("alias");
			selected.removeAttr("price");
			selected.removeAttr("color");
			selected.children().css("background-color", "");
		};

		var setReserve = function(reserve) {
			var selected = $("#" + itemContainerName).children("div.selected");
			selected.attr("reserve", reserve);
			if (opts.displayType == 2) {
				selected.children().html(reserve);
			}
		};

		var removeReserve = function() {
			var selected = $("#" + itemContainerName).children("div.selected");
			selected.removeAttr("reserve");
			if (opts.displayType == 2) {
				selected.children().html("");
			}
		};

		var redrawContainer = function(row, column) {
			var data = getData();
			var maxX;
			var maxY;
			for (var i = 0; i < data.length; i++) {
				var x = parseInt((data[i].column)) - 1;
				var y = parseInt((data[i].row)) - 1;
				if (maxX == null || x > maxX) {
					maxX = x;
				}
				if (maxY == null || y > maxY) {
					maxY = y;
				}
			}
			if (row <= maxY) {
				alert("行数不能小于已有座位");
				return;
			}
			if (column <= maxX) {
				alert("列数不能小于已有座位");
				return;
			}
			canUpdateItem = false;
			selectWork = false;
			current.empty();
			opts.maxRow = row;
			opts.maxColumn = column;
			init();
			blockStatistics = {};
			setData(data);
		};

		// seat_no reserve price alias color is_sold
		var getData = function() {
			var selected = $("#" + itemContainerName).children("div").not("#selector");
			var data = new Array();
			selected.each(function() {
				var temp = {};
				var x = $(this).attr("x");
				var y = $(this).attr("y");
				var price = $(this).attr("price");
				var reserve = $(this).attr("reserve");
				var seatNo = $(this).attr("seatNo");
				var alias = $(this).attr("alias");
				var color = $(this).attr("color");
				var isSold = $(this).attr("isSold");
				temp.column = parseInt(x) + 1;
				temp.row = parseInt(y) + 1;
				temp.rowNo = rowNoMap[temp.row];
				temp.price = price;
				temp.reserve = reserve;
				temp.seatNo = seatNo;
				temp.alias = alias;
				temp.color = color;
				temp.isSold = isSold;
				data.push(temp);
			});
			return data;
		};

		var setData = function(data) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].x == null && data[i].y == null) {
					drawItem((data[i].column - 1) * opts.blockWidth, (data[i].row - 1) * opts.blockHeight, opts.blockWidth, opts.blockHeight, data[i]);
				} else {
					drawItem(data[i].x * opts.blockWidth, data[i].y * opts.blockHeight, opts.blockWidth, opts.blockHeight, data[i]);
				}

			}
		};

		var getSelectData = function() {
			var selected;
			if (opts.shoppingMode) {
				selected = $("#" + itemContainerName).children("div.shoppingSelected");
			} else {
				selected = $("#" + itemContainerName).children("div.selected");
			}
			var data = new Array();
			selected.each(function() {
				var temp = {};
				var x = $(this).attr("x");
				var y = $(this).attr("y");
				var price = $(this).attr("price");
				var reserve = $(this).attr("reserve");
				var seatNo = $(this).attr("seatNo");
				var alias = $(this).attr("alias");
				var color = $(this).attr("color");
				var isSold = $(this).attr("isSold");
				temp.column = parseInt(x) + 1;
				temp.row = parseInt(y) + 1;
				temp.rowNo = rowNoMap[temp.row];
				temp.price = price;
				temp.reserve = reserve;
				temp.seatNo = seatNo;
				temp.alias = alias;
				temp.color = color;
				temp.isSold = isSold;
				data.push(temp);
			});
			return data;
		};

		var getBlockStatistics = function() {
			return blockStatistics;
		}

		var getTotalRow = function() {
			var i = 0;
			for ( var x in blockStatistics) {
				i++;
			}
			return i;
		}

		var getTotalColumn = function() {
			var max = 0;
			for ( var x in blockStatistics) {
				var y = blockStatistics[x];
				if (y > max) {
					max = y;
				}
			}
			return max;
		};

		var setRowNo = function(rowNo, row) {
			// var item = $("#"+itemContainerName+" > div[y=" + parseInt(row-1)
			// + "]");
			// item.attr("rowNo", rowNo);
			rowNoMap[row] = rowNo;
		};

		var getRowNo = function(row) {
			return rowNoMap[row];
		};

		init();

		var returnData;

		if (opts.shoppingMode) {
			returnData = {
				getData : function() {
					return getData();
				},
				setData : function(data) {
					setData(data)
				},
				getSelectData : function() {
					return getSelectData();
				},
				clearSelected : function() {
					itemContainer.children("div.shoppingSelected").removeClass("shoppingSelected");
				},
				getRowNo : function(row) {
					return getRowNo(row);
				}
			}
		} else {
			returnData = {
				addItem : function(row, column) {
					if (column != null) {
						addItemXY(column, row);
					} else {
						addItem(row);
					}
				},
				removeItem : function() {
					removeItem();
				},
				setItemNo : function(start, step, reverse) {
					return setItemNo(start, step, reverse);
				},
				removeItemNo : function() {
					removeItemNo();
				},
				setStrategy : function(alias) {
					setStrategy(alias);
				},
				removeStrategy : function() {
					removeStrategy();
				},
				setReserve : function(type) {
					setReserve(type);
				},
				removeReserve : function() {
					removeReserve();
				},
				getData : function() {
					return getData();
				},
				setData : function(data) {
					setData(data)
				},
				getSelectData : function() {
					return getSelectData();
				},
				redrawContainer : function(row, column) {
					redrawContainer(row, column);
				},
				clearSelected : function() {
					itemContainer.children("div.selected").removeClass("selected");
				},
				getBlockStatistics : function() {
					return getBlockStatistics();
				},
				getTotalRow : function() {
					return getTotalRow();
				},
				getTotalColumn : function() {
					return getTotalColumn();
				},
				setRowNo : function(rowNo, row) {
					setRowNo(rowNo, row);
				},
				getRowNo : function(row) {
					return getRowNo(row);
				},
				maxRow : function() {
					return opts.maxRow;
				},
				maxColumn : function() {
					return opts.maxColumn;
				}

			}
		}

		return returnData;

	};

	// displayType 1 显示座位号 2 显示预留 0 不显示 3 已卖出不可选 （选座&开票后修改）
	$.fn.seatSortable.defaults = {
		maxRow : 10,
		maxColumn : 10,
		blockWidth : 30,
		blockHeight : 30,
		blockPadding : 5,
		containerBorderColor : "#999",
		selectMode : true,
		moveMode : true,
		shoppingMode : false,
		navMode : true,
		displayType : 0,
		priceStrategy : {},
		itemContainerName : "itemContainer"
	}

})(jQuery);