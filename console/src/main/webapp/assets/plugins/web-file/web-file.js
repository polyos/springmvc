;
(function($) {
	'use strict';

	var Modal = function(element, options) {
		this.init(element, options);
	};

	Modal.prototype = {
		constructor : Modal,

		/*
		 * 启动
		 */
		init : function(element, options) {
			this.options = options;
			this.$element = element;

			this._initContent();
			this._initPrototype();
			this._initEvent();

			var otherHeight = 0;
			if (this.options.navigation) {
				otherHeight += this.$navigation.height() + 15;
			}
			if (this.options.toolbar) {
				if (this.options.navigation) {
					this.$toolbar.css({
						"margin-top" : otherHeight,
					});
				}
				otherHeight += this.$toolbar.height() + 5;
			}
			this.$filelist.css({
				"margin-top" : otherHeight,
			});
			if (this.options.height) {
				this.$filelist.height(this.options.height - 20);
			}
			this.$container.show();
			return this;
		},

		/*
		 * 页面内容初始化
		 */
		_initContent : function() {
			this.fileNum = 0;
			this.fileNumChked = 0;

			var content = '<div class="wf-folder-container" oncontextmenu="return false">';
			// var content = '<div class="wf-folder-container" >';

			if (this.options.navigation) {
				// 导航栏维护数组
				if (!this.navigationArray) {
					this.navigationArray = [];
					this.navigationArray.push({
						pathId : 0,
						pathName : "全部文件"
					});
				}
				content += '<div class="wf-module-navigation"><ul>';
				for (var i = 0; i < this.navigationArray.length; i++) {
					if (i == this.navigationArray.length - 1) {
						content += '<li>' + this.navigationArray[i].pathName + '</li>';
					} else {
						content += '<li><a href="javascript:void(0);" >' + this.navigationArray[i].pathName + '</a></li>';
					}
				}
				content += '</ul></div>';
			}

			if (this.options.toolbar) {
				if (this.options.width >= 561) {
					content += '<div class="wf-module-toolbar">' + '<div class="wf-tool-chk">' + '<div class="wf-chk"></div>' + '<div class="wf-chk-status">共 <span class="wf-fileNum">' + this.fileNum + '</span> 个文件/文件夹，已选 <span class="wf-fileNumChked">' + this.fileNumChked + '</span> 个</div>' + '</div>' + '<div class="wf-tool-btn">' + '<div class="wf-btn wf-btn-upload">' + '<i class="wf-icon-upload"></i>上传' + '</div>' + '<div class="wf-btn wf-btn-download">' + '<i class="wf-icon-download"></i>下载' + '</div>' + '<div class="wf-btn wf-btn-delete">' + '<i class="wf-icon-delete"></i>删除' + '</div>' + '<div class="wf-btn wf-btn-rename">' + '<i class="wf-icon-rename"></i>重命名' + '</div>' + '</div>' + '</div>';
				} else {
					content += '<div class="wf-module-toolbar">' + '<div class="wf-tool-chk">' + '<div class="wf-chk"></div>' + '<div class="wf-chk-status">共 <span class="wf-fileNum">' + this.fileNum + '</span> 个文件/文件夹，已选 <span class="wf-fileNumChked">' + this.fileNumChked + '</span> 个</div>' + '</div>' + '</div>';
				}
			}

			if (arrayHasObject(this.options.filelist)) {
				content += '<div class="wf-module-list">';
			} else {
				content += '<div class="wf-module-list"><div class="wf-nofile">暂无内容</div>';
			}

			if (this.options.rightmenu) {
				var rightMenuHtml = "";
				var rightmenu = this.options.rightmenu;

				if (rightmenu.indexOf("cover") > -1) {
					rightMenuHtml += '<li><a class="wf-menu-cover">设为封面 </a></li>';
				}
				if (rightmenu.indexOf("open") > -1) {
					rightMenuHtml += '<li><a class="wf-menu-open">打开 </a></li>';
				}
				if (rightmenu.indexOf("download") > -1) {
					rightMenuHtml += '<li><a class="wf-menu-download">下载 </a></li>';
				}
				if (rightmenu.indexOf("rename") > -1) {
					rightMenuHtml += '<li><a class="wf-menu-rename">重命名</a></li>';
				}
				if (rightmenu.indexOf("delete") > -1) {
					rightMenuHtml += '<li class="wf-divider"></li><li><a class="wf-menu-delete">删除 </a></li>';
				}
				if (rightmenu.indexOf("newfolder") > -1) {
					rightMenuHtml += '<li class="wf-menu-out"><a class="wf-menu-newfolder">新建文件夹</a></li>';
				}
				if (rightmenu.indexOf("upload") > -1) {
					rightMenuHtml += '<li class="wf-menu-out"><a class="wf-menu-upload">上传 </a></li>';
				}
				if (rightmenu.indexOf("refresh") > -1) {
					rightMenuHtml += '<li class="wf-menu-out"><a class="wf-menu-refresh">刷新 </a></li>';
				}

				content += '<div class="wf-dropdown-menu-box"><ul class="wf-dropdown-menu">' + rightMenuHtml + '</ul></div>';
			}

			content += '</div></div>';
			$(this.$element).html(content);

			this.$container = $('.wf-folder-container', this.$element);
			this.$navigation = $('.wf-module-navigation', this.$container);
			this.$toolbar = $('.wf-module-toolbar', this.$container);
			this.$fileTotalnum = $('.wf-fileNum', this.$container);
			this.$wfChk = $(".wf-chk", this.$toolbar);
			this.$filelist = $('.wf-module-list', this.$container);

			this.fileChkedList = new Array();
		},
		/*
		 * 自定义内容初始化 file{ id,type,name,saveUrl,imgUrl,mark }
		 */
		_initPrototype : function() {
			if (this.options.width) {
				this.$container.width(this.options.width);
				if (this.options.navigation) {
					this.$navigation.width(this.options.width - 40);
				}
				if (this.options.toolbar) {
					this.$toolbar.width(this.options.width);
				}
			}
			if (this.options.height) {
				this.$container.height(this.options.height);
			}

			// 文件数组拷贝
			var __this = this, files = this.options.filelist = repairFileList(__this);
			if (files) {
				var fileHtml;
				for (var i = 0; i < files.length; i++) {
					if (__this.options.isShowFilename) {
						var typeNum = getFileType(files[i].fileType);
						if (typeNum == 3) {
							var openUrl = files[i].transcodingUrl;
							if (!openUrl || openUrl.length == 0) {
								openUrl = files[i].saveUrl;
							}
							var fileNameElem = '<a href="' + getMDFullUrl(__this.options.mediaDomain, openUrl) + '" target="_blank"  title="' + files[i].fileName + '">' + files[i].fileName + '</a>';
						} else if (typeNum == 0) {
							var fileNameElem = '<a href="javascript:void(0);"  title="' + files[i].fileName + '">' + files[i].fileName + '</a>';
						} else {
							var openUrl = files[i].saveUrl;
							var fileNameElem = '<a href="' + getMDFullUrl(__this.options.mediaDomain, openUrl) + '" target="_blank"  title="' + files[i].fileName + '">' + files[i].fileName + '</a>';
						}
						var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '<div class="wf-file-name">' + fileNameElem + '</div>' + '</div>';
					} else {
						var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '</div>';
					}
					addFile(fileHtml, files[i], i, __this);
				}
			}
			upadteTotalnum(__this);
		},

		/*
		 * 事件初始化
		 */
		_initEvent : function() {
			var __this = this;

			// 导航栏事件
			if (this.options.navigation) {
				$("li>a", this.$navigation).each(function() {
					$(this).click(function() {
						var aIndex = $(this).closest("li").index();
						var pathData = __this.navigationArray[aIndex];
						var filelist = __this.options.gotoFolderFunction.apply(__this, [pathData]);
						gotoFolder(__this, filelist, aIndex);
					});
				});
			}
			// 文件夹点击事件
			$(".wf-file-icon-dir", this.$filelist).each(function() {
				var clickElem = $(this).siblings('.wf-file-name').children("a");
				clickElem.click(function() {
					var chip = $(this).closest(".wf-file").data().file;
					var pathData = {
						pathId : chip.dirId,
						pathName : chip.fileName
					};
					var filelist = __this.options.gotoFolderFunction.apply(__this, [pathData]);
					gotoFolder(__this, filelist, -1, pathData);
				});
			})

			// 工具栏chk事件
			if (__this.options.toolbar) {
				__this.$wfChk.click(function() {
					$(this).toggleClass("wf-chked");
					if ($(this).hasClass("wf-chked")) {
						__this.$filelist.find(".wf-file-thumb").each(function() {
							if (!__this.options.isChooseDir && $(this).hasClass("wf-file-icon-dir")) {
								return true;
							}
							$(this).css("border-color", "#5fafaf");
							$(this).find(".wf-file-chk").addClass("wf-show").addClass("wf-file-chked");
							updateFileChked(__this, 0, __this.fileNum);
						})
						__this.fileChkedList = repairFileList(__this);
					} else {
						__this.$filelist.find(".wf-file-thumb").each(function() {
							$(this).css("border-color", "#f9f9f9");
							$(this).find(".wf-file-chk").removeClass("wf-show").removeClass("wf-file-chked");
							updateFileChked(__this, 0, 0);
						})
						__this.fileChkedList = [];
					}

				});

				/* 工具栏按钮事件 */
				// 上传
				$(".wf-btn-upload", __this.$toolbar).on('click', function(e) {
					killEvent(e);
					e.target = $(this);
					__this.options.uploadFileBtn.apply(__this, []);
				});
				// 删除
				$(".wf-btn-delete", __this.$toolbar).on('click', function(e) {
					killEvent(e);
					if (__this.options.deleteFunction.apply(__this, [__this.fileChkedList])) {
						deleteSelect($(this), __this);
					}
				});
				// 下载
				$(".wf-btn-download", __this.$toolbar).on('click', function(e) {
					killEvent(e);
					__this.options.downloadFileBtn.apply(__this, []);
				});
				// 重命名
				$(".wf-btn-rename", __this.$toolbar).on('click', function(e) {
					killEvent(e);
					reNameBefore($(this), __this);
					__this.options.renameBtn.apply(__this, []);
				});

			}

			if (__this.options.rightmenu) {

				/* 文件容器右键显示 */
				__this.$filelist.mousedown(function(e) {
					if (e.button == 2) {
						$(".wf-dropdown-menu", __this.$filelist).css({
							left : e.clientX - 5,
							top : e.clientY - 15
						}).show().children('.wf-menu-out').show().end().children(':not(.wf-menu-out)').hide();
					}
				})

				/* 右键菜单显示 */
				$(".wf-dropdown-menu", __this.$filelist).mouseleave(function(event) {
					disappearRight(__this);
				});

				/* 右键菜单事件 */
				if (__this.options.rightmenu.indexOf("open") > -1) {
					// 打开
					$(".wf-dropdown-menu .wf-menu-open", __this.$filelist).click(function() {
						var x = (screen.width - 800) / 2;
						var y = (screen.height - 600) / 2;
						var clickType = getFileType(__this.rightClickData.file.fileType);
						if (clickType == 3) {
							var openUrl = __this.rightClickData.file.transcodingUrl;
							if (!openUrl || openUrl.length == 0) {
								openUrl = __this.rightClickData.file.saveUrl;
							}
						} else if (clickType == 0) {
							var pathData = {
								pathId : __this.rightClickData.file.dirId,
								pathName : __this.rightClickData.file.fileName
							};
							var filelist = __this.options.gotoFolderFunction.apply(__this, [pathData]);
							gotoFolder(__this, filelist, -1, pathData);
							return;
						} else {
							var openUrl = __this.rightClickData.file.saveUrl;
						}
						window.open(contextPath + "/admin/preview.do?openUrl=" + getMDFullUrl(__this.options.mediaDomain, openUrl) + "&contentType=" + __this.rightClickData.file.fileType, 'showWindow', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 30) + ',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
						__this.options.rbOpenFunction.apply(__this, [__this.fileChkedList]);
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("upload") > -1) {
					// 上传
					$(".wf-dropdown-menu .wf-menu-upload", __this.$filelist).click(function() {
						__this.options.rbUploadFunction.apply(__this, [__this.fileChkedList]);
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("download") > -1) {
					// 下载
					$(".wf-dropdown-menu .wf-menu-download", __this.$filelist).click(function() {
						__this.options.rbDownloadFunction.apply(__this, [__this.fileChkedList]);
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("cover") > -1) {
					// 设为封面
					$(".wf-dropdown-menu .wf-menu-cover", __this.$filelist).click(function() {
						if (__this.options.rbSetCoverFunction.apply(__this, [__this.rightClickData.file])) {
							__this.rightClickData.element.addClass("wf-file-cover").siblings().removeClass("wf-file-cover");
						}
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("refresh") > -1) {
					// 刷新
					$(".wf-dropdown-menu .wf-menu-refresh", __this.$filelist).click(function() {
						if (__this.options.rbRefreshFunction.apply(__this, [repairFileList(__this)])) {
							__this.fnDraw();
						}
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("rename") > -1) {
					// 重命名
					$(".wf-dropdown-menu .wf-menu-rename", __this.$filelist).click(function() {
						reNameBefore($(".wf-btn-rename", __this.$toolbar), __this);
						__this.options.rbRenameFunction.apply(__this, [__this.fileChkedList]);
						disappearRight(__this);
					})
				}
				if (__this.options.rightmenu.indexOf("delete") > -1) {
					// 删除
					$(".wf-dropdown-menu .wf-menu-delete", __this.$filelist).click(function() {
						if (__this.options.rbDeleteFunction.apply(__this, [__this.fileChkedList])) {
							deleteSelect($(this), __this);
						}
					})
				}
				if (__this.options.rightmenu.indexOf("newfolder") > -1) {
					// 新建文件夹
					$(".wf-dropdown-menu .wf-menu-newfolder", __this.$filelist).click(function() {

						if (__this.options.isShowFilename) {
							var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '<div class="wf-file-name">' + '<input class="wf-rename-input" type="text" value="新建文件夹"/><span class="wf-rename-confirm"></span><span class="wf-rename-cancel"></span>' + '</div>' + '</div>';
						} else {
							var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '<div class="wf-file-name">' + '</div></div>';
						}

						var file = {
							"fileName" : "新建文件夹",
							"fileType" : "dir",
							"createTime" : new Date().getTime() / 1000,
							"status" : 10,
							"chipType" : 0,
							"dirId" : 2
						};

						// 添加文件
						var $newFile = addFile(fileHtml, file, __this.options.filelist.length, __this);

						// 添加事件
						$("input", $newFile).focus(function() {
							$(this).removeClass("wf-input-error");
						});

						$(".wf-rename-confirm", $newFile).click(function() {
							var newName = $.trim($('input', $newFile).val());
							if (newName.length == 0) {
								$('input', $newFile).addClass("wf-input-error");
								return;
							}
							var fileData = $newFile.data();
							var userFile = __this.options.rbNewfolderFunction.apply(__this, [newName, fileData.file]);

							if (userFile) {
								$newFile.data().file = userFile;
								var newFileData = $.extend({}, fileData.file, userFile);

								__this.options.filelist[fileData.fileIndex] = newFileData;

								$(".wf-file-name", $newFile).html('<a href="javascript:void(0);" title="' + newFileData.fileName + '">' + newFileData.fileName + '</a>').click(function() {
									var clickElem = $(this).children("a");
									clickElem.click(function() {
										var chip = $(this).closest(".wf-file").data().file;
										var pathData = {
											pathId : chip.dirId,
											pathName : chip.fileName
										};
										var filelist = __this.options.gotoFolderFunction.apply(__this, [pathData]);
										gotoFolder(__this, filelist, -1, pathData);
									});
								});
							} else {
								$('input', $newFile).addClass("wf-input-error");
							}
						});

						$(".wf-rename-cancel", $newFile).click(function() {
							var $$fileElem = $(this).closest('.wf-file');
							var _fileData = $$fileElem.data();

							__this.options.filelist[_fileData.fileIndex] = null;
							--__this.fileNum;
							upadteTotalnum(__this);
							$$fileElem.removeData();
							$$fileElem.remove();

							if (__this.fileNum == 0) {
								__this.fnDraw();
							}
						})

						upadteTotalnum(__this);
						disappearRight(__this);
					})
				}

			}

		},
		getFile : function(index) {
			return this.options.filelist[index];
		},
		getSelectData : function() {
			return this.fileChkedList;
		},
		getFiles : function() {
			return this.options.filelist;
		},
		delFile : function(index) {
			if (this.options.rightmenu && this.options.rightmenu.length > 1) {
				var delElemt = $(".wf-file", this.$filelist).eq(index + 1);
			} else {
				var delElemt = $(".wf-file", this.$filelist).eq(index + 0);
			}
			this.fileChkedList = [];
			this.options.filelist[index] = null;
			delElemt.remove();
		},
		fnDraw : function(data) {
			if (data) {
				this.options.filelist = data;
			} else {
				this.options.filelist = repairFileList(this);
			}

			return this.init(this.$element, this.options);

		},
		addLabel : function(chipId, label) {
			fnAddLabel(chipId, label, this);
		},
		addFileFunction : function(fileData) {
			var typeNum = getFileType(fileData.fileType);
			if (typeNum == 3) {
				var openUrl = fileData.transcodingUrl;
				if (!openUrl || openUrl.length == 0) {
					openUrl = fileData.saveUrl;
				}
				var fileNameElem = '<a href="' + getMDFullUrl(this.options.mediaDomain, openUrl) + '" target="_blank">' + fileData.fileName + '</a>';
			} else if (typeNum == 0) {
				var fileNameElem = '<a href="javascript:void(0);">' + fileData.fileName + '</a>';
			} else {
				var openUrl = fileData.saveUrl;
				var fileNameElem = '<a href="' + getMDFullUrl(this.options.mediaDomain, openUrl) + '" target="_blank">' + fileData.fileName + '</a>';
			}

			if (this.options.isShowFilename) {
				var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '<div class="wf-file-name">' + fileNameElem + '</div>' + '</div>';
			} else {
				var fileHtml = '<div class="wf-file">' + '<div class="wf-file-thumb">' + '<span  class="wf-file-chk"></span>' + '</div>' + '</div>';
			}
			var dataIndex = this.options.filelist.length;
			this.options.filelist.push(fileData);
			return addFile(fileHtml, fileData, dataIndex, this);
		},
		fnGetFileType : function(fileType) {
			// 输入字符串，返回 类型 数值
			// 未识别：-1,文件夹：0，图片：1，音频：2，视频：3
			return getFileType(fileType);
		},
		fnGetCurrentPathId : function() {
			return this.navigationArray[this.navigationArray.length - 1].pathId;
		}

	};

	var killEvent = function(event) {
		event.preventDefault();
		event.stopPropagation();
	};

	var getMDFullUrl = function(mediaDomain, url) {
		if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
			return url;
		}
		if (mediaDomain == null) {
			return url;
		}
		return mediaDomain + url;
	};

	var fnAddLabel = function(chipId, label, WebFile) {
		var labelStr = "";
		switch (label) {
			case 0 :
				labelStr = "";
				break;
			case 1 :
				labelStr = "已用";// 已用未编辑
				break;
			case 2 :
				labelStr = "已编辑";
				break;
			case 3 :
				labelStr = "已用";// 已用已编辑
				break;
			default :
				labelStr = "";
		}

		if (label && label > 0) {
			$(".wf-file", WebFile.$filelist).each(function() {
				var data = $(this).data();

				if (data.file && data.file.id == chipId) {
					$(this).append("<span class='wf-file-label'>" + labelStr + "</span>");

					data.file.isCompere = label;
					data.file.editStatus = 2;
					WebFile.options.filelist[data.fileIndex].isCompere = label;
					WebFile.options.filelist[data.fileIndex].editStatus = 2;
					return false;
				}
			})
		}
	}

	var reNameBefore = function(elem, WebFile) {
		if (!elem.data("change")) {
			$(".wf-file-chked", WebFile.$filelist).each(function() {
				var $fileElem = $(this).closest('.wf-file');
				var fileData = $fileElem.data("file");
				var input = '<input class="wf-rename-input" type="text" value="' + fileData.fileName + '"/><span class="wf-rename-confirm"></span><span class="wf-rename-cancel"></span>';
				$(".wf-file-name", $fileElem).html('').append($(input));

				// 命名提交/取消
				$("input", $fileElem).focus(function() {
					$(this).removeClass("wf-input-error");
				});
				$(".wf-rename-confirm", $fileElem).click(function() {
					var $$fileElem = $(this).closest('.wf-file');
					var _fileData = $$fileElem.data();
					var oldName = _fileData.file.fileName;
					var newName = $.trim($('input', $$fileElem).val());

					if (newName.length == 0) {
						$('input', $$fileElem).addClass("wf-input-error");
						return;
					}

					if (WebFile.options.renameCallback.apply(WebFile, [oldName, newName, _fileData.file])) {
						_fileData.file.fileName = newName;
						WebFile.options.filelist[_fileData.fileIndex] = _fileData.file;
						$(".wf-file-name", $fileElem).html('<a href="javascript:void(0);">' + _fileData.file.fileName + '</a>').click(function() {
							var clickElem = $(this).children("a");
							clickElem.click(function() {
								var chip = $(this).closest(".wf-file").data().file;
								var pathData = {
									pathId : chip.dirId,
									pathName : chip.fileName
								};
								var filelist = WebFile.options.gotoFolderFunction.apply(WebFile, [pathData]);
								gotoFolder(WebFile, filelist, -1, pathData);
							});
						});
					}
				})
				$(".wf-rename-cancel", $fileElem).click(function() {
					var $$fileElem = $(this).closest('.wf-file');
					var _fileData = $$fileElem.data();
					$(".wf-file-name", $fileElem).html('<a href="' + _fileData.file.saveUrl + '" title="' + _fileData.file.fileName + '">' + _fileData.file.fileName + '</a>');

					if ($(".wf-file-chked", WebFile.$filelist).length == 0) {
						elem.html('<i class="wf-icon-rename"></i>重命名').data("change", false);
					}
				})

			});
			// 重命名按钮是否改变标识
			if ($(".wf-file-chked", WebFile.$filelist).length > 0) {
				elem.html('<i class="wf-icon-rename"></i>取消').data("change", true);
			}
		} else {
			WebFile.$wfChk.removeClass("wf-chked");
			WebFile.fileChkedList = [];
			updateFileChked(WebFile, 0, 0);
			$(".wf-file-chked", WebFile.$filelist).each(function() {
				$(this).parent().css("border-color", "#f9f9f9");
				$(this).removeClass("wf-show").removeClass("wf-file-chked");
				var $fileElem = $(this).closest('.wf-file');
				var fileData = $fileElem.data("file");
				$(".wf-file-name", $fileElem).html('<a href="' + fileData.saveUrl + '">' + fileData.fileName + '</a>');
			});
			elem.html('<i class="wf-icon-rename"></i>重命名').data("change", false);
		}
	}

	var disappearRight = function(WebFile) {
		$(".wf-dropdown-menu", WebFile.$filelist).hide();
	}

	// 取消选择
	var cancelSelectElement = function(dataIndex, WebFile) {
		if (WebFile.options.toolbar) {
			updateFileChked(WebFile, -1);
		}
		if (WebFile.$wfChk.hasClass("wf-chked")) {
			WebFile.$wfChk.removeClass("wf-chked");
		}

		var chks = WebFile.fileChkedList;
		var id = WebFile.options.filelist[dataIndex].id;
		var array = [];
		for (var i = 0; i < chks.length; i++) {
			if (chks[i].id != id) {
				array.push(chks[i]);
			}
		}
		WebFile.fileChkedList = array;
	}

	var upadteTotalnum = function(WebFile) {
		WebFile.$fileTotalnum.text(WebFile.fileNum);
		return WebFile.fileNum;
	}

	// 已选文件数 监听
	var updateFileChked = function(WebFile, type, val) {
		switch (type) {
			case 1 :// ++
				++WebFile.fileNumChked;
				break;
			case -1 :// --
				--WebFile.fileNumChked;
				break;
			case 0 :// val
				WebFile.fileNumChked = val;
				break;
			default :
				break;
		}
		var num = WebFile.fileNumChked;

		if (WebFile.options.toolbar) {
			$(".wf-chk-status>.wf-fileNumChked", WebFile.$toolbar).text(num);

			if (num > 0) {
				$(".wf-btn-delete", WebFile.$toolbar).fadeIn(300);
			} else {
				$(".wf-btn-delete", WebFile.$toolbar).fadeOut(300);
			}

			if (num == 1) {
				$(".wf-btn-download", WebFile.$toolbar).fadeIn(300);
				$(".wf-btn-rename", WebFile.$toolbar).fadeIn(300);
				$(".wf-dropdown-menu .wf-menu-rename", WebFile.$filelist).removeClass("wf-menu-disable");

			} else {
				$(".wf-btn-download", WebFile.$toolbar).fadeOut(300);
				$(".wf-btn-rename", WebFile.$toolbar).fadeOut(300);
				$(".wf-dropdown-menu .wf-menu-rename", WebFile.$filelist).addClass("wf-menu-disable");
			}
		}

		if (WebFile.options.rightmenu && WebFile.options.rightmenu.indexOf("rename") > -1) {
			if (num == 1) {
				$(".wf-dropdown-menu .wf-menu-rename", WebFile.$filelist).removeClass("wf-menu-disable");
			} else {
				$(".wf-dropdown-menu .wf-menu-rename", WebFile.$filelist).addClass("wf-menu-disable");
			}
		}
		return num;
	}

	// 获取当前所有文件数据
	var repairFileList = function(WebFile) {
		var array = [];
		var files = WebFile.options.filelist;
		for ( var i in files) {
			if (files[i] && typeof files[i] == 'object') {
				array.push(files[i]);
			}
		}
		return array;
	}

	// 删除已选
	var deleteSelect = function(element, WebFile) {
		$(".wf-file-chked", WebFile.$filelist).each(function() {
			var $fileElem = $(this).closest('.wf-file');
			var fileIndex = $fileElem.data().fileIndex;
			$fileElem.remove();
			WebFile.options.filelist[fileIndex] = null;
			--WebFile.fileNum;
		});
		WebFile.fileChkedList = [];
		updateFileChked(WebFile, 0, 0);

		if (upadteTotalnum(WebFile) == 0) {
			WebFile.fnDraw();
		}
		disappearRight(WebFile);
	}

	// 传入数组，[] = false, [function()] = false,[{}] = true,[{},function] = true
	var arrayHasObject = function(array) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] && typeof array[i] == 'object') {
				return true;
			}
		}
		return false;
	}

	var addFile = function(fileHtml, fileData, dataIndex, WebFile) {
		if (!fileData || typeof fileData != 'object') {
			return false;
		}

		var returnElement;
		var _fileItem = $(fileHtml);
		// 文件类型
		var filetype = getFileType(fileData.fileType);
		switch (filetype) {
			case -1 :
				if ("XLS,RTF,DOC,PPT,PDF,TXT".indexOf(fileData.fileType.toUpperCase()) > -1) {
					$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-" + fileData.fileType.toLowerCase());
				} else {
					$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-undefined");
				}
				break;
			case 0 :
				$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-dir");
				break;
			case 1 :
				$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-img");
				var showUrl = fileData.thumbUrl == null ? fileData.saveUrl : fileData.thumbUrl;
				$(".wf-file-thumb", _fileItem).css({
					'background-image' : 'url("' + getMDFullUrl(WebFile.options.mediaDomain, showUrl) + '")',
					'background-size' : WebFile.options.thumbWith + 'px ' + WebFile.options.thumbHeight + 'px',
					'background-position' : 'center center',
				});
				break;
			case 2 :
				$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-music");
				break;
			case 3 :
				$(".wf-file-thumb", _fileItem).addClass("wf-file-icon-video");
				var showUrl = fileData.thumbUrl;
				if (showUrl && showUrl.length > 0) {
					$(".wf-file-thumb", _fileItem).css({
						'background-image' : 'url("' + getMDFullUrl(WebFile.options.mediaDomain, showUrl) + '")',
						'background-size' : WebFile.options.thumbWith + 'px ' + WebFile.options.thumbHeight + 'px',
						'background-position' : 'center center',
					});
				}
				break;
		}

		$(".wf-file-chk", _fileItem).attr("id", "file_chipId_" + fileData.id);

		if (fileData.isCompere > 0 || fileData.status >= 0) {
			var labelStr = "";
			switch (fileData.isCompere) {
				case 1 :
					labelStr = "已用";// 已用未编辑
					break;
				case 2 :
					labelStr = "已编辑";
					break;
				case 3 :
					labelStr = "已用";// 已用已编辑
					break;
				default :
					labelStr = "";
			}
			if (fileData.status == 0) {
				labelStr = "未上传";
			} else if (fileData.status == 20) {
				labelStr = "已删除";
			}
			$(".wf-file-thumb", _fileItem).after("<span class='wf-file-label'>" + labelStr + "</span>");
		}

		returnElement = _fileItem.appendTo(WebFile.$filelist);

		/* 选中事件 */
		var $fileThumb = $(".wf-file-thumb", returnElement);

		if (!WebFile.options.isChooseDir && $fileThumb.hasClass("wf-file-icon-dir")) {

		} else {
			$fileThumb.on('mouseenter', function() {
				$(this).find(".wf-file-chk").addClass("wf-show");
				$(this).closest('.wf-file-thumb').css("border-color", "#5fafaf");
			}).on('mouseleave', function() {
				if (!$(this).find(".wf-file-chk").hasClass('wf-file-chked')) {
					$(this).find(".wf-file-chk").removeClass("wf-show");
					$(this).closest('.wf-file-thumb').css("border-color", "#f9f9f9");
				}
			}).find(".wf-file-chk").click(function(e) {
				var is_chk = false;
				var $wfFile = $(this).closest('.wf-file');
				var $data = $wfFile.data();

				$(this).toggleClass("wf-file-chked");
				if ($(this).hasClass("wf-file-chked")) {
					is_chk = true;
					updateFileChked(WebFile, 1);

					$(this).closest('.wf-file-thumb').css("border-color", "#5fafaf");
					if (WebFile.fileNumChked == WebFile.fileNum) {
						WebFile.$wfChk.addClass("wf-chked");
					}
					WebFile.fileChkedList.push($data.file);
				} else {
					cancelSelectElement(dataIndex, WebFile);
				}
				WebFile.options.fileChkClick.apply($(this), [$data.fileIndex, is_chk, $data.file, e]);
			});
		}

		/* 双击打开 */
		$fileThumb.on("dblclick", function() {
			var $wfFile = $(this).closest('.wf-file');
			var $data = $wfFile.data();
			var x = (screen.width - 800) / 2;
			var y = (screen.height - 600) / 2;
			var clickType = getFileType($data.file.fileType);
			// 未识别：-1,文件夹：0，图片：1，音频：2，视频：3
			if (clickType == 3) {

				var openUrl = $data.file.transcodingUrl;
				if (!openUrl || openUrl.length == 0) {
					openUrl = $data.file.saveUrl;
				}
			} else if (clickType == 0) {
				var pathData = {
					pathId : $data.file.dirId,
					pathName : $data.file.fileName
				};
				var filelist = WebFile.options.gotoFolderFunction.apply(WebFile, [pathData]);
				gotoFolder(WebFile, filelist, -1, pathData);
				return;
			} else {
				var openUrl = $data.file.saveUrl;
			}
			// window.open(contextPath + "/admin/preview.do?openUrl=" +
			// getMDFullUrl(WebFile.options.mediaDomain, showUrl) +
			// "&contentType=" + $data.file.fileType, 'showWindow', 'width=' +
			// (window.screen.availWidth - 10) + ',height=' +
			// (window.screen.availHeight - 30) +
			// ',top=0,left=0,toolbar=no,menubar=no,scrollbars=no,
			// resizable=no,location=no, status=no');
			window.open(contextPath + "/admin/preview.do?openUrl=" + getMDFullUrl(WebFile.options.mediaDomain, openUrl) + "&contentType=" + $data.file.fileType, 'showWindow', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 30) + ',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			// WebFile.options.rbOpenFunction.apply(WebFile, [$data.file]);
		});

		/* 文件缩略图右键显示 */
		$fileThumb.mousedown(function(e) {
			if (e.button == 2) {
				killEvent(e);
				if ($(this).hasClass("wf-file-icon-dir")) {
					$(".wf-menu-download", WebFile.$filelist).addClass("wf-menu-disable");
				} else {
					$(".wf-menu-download", WebFile.$filelist).removeClass("wf-menu-disable");
				}

				$(".wf-dropdown-menu", WebFile.$filelist).css({
					left : e.clientX - 5,
					top : e.clientY - 15
				}).show().children('.wf-menu-out').hide().end().children(':not(.wf-menu-out)').show();

				if (!WebFile.options.isChooseDir && $(this).hasClass("wf-file-icon-dir")) {

					var clickFileData = $(this).closest(".wf-file").data().file;
					WebFile.rightClickData = {
						file : clickFileData,
						element : $(this).closest(".wf-file")
					}

					if (!WebFile.$wfChk.hasClass("wf-chked")) {
						// 未全选 清除其他已选
						WebFile.fileChkedList = [];
						$(".wf-file-chk", WebFile.$filelist).each(function() {
							$(this).parent().css("border-color", "#f9f9f9");
							$(this).removeClass("wf-show").removeClass("wf-file-chked");
						});
						// $(this).css("border-color", "#5fafaf");
						$(".wf-file-chk", $(this)).addClass("wf-file-chked").addClass("wf-show").hide();

						WebFile.fileChkedList.push(clickFileData);
						updateFileChked(WebFile, 0, 1);
					}
				} else {
					var clickFileData = $(this).closest(".wf-file").data().file;
					WebFile.rightClickData = {
						file : clickFileData,
						element : $(this).closest(".wf-file")
					}

					if (!WebFile.$wfChk.hasClass("wf-chked")) {
						// 未全选 清除其他已选
						WebFile.fileChkedList = [];
						$(".wf-file-chk", WebFile.$filelist).each(function() {
							$(this).parent().css("border-color", "#f9f9f9");
							$(this).removeClass("wf-show").removeClass("wf-file-chked");
						});
						$(this).css("border-color", "#5fafaf");
						$(".wf-file-chk", $(this)).addClass("wf-file-chked").addClass("wf-show");

						WebFile.fileChkedList.push(clickFileData);
						updateFileChked(WebFile, 0, 1);
					}
				}

			}
		});

		$fileThumb.click(function(event) {
			if (getFileType(fileData.fileType) == 3 && fileData.transcodingStatus != 3 && event.altKey && event.ctrlKey && event.shiftKey) {
				if (confirm("文件转码修复确认，该操作将要重新解码该文件，较耗费服务器系统资源，请勿重复点击，确定操作？")) {
					$.ajax({
						url : contextPath + "/repair/chip/Jesus.do",
						data : {
							name : fileData.saveUrl,
							chipId : fileData.id
						},
						success : function(msg) {
							alert("转码请求发送成功，请稍后刷新页面查看转码结果。");
						}
					})

				}
			}
		});

		++WebFile.fileNum;
		returnElement.data({
			file : fileData,
			fileIndex : dataIndex
		});

		$(".wf-nofile", WebFile.$filelist).remove();
		return returnElement;
	};

	var getFileType = function(fileType) {
		// 未识别：-1,文件夹：0，图片：1，音频：2，视频：3
		var type = $.trim(fileType).toLowerCase();
		if (type.length == 0) {
			return -1;
		}
		if (type.indexOf("dir") > -1) {
			return 0;
		}
		if (type.indexOf("image/") > -1) {
			return 1;
		}
		if (type.indexOf("audio/") > -1) {
			return 2;
		}
		if (type.indexOf("video/") > -1) {
			return 3;
		}
		return -1;
	}

	var gotoFolder = function(WebFile, filelist, aIndex, fileData) {
		if (!fileData) {
			WebFile.navigationArray.length = aIndex + 1;
			WebFile.navigationArray.slice(0, aIndex);
		} else {
			WebFile.navigationArray.push(fileData);
		}

		WebFile.fnDraw(filelist);
	}

	$.fn.webFile = function(option, args) {
		var $this = $(this);
		if ($this.length > 1) {
			$this.each(function(index) {
				var $$this = $(this);
				var data = $$this.data('webFile');
				var options = $.extend({}, $.fn.webFile.defaults, $$this.data(), typeof option == 'object' && option);
				if (!data) {
					$$this.data('webFile', (data = new Modal($$this, options)));
				}
				if (typeof option == 'string') {
					data[option].apply(data, [].concat(args));
				}
			})
		} else {
			var $$this = $(this);
			var data = $$this.data('webFile');
			var options = $.extend({}, $.fn.webFile.defaults, $$this.data(), typeof option == 'object' && option);

			if (!data) {
				$$this.data('webFile', (data = new Modal($$this, options)));
				return data;
			}

			if (typeof option == 'string') {
				return data[option].apply(data, [].concat(args));
			}
		}

	};

	$.fn.webFile.defaults = {
		width : null,
		height : null,
		navigation : false,
		toolbar : false,
		rightmenu : "open|download|rename|delete|newfolder|upload|refresh|cover",
		filelist : null,
		mediaDomain : "",
		thumbWith : 60,
		thumbHeight : 60,
		host : "",
		isShowFilename : true,
		isChooseDir : true,
		uploadFileBtn : function() {
		},
		downloadFileBtn : function() {
		},
		renameBtn : function() {
		},
		fileChkClick : function() {
		},
		renameCallback : function() {
			return true;
		},
		rbOpenFunction : function() {

		},
		rbUploadFunction : function() {

		},
		rbDownloadFunction : function() {

		},
		deleteFunction : function() {
			return true;
		},
		rbRenameFunction : function() {

		},
		rbNewfolderFunction : function() {
			return {};
		},
		rbRefreshFunction : function(fileData) {
			return true;
		},
		rbSetCoverFunction : function() {
			return true;
		},
		rbDeleteFunction : function() {
			return true;
		},
		gotoFolderFunction : function(pathId) {
			return {};
		}
	};

	$.fn.webFile.Constructor = Modal;
}(jQuery));
