webFile(){
	properties:
		
		内建属性：
			fileNum:文件个数,
			fileChkedList:已选文件数据数组
			fileNumChked:已选文件个数
			rightClickData:右键点击{file:文件数据，element:页面元素}
			页面元素（Jquery对象）
				$element：webFile调用元素
				$container: 容器
				$navigation:导航栏
				$wfChk:全选按钮
				$toolbar:工具栏
				$filelist:文件列表元素
			
	
		自定义属性：
			width(int):容器宽,
			height(int):容器高,
			navigation (boolean): 是否需要导航栏,
			toolbar (boolean): 是否需要工具栏,
			rightmenu (boolean): 是否需要右键菜单,
			isShowFilename (boolean): 是否显示文件名,
			filelist (Array): 容器初始化数据,
			mediaDomain (String): 文件存储地址的 主机地址,
		
		元素存储属性：
			文件元素(.wf-file):{file:当前文件数据，fileIndex:当前文件序号}
		
	


	function:
		初始化函数：
			init():	启动入口，
			
			_initContent():	页面内容初始化，
			
			_initPrototype():	自定义内容初始化
				初始化数据结构：file{ id,fileType,fileName,saveUrl}
			
			_initEvent():	事件初始化（工具栏按钮、右键按钮）
		
		功能函数：
			getFile(index);
				按序号获取文件,序号从0开始
			getSelectData();
				获取选中file数据，返回object
			getFiles();
				获取所有file数据，返回array
			delFile(index);
				按序号删除,序号从0开始
			
		自定义函数：
			uploadFileBtn();
				工具栏上传按钮点击事件
			downloadFileBtn();
				工具栏下载按钮点击事件
			deleteFileBtn();
				工具栏删除按钮点击事件
			
			
			renameBtn();
				工具栏重命名按钮点击事件
			renameCallback();
				命名确认回调
			
			fileChkClick();
			
			11.29之前更新
		
		
	
}