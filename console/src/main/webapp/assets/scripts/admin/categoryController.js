"use strict";

kanConsole.controller('categoryController', function($scope, $compile) {
    $scope.categoryInfo = {};

    $scope.editCategory = function(categoryId){
        console.log(categoryId);
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/category/cateogry_info.do",
            data: "categoryId=" + categoryId,
            dataType: "json",
            async: false,
            success: function(msg){
                $("#addCategoryForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addCategoryForm").hide();
                $scope.categoryInfo = msg.data.categoryInfo;
                $('#addAdminCategoryModal').modal('show');
            }
        });
    };

    $scope.delCategory = function(categoryId){
        console.log(categoryId);
        console.log("==========");
        if (!confirm("确认要删除该分类吗？")) {
            return;
        }
        $.ajax({
            url: contextPath + "/admin/category/cateogry_delete.do",
            type: "POST",
            data: {
                "categoryId": categoryId
            },
            async: false,
            success: function (msg) {
                stopPop();
                if (msg.code == 200) {
                    popStatus(1, "删除成功！", 1, "", true);
                    dataTable.fnDraw();
                } else {
                    popStatus(4, msg.data.error, 2, "", true);
                }
            },
            beforeSend: function () {
                popStatus(3, "后台处理中，请稍后...", -1, "", true);
            }
        });
    }

    $(function(){
        $('#categoryList').bind('draw.dt',function(){
            var linkFn = $compile($('#categoryList'));
            linkFn($scope);
        });
    });
});

