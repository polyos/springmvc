"use strict";

kanConsole.controller('powerController', function($scope, $compile) {

	var roleTable;
	var privilegeTable;
	var versionValidator;
	var isEditParent = false;

	$(function() {

		$scope.pGroup = {};
		$("#privilegeSearchForm select option").each(function() {
			var thisId = parseInt($(this).val());
			if (!isNaN(thisId)) {
				$scope.pGroup[thisId] = $.trim($(this).text());
			}
		})

		//加1 插件
		$('#spinner_role,#spinner_privi').spinner({
			value : 0,
			min : 0,
			max : 10
		});
		dataTableInit();
		//tab 初始化
		$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
			var tabIndexId = $(e.target).attr("tabIndexID");
			if (tabIndexId == 0) {
				roleSearch();
			} else {
				privilegeSearch();
			}
		})

		$("#roleSearchBtn").click(roleSearch);
		$("#privilegeSearchBtn").click(privilegeSearch);
		$("#saveRolePriviBtn").click(savePrivilegeOfRole);

		$scope.priviIdMap = {};
		$scope.priviIdList = [];

		$scope.menuIdMap = {};
		$scope.menuIdList = [];

	});

	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if (phase == '$apply' || phase == '$digest') {
			if (fn && (typeof (fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	var jsTreeSelect = function(jsTreeElem, selectArray) {
		if (!jsTreeElem) {
			popStatus(4, "jsTree 未初始化", 2, "", true);
			return;
		}
		jsTreeElem.deselect_all();
		jsTreeElem.close_all();
		jsTreeElem.select_node(selectArray);
	}

	var roleSearch = function() {
		var param = $("#roleSearchForm").serialize();
		var oSettings = roleTable.fnSettings();
		oSettings._iDisplayStart = 0;
		oSettings.sAjaxSource = contextPath + "/power/role_data.do?" + param;
		roleTable.fnClearTable(0);
		roleTable.fnDraw();
	}

	var privilegeSearch = function() {
		if (!privilegeTable) {
			privilegeTableInit();
			return;
		}
		var param = $("#privilegeSearchForm").serialize();
		var oSettings = privilegeTable.fnSettings();
		oSettings._iDisplayStart = 0;
		oSettings.sAjaxSource = contextPath + "/privi/privilege/page.do?" + param;
		privilegeTable.fnClearTable(0);
		privilegeTable.fnDraw();
	}

	// 初始化datatable
	var dataTableInit = function() {
		roleTable = $("#roleTable").dataTable({
			"bServerSide" : true,
			'iDisplayLength' : 20,
			"bPaginate" : true,
			"bProcessing" : true,
			"bAutoWidth" : true,
			"sPaginationType" : "bootstrap",
			'bLengthChange' : false,
			"bSort" : false,
			"bFilter" : false,
			"oLanguage" : {
				"sUrl" : contextPath + "/assets/plugins/datatables/cn_lang.txt"
			},
			"sAjaxSource" : contextPath + "/power/role_data.do",
			"aoColumns" : [{
				"mData" : "id"
			}, {
				"mData" : "roleName"
			}, {
				"mData" : "rolePower"
			}, {
				"mData" : function() {
					var rowNum = arguments[3].row;
					return '<a class="link-btn" ng-click="editRole(' + rowNum + ')">编辑</a>';
				}
			}],
			"fnRowCallback" : function(nTr, data, row, sortRow) {
			}
		});

		$('#roleTable').bind('draw.dt', function() {
			var linkFn = $compile($('#roleTable'));
			linkFn($scope);
		});
	}

	$scope.editRole = function(rowNum) {
		$scope.updateRole = roleTable.fnGetData(rowNum);
		$("input[name=roleName]", $("#roleForm")).val($scope.updateRole.roleName);
		$('#spinner_role').spinner("value", $scope.updateRole.rolePower);

		fnInitRoleTree($scope.updateRole.id);
		$("#roleModal").modal('show');
	}

	var getPowerOfRole = function(roleId) {
		$.ajax({
			url : contextPath + "/power/role_power.do",
			type : "get",
			data : {
				roleId : roleId
			},
			async : false,
			success : function(msg) {
				$scope.priviIdList = msg;
			}
		})
		$scope.priviIdMap = {};
		// list-->map
		for (var i = 0; i < $scope.priviIdList.length; i++) {
			$scope.priviIdMap[$scope.priviIdList[i]] = $scope.priviIdList[i];
		}
	}

	// 初始化权限树
	var fnInitRoleTree = function(roleId) {
		getPowerOfRole(roleId);
		var element = $("#roleTree");
		if (element.data().jstree) {
			jsTreeSelect(element.data().jstree, $scope.priviIdList);
			return;
		}
		element.jstree({
			'plugins' : ["checkbox", "types"],
			'core' : {
				"themes" : {
					"responsive" : true
				},
				'data' : function(node, cb) {
					$.ajax({
						url : contextPath + "/privi/privilege/list.do",
						type : "get",
						success : function(msg) {
							// 初始化
							var priviTree = [];
							for ( var item in $scope.pGroup) {
								priviTree.push({
									"id" : "p_" + item,
									"text" : $scope.pGroup[item],
									"children" : [],
									"state" : {
										"opened" : true
									},
								})
							}

							for (var i = 0; i < msg.length; i++) {
								var node = msg[i];

								for (var j = 0; j < priviTree.length; j++) {
									if (priviTree[j].id == ("p_" + node.groupId)) {

										if ($scope.priviIdMap[node.id]) {
											node.selected = true;
										} else {
											node.selected = false;
										}

										priviTree[j].children.push({
											id : node.id,
											text : node.privilegeName,
											icon : "fa fa-file icon-state-warning icon-lg",
											state : {
												selected : node.selected
											}
										})
										break;
									}
								}
							}
							cb(priviTree);
						}
					})
				}
			},
			"types" : {
				"default" : {
					"icon" : "fa fa-folder icon-state-warning icon-lg"
				},
				"file" : {
					"icon" : "fa fa-file icon-state-warning icon-lg"
				}
			}
		});
	}

	// //////////////////********权限********/////////////////////////////////////////

	var privilegeTableInit = function() {
		if (privilegeTable) {
			return;
		}
		privilegeTable = $("#privilegeTable").dataTable({
			"bServerSide" : true,
			'iDisplayLength' : 20,
			"bPaginate" : true,
			"bProcessing" : true,
			"bAutoWidth" : true,
			"sPaginationType" : "bootstrap",
			'bLengthChange' : false,
			"bSort" : false,
			"bFilter" : false,
			"oLanguage" : {
				"sUrl" : contextPath + "/assets/plugins/datatables/cn_lang.txt"
			},
			"sAjaxSource" : contextPath + "/power/role_data.do",
			"aoColumns" : [{
				"mData" : "id"
			}, {
				"mData" : function(obj) {
					return $scope.pGroup[obj.groupId];
				}
			}, {
				"mData" : "privilegeName"
			}, {
				"mData" : "privilegeWeight"
			}, {
				"mData" : function() {
					var rowNum = arguments[3].row;
					return '<a class="link-btn" ng-click="editPrivilege(' + rowNum + ')">编辑</a>';
				}
			}]
		});
		$('#privilegeTable').bind('draw.dt', function() {
			var linkFn = $compile($('#privilegeTable'));
			linkFn($scope);
		});
	}

	// 编辑权限
	$scope.editPrivilege = function(rowNum) {
		$scope.updatePrivilege = privilegeTable.fnGetData(rowNum);

		$("select[name=groupId]", $("#priviModal")).val($scope.updatePrivilege.groupId);
		$("input[name=roleName]", $("#priviModal")).val($scope.updatePrivilege.privilegeName);
		$('#spinner_privi').spinner("value", $scope.updatePrivilege.privilegeWeight);
		fnInitPriviTree($scope.updatePrivilege.id);

		$("#priviModal").modal("show");
	}

	var fnInitPriviTree = function(priviId) {
		initIdListOfPrivi(priviId);
		var element = $('#priviModalTree');

		if (element.data().jstree) {
			jsTreeSelect(element.data().jstree, $scope.menuIdList);
			return;
		}
		element.jstree({
			'plugins' : ["checkbox", "types"],
			'core' : {
				"themes" : {
					"responsive" : true
				},
				'data' : function(node, cb) {
					$.ajax({
						url : contextPath + "/power/all_url.do",
						type : "get",
						success : function(msg) {
							cb(tidyMenus(msg));
						}
					})
				}
			},
			"types" : {
				"default" : {
					"icon" : "fa fa-folder icon-state-warning icon-lg"
				},
				"file" : {
					"icon" : "fa fa-file icon-state-warning icon-lg"
				}
			}
		});
	}

	/* 整理资源 */
	var tidyMenus = function(menus) {
		var nodes = [];
		var rootTree = {
			"id" : 0,
			"state" : {
				"opened" : true
			},
			"children" : []
		};

		// 有序链表
		for (var i = 0; i < menus.length; i++) {
			var menu = menus[i];
			var node = {
				"id" : menu.id,
				"text" : menu.name,
				"parentId" : menu.parentId,
				"children" : []
			}
			if (menu.type == 11) {
				node.icon = "fa fa-file icon-state-warning icon-lg"
			} else {
				node.icon = "fa fa-folder icon-state-success"
			}

			if ($scope.menuIdMap[menu.id]) {
				node.state = {
					selected : true,
					open : true
				}
			}
			$scope.putToTree(rootTree, node);
		}
		return rootTree;
	}

	// 树整理
	$scope.putToTree = function(tree, node) {
		if (tree.id == node.parentId) {
			if (!tree.children) {
				tree.children = new Array();
			}
			tree.children.push(node);
			return true;
		} else {
			for (var i = 0; i < tree.children.length; i++) {
				if ($scope.putToTree(tree.children[i], node)) {
					return true;
				}
			}
			return false;
		}
	}

	// 获取权限下的资源列表
	var initIdListOfPrivi = function(priviId) {
		$.ajax({
			url : contextPath + "/power/role_power.do",
			type : "get",
			async : false,
			data : {
				priviId : priviId
			},
			success : function(msg) {
				$scope.menuIdList = msg;
			}
		})
		$scope.menuIdMap = {};
		// list-->map
		for (var i = 0; i < $scope.menuIdList.length; i++) {
			$scope.menuIdMap[$scope.menuIdList[i]] = $scope.menuIdList[i];
		}
	}

	// 保存角色的权限信息
	var savePrivilegeOfRole = function() {
		$("#roleModal").modal("hide");
	}
});