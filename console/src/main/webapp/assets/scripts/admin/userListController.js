"use strict";

kanConsole.controller('userListController', function($scope, $compile) {
    $scope.user = {};

    $scope.editUser = function(id){
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/get_admin_user.do",
            data: "id=" + id,
            dataType: "json",
            async: false,
            success: function(msg){
                $("#addUserForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addUserForm").hide();
                $scope.user = msg.data.adminUser;
                $('#addAdminUserModal').modal('show');
            }
        });
    };
    $(function(){
        $('#userList').bind('draw.dt',function(){
            var linkFn = $compile($('#userList'));
            linkFn($scope);
        });
    });
});

