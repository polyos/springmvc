"use strict";
kanConsole.controller('startController', function ($scope, $compile) {
    $scope.roleClick = function (value) {
        console.log(value);
        $("span", "#role_list").removeClass("active0");
        $("#role_" + value).addClass("active0");

        if (value) {
            $.ajax({
                type: "post",
                url: contextPath + "/privilege/push_system_id.do",
                data: {systemId: value},
                dataType: "json",
                async: "false",
                success: function (msg) {
                    if (msg.code == 200) {
                        location.href =contextPath +  "/privilege/jump_to_system.do";
                    }
                }
            })
        }
    }

});

