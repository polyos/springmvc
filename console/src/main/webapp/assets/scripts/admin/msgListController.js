"use strict";

kanConsole.controller('msgListController', function($scope, $compile) {
    $scope.user = {};

    $scope.readMsg = function(id,isRead){
        console.log('111');
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/products/message_is_read.do",
            data: {"messageId":id,"isRead":isRead},
            dataType: "json",
            async: false,
            success: function(msg){
                $scope.message = msg.data.message;
                $('#showMsgModal').modal('show');
            }
        });
    };
    $scope.delMsg = function(id){
        console.log('111');
        $.ajax({
            type: "POST",
            url: contextPath + "/admin/products/message_delete.do",
            data: "messageId=" + id,
            dataType: "json",
            async: false,
            success: function(msg){
                var param = $("#searchForm").serialize();
                var oSettings = dataTable.fnSettings();
                oSettings._iDisplayStart = 0;
                oSettings.sAjaxSource = contextPath + "/admin/products/message_table.do?" + param;
                dataTable.fnClearTable(0);
                dataTable.fnDraw();
            }
        });
    };
    $(function(){
        $('#msgList').bind('draw.dt',function(){
            var linkFn = $compile($('#msgList'));
            linkFn($scope);
        });
    });
});

