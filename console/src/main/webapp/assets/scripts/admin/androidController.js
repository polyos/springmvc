"use strict";
/**
 * Created by Administrator on 2015/10/22.
 */

kanConsole.controller('androidController', function ($scope, $compile) {

    $scope.publishModal = function () {
        $scope.androidVersion = {};
        validator.resetForm();
        $(".has-error").removeClass("has-error");
        $scope.appName = "";
        $("#radioAndroid").attr('checked', true);
        $("#radioFormal").attr('checked', true);
        $("#installUrl").val('');
        ue.setContent('');

        $("#appModal").modal('show');
    }

    $scope.saveApp = function () {
        if ($("#appForm").valid()) {
            var content = ue.getContent();
            $("#changelog").val(content);
            var id = $("#id").val();
            if (id) {
                $.ajax({
                    type: "post",
                    url: contextPath + "/admin/update_version.do",
                    data: $("#appForm").serialize(),
                    dataType: "json",
                    async: false,
                    success: function (msg) {
                        $("#appModal").modal('hide');

                        var oSettings = dataTable.fnSettings();
                        oSettings.sAjaxSource = contextPath + "/admin/version_paginator.do";
                        dataTable._fnDraw(oSettings);
                    }
                })
            } else {
                var url = $("#installUrl").val();
                if (!url) {
                    if (confirm("您还没有上传APP文件，确定要提交吗？")) {
                        appAdd();
                    }
                } else {
                    appAdd();
                }
            }
        }
    }

    var appAdd = function () {
        $.ajax({
            type: "post",
            url: contextPath + "/admin/add_version.do",
            data: $("#appForm").serialize(),
            dataType: "json",
            async: false,
            success: function (msg) {
                if(msg.code == 200) {
                    $("#appModal").modal('hide');
                    var oSettings = dataTable.fnSettings();
                    oSettings.sAjaxSource = contextPath + "/admin/version_paginator.do";
                    dataTable._fnDraw(oSettings);
                } else{
                    popStatus(4,msg.data.error, 2, '', false);
                }
            }
        })
    }

    $scope.editInfo = function (id) {
        $scope.androidVersion = {};
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/version_info.do",
            data: {id: id},
            dataType: "json",
            async: false,
            success: function (msg) {
                //$("#addUserForm").validate().resetForm();
                //$(".has-error").removeClass("has-error");
                //$('.alert-danger', "#addUserForm").hide();
                $scope.androidVersion = msg.data.version;
                if (msg.data.version) {
                    if (msg.data.version.platform == 1) {
                        $("#radioIos").attr('checked', true);
                    } else {
                        $("#radioAndroid").attr('checked', true);
                    }

                    if (msg.data.version.type == 1) {
                        $("#radioFormal").attr('checked', true);
                    } else {
                        $("#radioTest").attr('checked', true);
                    }
                    ue.setContent(msg.data.version.changeLog);
                }
                $('#appModal').modal('show');
            }
        });
    };

    $("#uploadFile_app").live('change', function () {
        if ($(".info", ".fileupload-preview").size() > 0) {
            return;
        }
        if ($(this).val() != "") {
            $.ajaxFileUpload
            (
                {
                    url: contextPath + '/upload_file_big.do?dir=appSoft',
                    secureuri: false,
                    fileElementId: this.id,
                    dataType: 'json',
                    success: function (data, status) {
                        $(this).val("");
                        stopPop();
                        $("#installUrl").val(data.data.uploadFile.path);
                        $scope.appName = data.data.uploadFile.fileName;
                        $scope.$apply();
                        //$("#appName").val(data.data.uploadFile.fileName);
                    },
                    before: function () {
                        popStatus(3, '正在上传。。。', -1, '', true);
                    }
                }
            );
        }
    });

    $('#appList').bind('draw.dt', function () {
        var linkFn = $compile($('#appList'));
        linkFn($scope);
    });

});