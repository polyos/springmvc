"use strict";

kanConsole.controller('productsController', function($scope, $compile) {
    $scope.productInfo = {};
    $scope.skin = {};

    $scope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    var categoryListInit = function() {
        $.ajax({
            url : contextPath + "/admin/category/cateogry_list.do",
            type : "get",
            async : false,
            success : function(msg) {
                if (msg.code == 200) {
                    $scope.categoryList = msg.data.categoryList;
                    $scope.categoryList_modal = msg.data.categoryList;
                    $scope.productInfo.categoryId = $scope.categoryList_modal[0].cateogryId;
                    $scope.safeApply();
                } else {
                    popStatus(4, msg.data.error, 2, "", true);
                }
            }
        });
    }

    $(".btn-submit").live('click', function (){
        var _this = $(this);
        var galleryId = _this.parent().prevAll().find("input[ name='galleryId']").val();
        var imgUrl = _this.parent().prevAll().find("input[ name='imgUrl']").val();
        var desc = _this.parent().prevAll().find("textarea").val();
        var productId= $scope.productInfo.productId;
        if (imgUrl && productId){
            $.ajax({
                type: "POST",
                url:contextPath + "/admin/products/gallery_add.do",
                data : {
                    "galleryId":galleryId,
                    "productId":productId,
                    "imgUrl":imgUrl,
                    "desc":desc
                },
                dataType: "json",
                async: false,
                success: function(msg){
                    if (msg.code == 200) {
                        _this.parent().prevAll().find("input[name='galleryId']").val(msg.data.galleryId);
                    } else {
                        popStatus(4, msg.data.error, 2, "", true);
                    }
                }
            });
        } else {
            popStatus(1, "设备信息不完整！", 1, "", true);
        }
    })

    $(".btn-delete").live('click', function (){

        var _this = $(this);
        var galleryId = _this.parent().prevAll().find("input[ name='galleryId']").val();
        if (galleryId){
            $.ajax({
                type: "POST",
                url:contextPath + "/admin/products/gallery_delete.do",
                data : {
                    "galleryId":galleryId
                },
                dataType: "json",
                async: false,
                success: function(msg){
                    if (msg.code == 200) {

                    }
                }
            });
        }
        _this.parent().parent().remove();
        popStatus(1, "删除成功！", 1, "", true);
    })

    // 初始化上传组件
    var initUpload = function() {
        $("#uploadFile_one").each(function() {
            var _this = $(this);
            var out = _this.closest(".upload-out");
            _this.fileupload({
                dataType : "json",
                type : 'POST',
                url : contextPath + "/admin/upload_file.do?dir=/products",
                start : function(e, data) {
                    $('.progress-bar', out).css('width', 0 + '%');
                    $(".progress-bar-div", out).fadeIn(200);
                },
                done : function(e, data) {
                    var msg = data.result;
                    if (msg.code == 200) {
                        $("input[type=hidden]", out).each(function() {
                            var key = $(this).attr("name");
                            $scope.skin.bottomImgUrl = msg.data.uploadFile.path;
                            $scope.skin.bottomImgId = msg.data.uploadFile.uploadFileId;
                        })
                        $scope.safeApply();
                        //console.log(msg.data.uploadFile);
                        $(".fileupload-preview", out).text(msg.data.uploadFile.fileName);
                    } else {
                        $(".fileupload-preview", out).text(msg.error);
                    }
                },
                progressall : function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100 * 0.5, 10);
                    //console.log(progress);
                    if (progress == 50) {
                        var progressGo = function(widthNum) {
                            if (widthNum < 100) {
                                $('.progress-bar', out).css('width', (++widthNum) + '%');
                                setTimeout(function() {
                                    progressGo(widthNum);
                                }, 10);
                            } else {
                                $(".progress-bar-div", out).fadeOut(200);
                                return;
                            }
                        }
                        progressGo(50);
                    }
                    $('.progress-bar', out).css('width', progress + '%');
                },
            })
        })
    }

    //上传文件
    var uploadFile = function () {
        $("#uploadFile").fileupload({
            dataType: "json",
            type: 'POST',
            url: contextPath + "/admin/upload_file.do?dir=media",
            start: function (e, data) {
            },
            done: function (e, data) {
                var uploadfile = data.result.data.uploadFile;

                var galleryId=" <td><input type='hidden' name='galleryId' value=''><input type='hidden' name='imgUrl' value='"+ uploadfile.path +"'></td>"
                var img ="<td><img style='width:100px;height: 100px;' src='"+uploadfile.path+"'></td>";
                var text ="<td><textarea  rows='5' cols='70' name='desc'></textarea></td>";
                var btn ="<td> <a href='javascript:;' class='btn btn-circle btn-submit purple'> <i class='fa fa-edit'></i> 提交 </a></td>";
                var delBtn ="<td> <a href='javascript:;' class='btn btn-circle btn-delete default'> <i class='fa fa-edit'></i> 删除 </a></td>";

                $("<tr></tr>").appendTo($("#gallery_table"));
                $(galleryId).appendTo($("#gallery_table>tr"));
                $(img).appendTo($("#gallery_table>tr"));
                $(text).appendTo($("#gallery_table>tr"));
                $(btn).appendTo($("#gallery_table>tr"));
                $(delBtn).appendTo($("#gallery_table>tr"));
            },
            progressall: function (e, data) {
            }
        });
    };

    //tab切换时，初始化 ueeditor
    $('.nav-tabs').delegate('li','click',function () {
        if($(this).index() ==1){
            ue = UE.getEditor('editor_benefit', {
                initialFrameWidth : 800,
                initialFrameHeight : 500,
                saveInterval : 10000,
                autoFloatEnabled : false
            });
            ue.ready(function(){
                // ue.setContent($("#editor_benefit").text());
                //console.log($scope.productInfo);
                ue.setContent($scope.productInfo.benefit);
            })
        }else if ($(this).index() ==2){
            ue = UE.getEditor('editor_technical', {
                initialFrameWidth : 800,
                initialFrameHeight : 500,
                saveInterval : 10000,
                autoFloatEnabled : false
            });
            ue.ready(function(){
                //console.log($scope.productInfo);
                ue.setContent($scope.productInfo.technicalData);
            })
        }else if ($(this).index() ==0) {
            ue = UE.getEditor('editor_overview', {
                initialFrameWidth : 800,
                initialFrameHeight : 500,
                saveInterval : 10000,
                autoFloatEnabled : false
            });
            ue.ready(function(){
                //console.log($scope.productInfo);
                ue.setContent($scope.productInfo.overview);
            })
        }
    });

    $scope.editProduct = function(productId){
        //console.log(productId);
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/products/products_info.do",
            data: "productId=" + productId,
            dataType: "json",
            async: false,
            success: function(msg){
                //$("#addProductsForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addProductsForm").hide();
                $scope.productInfo = msg.data.productInfo;
                $scope.skin.bottomImgUrl = msg.data.productInfo.productImg;
                $scope.skin.bottomImgId = msg.data.productInfo.imgId;
                //console.log("====");
                //console.log( $scope.skin);

                var galleryList = msg.data.productInfo.galleryList;
                $("#gallery_table").empty();
                if (galleryList){
                    for (var i=0;i<galleryList.length;i++){
                        var gallery = galleryList[i];
                        var galleryId=" <td><input type='hidden' name='galleryId' value='"+gallery.id+"'><input type='hidden' name='imgUrl' value='"+ gallery.imgPath +"'></td>"
                        var img ="<td><img style='width:100px;height: 100px;' src='"+gallery.imgPath+"'></td>";
                        var text ="<td><textarea  rows='5' cols='70' name='desc'>" + gallery.description +"</textarea></td>";
                        var btn ="<td> <a href='javascript:;' class='btn btn-circle btn-submit purple'> <i class='fa fa-edit'></i> 提交 </a></td>";
                        var delBtn ="<td> <a href='javascript:;' class='btn btn-circle btn-delete default'> <i class='fa fa-edit'></i> 删除 </a></td>";

                        var tr = $("<tr></tr>");
                        $(galleryId).appendTo(tr);
                        $(img).appendTo(tr);
                        $(text).appendTo(tr);
                        $(btn).appendTo(tr);
                        $(delBtn).appendTo(tr);
                        tr.appendTo($("#gallery_table"));
                    }
                }

                $scope.safeApply();
                $('#addAdminProductModal').modal('show');
                $('.nav-tabs li:first').click();
            }
        });
    };

    $scope.addOverview = function(){
        var content = ue.getContent();
        var productId =$scope.productInfo.productId;
        var productName =$("#productName").val();
        var categoryId = $scope.productInfo.categoryId;
        var imgUrl = $scope.skin.bottomImgUrl;
        var imgId = $scope.skin.bottomImgId;
        if (!content){
            $('.alert-danger', "#addProductsForm").show().children("span").html('内容不能为空');
            return;
        }

        $.ajax({
            type: "POST",
            url: contextPath + "/admin/products/products_modify.do",
            data : {
                "overview" : content,
                "productId" : productId,
                "categoryId" : categoryId,
                "productName" : productName,
                "productImg":imgUrl,
                "imgId":imgId,
                "type" : 0
            },
            dataType: "json",
            async: false,
            success: function(msg){
                //$("#addProductsForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addProductsForm").hide();
                $scope.productInfo = msg.data.productInfo;
                $scope.safeApply();
            }
        });
    };

    $scope.addBenefit = function(){
        var content = ue.getContent();
        var productId =$scope.productInfo.productId;
        var productName =$("#productName").val();
        var categoryId = $scope.productInfo.categoryId;
        var imgUrl = $scope.skin.bottomImgUrl;
        var imgId = $scope.skin.bottomImgId;
        if (!content){
            $('.alert-danger', "#addProductsForm").show().children("span").html('内容不能为空');
            return;
        }

        $.ajax({
            type: "POST",
            url: contextPath + "/admin/products/products_modify.do",
            data : {
                "benefit" : content,
                "productId" : productId,
                "categoryId" : categoryId,
                "productName" : productName,
                "productImg":imgUrl,
                "imgId":imgId,
                "type" : 1
            },
            dataType: "json",
            async: false,
            success: function(msg){
                //$("#addProductsForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addProductsForm").hide();
                $scope.productInfo = msg.data.productInfo;
                $scope.safeApply();
            }
        });
    };

    $scope.addTechnical = function(){
        var content = ue.getContent();
        var productId =$scope.productInfo.productId;
        var productName =$("#productName").val();
        var categoryId = $scope.productInfo.categoryId;
        var imgUrl = $scope.skin.bottomImgUrl;
        var imgId = $scope.skin.bottomImgId;

        if (!content){
            $('.alert-danger', "#addProductsForm").show().children("span").html('内容不能为空');
            return;
        }

        $.ajax({
            type: "POST",
            url: contextPath + "/admin/products/products_modify.do",
            data : {
                "technicalData" : content,
                "productId" : productId,
                "categoryId" : categoryId,
                "productName" : productName,
                "productImg":imgUrl,
                "imgId":imgId,
                "type" : 2
            },
            dataType: "json",
            async: false,
            success: function(msg){
                //$("#addProductsForm").validate().resetForm();
                $(".has-error").removeClass("has-error");
                $('.alert-danger', "#addProductsForm").hide();
                $scope.productInfo = msg.data.productInfo;

                $scope.safeApply();
            }
        });
    };

    $scope.delProduct = function(productId){

        if (!confirm("确认要删除该设备吗？")) {
            return;
        }
        $.ajax({
            url: contextPath + "/admin/products/products_delete.do",
            type: "POST",
            data: {
                "productId": productId
            },
            async: false,
            success: function (msg) {
                stopPop();
                if (msg.code == 200) {
                    popStatus(1, "删除成功！", 1, "", true);
                    dataTable.fnDraw();
                } else {
                    popStatus(4, msg.data.error, 2, "", true);
                }
            },
            beforeSend: function () {
                popStatus(3, "后台处理中，请稍后...", -1, "", true);
            }
        });
    }

    $(function(){
        categoryListInit();
        initUpload();
        uploadFile();
        $("#cateogryIdSelect").find("option").first().attr("selected", true);
        $("#cateogryId_modal").find("option").first().attr("selected", true);
        $('#productsList').bind('draw.dt',function(){
            var linkFn = $compile($('#productsList'));
            linkFn($scope);
        });
    });
});

