"user strict";
/**
 * Created by Administrator on 2015/11/7.
 */

kanConsole.controller("adController", function ($scope, $compile) {

    //新增 Btn
    $("#addAdBtn").click(function () {
        $('.alert-danger', "#addAdModal").hide().children("span").html('');
        $scope.noLiveImg = {};
        $(':input', "#addAdModal").not(':button,:submit,:reset,select').val('');
        ue.setContent('');
        $("#radioAll").attr('checked', true);
        $scope.imgShow = false;
        $scope.urlShow = true;
        $scope.contentShow = false;
        $("#addAdModal").modal('show');
    })

    $("#radioDiv input:radio").live('click', function () {
        if ($("#radioRemind").attr('checked')) {
            //富文本
            $scope.urlShow = false;
            $scope.contentShow = true;
        } else {
            $scope.urlShow = true;
            $scope.contentShow = false;
        }
        $scope.$apply();
    })

    //清空
    $("#resetSearchBtn").click(function () {
        $(':input', "#searchForm").not(':button,:submit,:reset').val('');
        var param = $("#searchForm").serialize();
        var oSettings = dataTable.fnSettings();
        oSettings._iDisplayStart = 0;
        oSettings.sAjaxSource = contextPath + "/admin/img_data.do?" + param;
        dataTable.fnClearTable(0);
        dataTable.fnDraw();
    });

    //禁用 btn
    $scope.disable = function (data) {
        if (!data) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }

        if(confirm("您确定要禁用此条广告吗?")) {
            updateStatus(data, 10);
        }
        /*$.ajax({
         type: "post",
         url: contextPath + "/admin/update_status.do",
         data: {id: data, status: 0},
         dataType: "json",
         async: false,
         success: function (msg) {
         if (msg.code == 200) {
         var param = $("#searchForm").serialize();
         var oSettings = dataTable.fnSettings();
         oSettings.ajaxSource = "/admin/img_data.do?" + param;
         dataTable._fnDraw(oSettings);
         } else {
         popStatus(4, msg.data.error, 4, "", false);
         }
         }
         })*/
    }

    //启用
    $scope.enable = function (data) {
        if (!data) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }
        if(confirm("您确定要启用此条广告吗?")) {
            $.ajax({
                type: "get",
                url: contextPath + "/admin/ad/is_have.do",
                data: {id: data},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var isHave = msg.data.isHave;
                        if (isHave == 1) {
                            if (confirm("当前位置已有广告，您确定要启用吗?")) {
                                updateStatus(data, 1);
                            }
                        } else {
                            updateStatus(data, 1)
                        }
                    } else {
                        popStatus(4, msg.data.error, 4, "", false);
                    }
                }
            })
        }
    }

    var updateStatus = function (id, status) {
        $.ajax({
            type: "post",
            url: contextPath + "/admin/ad/update_status.do",
            data: {id: id, status: status},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    var param = $("#searchForm").serialize();
                    var oSettings = dataTable.fnSettings();
                    oSettings.ajaxSource = "/admin/img_data.do?" + param;
                    dataTable._fnDraw(oSettings);
                } else {
                    popStatus(4, msg.data.error, 4, "", false);
                }
            }
        })
    }

    //删除
    $scope.delAd = function (id) {
        if (confirm("确定要删除此广告吗？")) {
            $.ajax({
                type: "post",
                url: contextPath + "/admin/ad/delAd.do",
                data: {adId: id},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        popStatus(1, "操作成功", 2, "", "");
                        var param = $("#searchForm").serialize();
                        var oSettings = dataTable.fnSettings();
                        oSettings.ajaxSource = "/admin/ad/pageData.do?" + param;
                        dataTable._fnDraw(oSettings);
                    } else {
                        popStatus(4, msg.data.error, 4, "", false);
                    }
                }
            })
        }
    }

    //修改
    $scope.updateAd = function (id) {
        $('.alert-danger', "#addAdModal").hide().children("span").html('');
        if (!id) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }

        $.ajax({
            type: "get",
            url: contextPath + "/admin/ad/getById.do",
            data: {adId: id},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    $("#addAdModal").modal('show');
                    $scope.advertisement = msg.data.ad;
                    $scope.advertisementImgPath = getFullUrl($scope.advertisement.imgPath);
                    if (msg.data.ad) {
                        if (msg.data.ad.imgPath) {
                            $scope.imgShow = true;
                        }
                        $("#startM").val(timeStamp2String(msg.data.ad.startTime, "yyyy/MM/dd"));
                        $("#endM").val(timeStamp2String(msg.data.ad.endTime, "yyyy/MM/dd"));
                    }
                    if (msg.data.ad && msg.data.ad.type == 1) {
                        $("#radioRemind").attr('checked', true);
                        $scope.contentShow = true;
                        $scope.urlShow = false;
                        ue.setContent($scope.advertisement.content);
                    } else {
                        $("#radioAll").attr('checked', true);
                        $scope.contentShow = false;
                        $scope.urlShow = true;
                    }
                } else {
                    popStatus(4, msg.data.error, 2, "", "");
                }
            }
        })

        $("#addImgModal").modal('show');


    }

    // 提交
    $scope.addImgSubmit = function () {
        var type = 0;
        var content = "";
        $("#radioDiv input:radio").each(function () {
            if ($("#radioRemind").attr('checked')) {
                type = 1;
                content = ue.getContent();
                $("#note").text(content);

            } else {
                content = $("#adUrl").val();
            }
        })

        //验证
        var s = [];
        var startM = $("#startM").val();
        var endM = $("#endM").val();
        var note = $("#note").val();
        var coverPath = $("#coverPath").val();
        var url = $("#adUrl").val();
        if (!startM) {
            s.push("<span>开始时间不能为空|</span>");
        }
        if (!endM) {
            s.push("<span>结束时间不能为空|</span>");
        }
        if (!note) {
            s.push("<span>备注不能为空|</span>");
        }
        if (!coverPath) {
            s.push("<span>图片不能为空|</span>");
        }
        if (type == 0) {
            if (!url) {
                s.push("<span>外部链接URL不能为空|</span>");
            }
        } else {
            if (!content) {
                s.push("<span>富文本不能为空|</span>");
            }
        }
        if (s.join("")) {
            $('.alert-danger', "#addAdModal").show().children("span").html(s);
            return;
        }

        $.ajax({
            type: "post",
            url: contextPath + "/admin/ad/modify.do",
            data: {
                id: $("#adId").val(), content: content, note: $("#note").val(), type: type, coverId: $("#coverId").val(),
                coverPath: $("#coverPath").val(), position: $("#position").val(), startM: $("#startM").val(), endM: $("#endM").val()
            },
            dataType: "json",
            async: false,
            success: function (msg) {
                $("#addAdModal").modal('hide');
                popStatus(1, "操作成功", 2, "", "");
                var param = $("#searchForm").serialize();
                var oSettings = dataTable.fnSettings();
                oSettings._iDisplayStart = 0;
                oSettings.sAjaxSource = contextPath + "/admin/ad/pageData.do?" + param;
                dataTable.fnClearTable(0);
                dataTable.fnDraw();
            }
        })
    }

    $('#adList').bind('draw.dt', function () {
        var linkFn = $compile($('#adList'));
        linkFn($scope);
    });
})
