/**
 * Created by Administrator on 2015/8/4.
 */

kanConsole.controller('guideController',function($scope,$compile){

    $scope.saveGuide = function() {
        var content = ue.getContent();
        $("#guide").text(content);
        $.ajax({
            type:"post",
            url:contextPath + "/admin/modify_guide.do",
            data:{id:$("#id").val(),content:content},
            dataType:"json",
            async: false,
            success: function(msg){
                if(msg.code == 200) {
                    stopPop();
                    $scope.guide = {};
                    $scope.guide = msg.data.guide;
                    popStatus(1, '提交成功', 2, '', false);
                } else {
                    stopPop();
                    popStatus(4, msg.data.error, 2, '', false);
                }
            },before: function(){
                popStatus(3, '正在上传。。。', -1, '', true);
            }
        })

    }

    $("#restBtn").click(function(){
        ue.setContent('');
    })

    $(function(){
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/get_guide.do",
            dataType: "json",
            async: false,
            success: function(msg){
                $scope.guide = msg.data.guide;
                $scope.$apply();
            }
        })
    });

})
