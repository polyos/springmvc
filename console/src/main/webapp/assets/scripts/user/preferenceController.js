/**
 * Created by Administrator on 2015/8/3.
 */

kanConsole.controller('preferenceController', function ($scope, $compile) {

    //添加/修改 modal
    $("#addBtn").click(function () {
        $(':input', "#labelsForm").not(':button,:submit,:reset').val('')
        $scope.preferenceLabel = {};
        $scope.imgShow = false;
        $(".select2-search-choice").remove();
        validate.resetForm();
        $(".has-error").removeClass("has-error");
        $("#addLabelModal").modal('show');

    })

    $scope.modify = function (id) {
        if (id) {
            //$(':input', "#labelsForm").not(':button,:submit,:reset').val('')
            $scope.preferenceLabel = {};
            $scope.imgShow = true;

            $.ajax({
                type: "GET",
                url: contextPath + "/admin/preference/getById.do",
                data: {id: id},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        $scope.preferenceLabel = msg.data.preference;
                        if (msg.data.preference) {
                            var typeIds = msg.data.preference.projectType;
                            if(typeIds) {
                                var idArray = typeIds.split(",");
                                if (idArray.length > 0) {
                                    $("#s2id_autogen1").data("select2").val(idArray);
                                }
                            }
                        }
                        $("#addLabelModal").modal('show');
                    } else {
                        popStatus(4, msg.data.error, 2, "", true);
                    }
                }
            })
        } else {
            popStatus(4, "数据不正确，请刷新页面。", 2, "", true);
        }
    }

    $("#labelSubmitBtn").click(function () {
        validate.resetForm();
        $(".has-error").removeClass("has-error");
        if ($("#labelsForm").valid()) {
            //读取 项目类型
            var uploadArray = [];
            var selectData = $("#s2id_autogen1").data("select2").data();
            if (selectData) {
                for (var i = 0; i < selectData.length; i++) {
                    uploadArray.push(parseInt(selectData[i].id));
                }
            }
            $.ajax({
                type: "POST",
                url: contextPath + "/admin/post_preference.do",
                data: {
                    labelId: $("#labelId").val(), labels: $("#labels").val(), coverPath: $("#coverPath").val(),
                    typeName: uploadArray.join(",")
                },
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = table.fnSettings();
                        oSettings._iDisplayStart = 0;
                        oSettings.sAjaxSource = contextPath + "/admin/preference_page.do?" + param;
                        table._fnDraw(oSettings);
                        $("#addLabelModal").modal('hide');
                    } else {
                        popStatus(4, msg.data.error, 2, "", true);
                    }
                }
            })
        }
    })

    $scope.disabled = function (value) {
        if (null == value) {
            alert("操作失败");
        }
        if (confirm("确认要禁用此标签吗？")) {
            $.ajax({
                type: "GET",
                url: contextPath + "/admin/preference_status.do",
                data: {id: value, status: 1},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = table.fnSettings();
                        oSettings.sAjaxSource = contextPath + "/admin/preference_page.do?" + param;
                        table._fnDraw(oSettings);

                    } else {
                        popStatus(4, msg.data.error, 2, "", true);
                    }
                }
            });
        }
    }

    $scope.enabled = function (value) {
        if (null == value) {
            alert("操作失败");
        }
        if (confirm("确认要启用此标签吗？")) {
            $.ajax({
                type: "GET",
                url: contextPath + "/admin/preference_status.do",
                data: {id: value, status: 0},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = table.fnSettings();
                        oSettings.sAjaxSource = contextPath + "/admin/preference_page.do?" + param;
                        //table.fnClearTable(0);
                        //table.fnDraw();
                        table._fnDraw(oSettings);
                    } else {
                        alert(msg.data.error);
                    }
                }
            });
        }
    }

    $('#preferenceList').bind('draw.dt', function () {
        var linkFn = $compile($('#preferenceList'));
        linkFn($scope);
    });
})