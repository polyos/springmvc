/**
 * Created by Administrator on 2015/8/3.
 */

kanConsole.controller('helpDocumentController',function($scope,$compile){

    $scope.details = function(value) {
        if(null == value) {
            alert("获取失败,请重试");
        }
        //$(':input', "#documentForm").not(':button,:submit,:reset').val('');
        $("#editDocumentModal").removeData('modal');
        $.ajax({
                type : "GET",
                url : contextPath + "/admin/document_get.do",
                data : {id: value},
                dataType : "json",
                async : false,
                success : function(msg) {
                    if (msg.code == 200) {
                        $scope.document = msg.data.document;
                        $("#editDocumentModal").modal('show');
                    } else {
                        alert(msg.data.error);
                    }
                }
            });
    }

    $scope.disabled = function (value) {
        if(null == value) {
            alert("操作失败");
        }
        if(confirm("确认要删除此文档吗？")){
            $.ajax({
                type : "GET",
                url : contextPath + "/admin/document_del.do",
                data : {id: value},
                dataType : "json",
                async : false,
                success : function(msg) {
                    if (msg.code == 200) {
                        var oSettings = dataTable.fnSettings();
                        oSettings.sAjaxSource = contextPath + "/admin/document_page.do";
                        //dataTable.fnClearTable(0);
                        //dataTable.fnDraw();
                        dataTable._fnDraw(oSettings);
                    } else {
                        alert(msg.data.error);
                    }
                }
            });
        }
    }

    //$scope.$watch('document.content',function(newVal,oldVal,scope){
    //    if(newVal == null || newVal == ""){
    //        $("#documentSubmitBtn").addClass('disabled')
    //    } else {
    //        $("#documentSubmitBtn").removeClass('disabled');
    //    }
    //})

    $('#helpDocumentList').bind('draw.dt',function(){
        var linkFn = $compile($('#helpDocumentList'));
        linkFn($scope);
    });
})