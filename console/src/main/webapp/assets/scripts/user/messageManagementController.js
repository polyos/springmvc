"use strict";
/**
 * Created by Administrator on 2015/7/30.
 */

kanConsole.controller('messageManagementController', function ($scope, $compile) {
    $scope.val = 0;

    $scope.msgModal = function () {
        $(':input', "#msgForm").not(':button,:submit,:reset').val('');
        $('.alert-danger', "#addModal").hide().children("span").html('');
        $("#radioAll").attr('checked', true);
        $("#msgSubmitBtn").addClass('disabled');
        $("#select2-chosen-2").text("请输入项目名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $("#select2-chosen-6").text("请输入场次名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $("#select2-chosen-4").text("请输入机构名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $.ajax({
            type: "get",
            url: contextPath + "/admin/get_msg_type.do",
            async: false,
            success: function (msg) {
                $scope.msgType = msg.data.msgType;
                $scope.typeDetails = msg.data.typeDetails;
            }
        })
        $("#addModal").modal('show');
        $("#msgType").get(0).selectedIndex = 0;
        $("#typeDetails").get(0).selectedIndex = 0;
    }

    $scope.change = function () {
        var select = $scope.val;
        $.ajax({
            type: "get",
            url: contextPath + "/admin/get_type_content.do",
            data: {select: select},
            dataType: "json",
            async: false,
            success: function (msg) {
                $scope.typeDetails = msg.data.typeDetails;
                //$scope.$apply();
            }
        })
    }

    $scope.details = function (value) {
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/message_info.do",
            data: {id: value},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    $scope.message = msg.data.message;
                    //var time = msg.data.message.createTime;
                    //$scope.createTime = (new Date(time * 1000)).format("yyyy-MM-dd hh:mm:ss");
                    $("#messageModal").modal('show');
                } else {
                    alert(msg.data.error);
                }
            }
        })
    }

    //发送
    $scope.msgSend = function () {
        //选中项
        var typeSelected = 0;
        //指定用户的类型
        var msgType = 0;
        //类型的具体内容
        var typeDetails = "";
        //项目ID
        var projectId = 0;
        //机构ID
        var organizeId = 0;
        //场次ID
        var sessionId = 0;
        //提示语
        var text = "";
        //验证
        var s = [];
        $("#userRadio input:radio").each(function () {
            //所有用户
            if ($("#radioAll").attr("checked")) {
                text = "您确定向   所有用户   推送此消息吗？"
            }
            //指定用户
            if ($("#radioOthers").attr("checked")) {
                typeSelected = 1;
                msgType = $scope.val;
                typeDetails = $("#typeDetails option:selected").text();
                alert(typeDetails);
                text = "您确定向   指定用户   推送此消息吗？"
            }
            //关注项目
            if ($("#radioFollow").attr("checked")) {
                typeSelected = 2;
                if (!$('.select2_sample2').next().val()) {
                    s.push("<span>项目名称不能为空|</span>");
                }
                text = "您确定推送   关注项目   的消息吗？"
                projectId = $('.select2_sample2').next().val();
            }
            //机构入驻
            if ($("#radioOrganize").attr("checked")) {
                typeSelected = 3;
                if (!$('.select2_sample2_1').next().val()) {
                    s.push("<span>机构名称不能为空|</span>");
                }
                text = "您确定推送   机构入驻   的消息吗？"
                organizeId = $('.select2_sample2_1').next().val();
            }
            //好看精选、回放
            if ($("#radioHistory").attr("checked")) {
                typeSelected = 4;
                if (!$('.select2_sample2_2').next().val()) {
                    s.push("<span>场次名称不能为空|</span>");
                }
                text = "您确定推送   好看精选/回放   的消息吗？"
                sessionId = $('.select2_sample2_2').next().val();
            }
        })
        if (s.join("")) {
            $('.alert-danger', "#addModal").show().children("span").html(s);
            return;
        }

        if (confirm(text)) {
            $.ajax({
                type: "post",
                url: contextPath + "/admin/post_message.do",
                data: {
                    title: $("#title").val(),
                    msgType: msgType,
                    typeSelected: typeSelected,
                    typeDetails: typeDetails,
                    content: $("#content").val(),
                    projectId: projectId,
                    organizeId: organizeId,
                    sessionId: sessionId
                },
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        $("#addModal").modal('hide');
                        alert("消息发送成功");

                        var param = $("#searchForm").serialize();
                        var oSettings = table.fnSettings();
                        oSettings._iDisplayStart = 0;
                        oSettings.sAjaxSource = contextPath + "/admin/message_page.do?" + param;
//                    table.fnClearTable(0);
//                    table.fnDraw();
                        table._fnDraw(oSettings);

                    } else {
                        alert(msg.data.error);
                    }
                }
            })
        }
    }

    $scope.$watch('title', function (newVal, oldVal, scope) {
        if (newVal == null || newVal == "") {
            $("#msgSubmitBtn").addClass('disabled')
        } else {
            $("#msgSubmitBtn").removeClass('disabled');
        }
    })

    $('#messageList').bind('draw.dt', function () {
        var linkFn = $compile($('#messageList'));
        linkFn($scope);
    });
})