/**
 * Created by Administrator on 2015/8/4.
 */

kanConsole.controller('userNoteController',function($scope,$compile){

    $scope.saveNote = function() {
        var content = ue.getContent();
        $("#note").text(content);
        $.ajax({
            type:"post",
            url:contextPath + "/admin/user_note_modify.do",
            data:{id:$("#id").val(),editorValue:content},
            dataType:"json",
            async: false,
            success: function(msg){
                if(msg.code == 200) {
                    stopPop();
                    $scope.userNote = {};
                    $scope.userNote = msg.data.userNote;
                    popStatus(1, '提交成功', 2, '', false);
                } else {
                    stopPop();
                    popStatus(4, msg.data.error, 2, '', false);
                }
            },before: function(){
                popStatus(3, '正在上传。。。', -1, '', true);
            }
        })

    }

    $("#restBtn").click(function(){
        ue.setContent('');
    })

    $(function(){
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/get_user_note.do",
            dataType: "json",
            async: false,
            success: function(msg){
                $scope.userNote = msg.data.userNote;
                $scope.$apply();
            }
        })
    });

})
