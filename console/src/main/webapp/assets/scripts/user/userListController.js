"use strict";

/**
 * Created by Administrator on 2015/7/30.
 */

kanConsole.controller('userListController', function ($scope, $compile) {

    $scope.perCount = 0;

    $scope.details = function (value) {
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/user_info.do",
            data: {id: value},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    $scope.user = msg.data.user;
                    $scope.user.imgPath = getFullUrl($scope.user.imgPath);
                    $scope.contactsList = msg.data.contactsList;
                    $scope.genderMap = msg.data.genderMap;
                    var preference = $scope.user.preference;
                    if (preference) {
                        var array = preference.split(/,|，|\\+/);
                        $scope.preference = array;
                    }

                    $("#detailModal").modal('show');
                } else {
                    alert(msg.data.error);
                }
            }
        })
    }

    $scope.modalSubmit = function () {
        var radioJc = 1;
        var type = "";
        if ($scope.kanShow == true) {

            $("#radioDiv input:radio").each(function () {
                type = "kan";
                if ($("#radioRemind").attr('checked')) {
                    radioJc = 0;
                }
            })

            if ($("#pointForm").valid()) {
                $.ajax({
                    type: "post",
                    url: contextPath + "/admin/points_change.do",
                    data: {userId: $("#userId").val(), haoKanNum: $("#haoKanNum").val(), radioJc: radioJc, reason: $("#reason").val()},
                    dataType: "json",
                    async: false,
                    success: function (msg) {
                        if (msg.code == 200) {
                            var param = $("#searchForm").serialize();
                            var oSettings = dataTable.fnSettings();
                            oSettings.sAjaxSource = contextPath + "/admin/user_page.do?" + param;
                            dataTable._fnDraw(oSettings);
                            $("#haoKanModal").modal('hide');
                            alert("操作成功");
                        } else {
                            $("#haoKanModal").modal('hide');
                            alert(msg.data.error);
                        }
                    }
                })
            }
        }

        if ($scope.cashShow == true) {
            $("#radioCashDiv input:radio").each(function () {
                type = "cash";
                if ($("#radioCashJian").attr('checked')) {
                    radioJc = 0;
                }
            })

            if ($("#cashForm").valid()) {
                $.ajax({
                    type: "post",
                    url: contextPath + "/admin/cash_change.do",
                    data: {userId: $("#userId").val(), cashNum: $("#cashNum").val(), radioJc: radioJc, reason: $("#reasonCash").val()},
                    dataType: "json",
                    async: false,
                    success: function (msg) {
                        if (msg.code == 200) {
                            var param = $("#searchForm").serialize();
                            var oSettings = dataTable.fnSettings();
                            oSettings.sAjaxSource = contextPath + "/admin/user_page.do?" + param;
                            dataTable._fnDraw(oSettings);
                            $("#haoKanModal").modal('hide');
                            alert("操作成功");
                        } else {
                            $("#haoKanModal").modal('hide');
                            alert(msg.data.error);
                        }
                    }
                })
            }
        }
    }

    $scope.disabled = function (value) {
        if (null == value) {
            alert("操作失败");
        }
        if (confirm("确认要禁用此用户吗？")) {
            $.ajax({
                type: "GET",
                url: contextPath + "/admin/user_status.do",
                data: {userId: value, status: -1},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = dataTable.fnSettings();
                        oSettings.sAjaxSource = contextPath + "/admin/user_page.do?" + param;
                        //dataTable.fnClearTable(0);
                        //dataTable.fnDraw();
                        dataTable._fnDraw(oSettings);
                        $scope.perCount = dataTable.dataTableSettings[0]._iRecordsTotal;
                    } else {
                        alert(msg.data.error);
                    }
                }
            });
        }
    }

    $scope.enabled = function (value) {
        if (null == value) {
            alert("操作失败");
        }
        if (confirm("确认要启用此用户吗？")) {
            $.ajax({
                type: "GET",
                url: contextPath + "/admin/user_status.do",
                data: {userId: value, status: 10},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = dataTable.fnSettings();
                        //oSettings._iDisplayStart = 0;
                        oSettings.sAjaxSource = contextPath + "/admin/user_page.do?" + param;
                        //dataTable.fnClearTable(0);
                        //dataTable.fnDraw();
                        dataTable._fnDraw(oSettings);
                        $scope.perCount = dataTable.dataTableSettings[0]._iRecordsTotal;
                    } else {
                        alert(msg.data.error);
                    }
                }
            });
        }
    }

    $scope.phoneBlur = function () {
        var phone = $scope.phoneNumber;
      /*  if (phone) {
            if (isPhone(phone)) {

            } else {
                alert("手机号不正确");
                $scope.phoneNumber = "";
            }
        }*/
    }

    $scope.idCardBlur = function () {
        var idCard = $.trim($scope.identityCard);
        if (idCard) {
            if (isIdCardNo(idCard)) {

            } else {
                alert("身份证号码不正确");
                $scope.identityCard = "";
            }
        }
    }

    $scope.numStartBlur = function () {
        var numStart = $.trim($scope.numStart);
        var numEnd = $.trim($scope.numEnd);
        if (numStart) {
            if (isNumber(numStart)) {
                if (numEnd && (numStart > numEnd)) {
                    $scope.numEnd = numStart;
                }
            } else {
                alert("请填写正确的数字");
                $scope.numStart = "";
            }
        }
    }

    $scope.numEndBlur = function () {
        var numStart = $.trim($scope.numStart);
        var numEnd = $.trim($scope.numEnd);
        if (numEnd) {
            if (isNumber(numEnd)) {
                if (numStart && (numEnd < numStart)) {
                    $scope.numStart = numEnd;
                }
            } else {
                alert("请填写正确的数字");
                $scope.numEnd = "";
            }
        }
    }

    //验证非负整数
    function isNumber(value) {
        var number = /^(0|[1-9]\d*)$/;
        return number.test(value);
    }

    //验证手机号
    function isPhone(value) {
        var length = value.length;
        var mobile = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/
        return (length == 11 && mobile.test(value));
    }

    //验证身份证
    function isIdCardNo(idCardNo) {
        //15位和18位身份证号码的基本校验
        var check = /^\d{15}|(\d{17}(\d|x|X))$/.test(idCardNo);
        if (!check) return false;
        //判断长度为15位或18位
        if (idCardNo.length == 15) {
            return check15IdCardNo(idCardNo);
        } else if (idCardNo.length == 18) {
            return check18IdCardNo(idCardNo);
        } else {
            return false;
        }
    }

    //校验15位的身份证号码
    function check15IdCardNo(idCardNo) {
        //15位身份证号码的基本校验
        var check = /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/.test(idCardNo);
        return check;
    }

    //校验18位的身份证号码
    function check18IdCardNo(idCardNo) {
        //18位身份证号码的基本格式校验
        var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/.test(idCardNo);
        return check;
    }


    $('#userPersonList').bind('draw.dt', function () {
        var linkFn = $compile($('#userPersonList'));
        linkFn($scope);
        //Metronic.initUniform();
    });
})
