/**
 * Created by Mam on 2015/8/12.
 */

kanConsole.controller('filterWordsController',function($scope,$compile){

    $scope.first = function(){
        $.ajax({
            type:"GET",
            url:contextPath + "/admin/get_filter.do",
            async:false,
            success:function(msg) {
                if(msg.code == 200) {
                    var filterWords = msg.data.filterWords;
                    $scope.filterWords = filterWords;
                }else {
                    alert(msg.data.error);
                }
            }
        })
    }

    $scope.submit = function() {
        var words = $scope.filterWords.words;
        $.ajax({
            type:"post",
            url:contextPath + "/admin/update_filter.do",
            data:{filterWords:words},
            dataType:"json",
            async:false,
            success:function(msg) {
                if(msg.code == 200) {
                    $scope.filterWords =  msg.data.filterWords;
                    alert("更新成功！");
                } else {
                    alert(msg.data.error);
                }
            }
        })
    }

})