/**
 * Created by Administrator on 2015/9/15.
 */

kanConsole.controller("freightPageController",function($scope,$compile){
    var freightJson = new Array();
    $scope.freightList = function() {
        $.ajax({
            type:"get",
            url:contextPath + "/admin/freight_list.do",
            async:false,
            success:function(msg) {
                if(msg.code == 200) {
                    $scope.freightList = msg.data.freightList;
                    $scope.$apply();
                }else {
                    alert(msg.data.error);
                }
            }
        })
    }

    $scope.setSubmit = function() {
        var reg=/^[-\+]?\d+(\.\d+)?$/;
        var flag = 1;
        $('#freightTable').find('tr').each(function () {
            var tds = $(this).find('td');
            var areaName = $(tds[1]).text();
            var expressName = $(tds[2]).text();
            var areaCode = $(tds[3]).find('input').attr('id');
            var freight = $(tds[3]).find('input').val();
            if(!freight) {
                freight = 0;
            } else if( !reg.test(freight)) {
                flag = 0;
            }
            freightJson.push({
                areaName:areaName,
                expressName:expressName,
                areaCode:areaCode,
                freight:freight
            })
        })
        if(!flag) {
            freightJson.length = 0;
            alert("模版运费价格设置不正确。");
            return;
        }
        if(confirm("你确定要提交设置吗？")) {
            $.ajax({
                type:"post",
                url:contextPath + "/admin/area_freight.do",
                data:{data:JSON.stringify(freightJson)},
                dataType:"json",
                async:false,
                success:function(msg) {
                    if(msg.code == 200) {
                        alert("设置成功");
                    }else {
                        alert(msg.data.error);
                    }
                }
            })
        }
    }

    $(function(){
        $scope.freightList();
    })
})