"user strict";
/**
 * Created by Administrator on 2015/11/7.
 */

kanConsole.controller("homeImgController", function ($scope, $compile) {

    //新增 Btn
    $("#addImgBtn").click(function () {
        $("#imgForm").removeClass('has-error');
        $scope.homeImg = {};
        $('.alert-danger', "#addHomeImgModal").hide().children("span").html('');
        $(':input', "#imgForm").not(':button,:submit,:reset').val('');
        $scope.imgShow = false;
        $scope.organ_show = true;
        $scope.project_show = false;
        $scope.project_show = false;
        $scope.upload_show = true;
        $scope.img_show = true;
        $("#radioOrgan").attr('checked', true);
        $("#select2-chosen-2").text("请输入机构名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $("#select2-chosen-4").text("请输入项目名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $("#select2-chosen-6").text("请输入资讯名称").parent().addClass("select2-default").parent().removeClass('select2-allowclear');
        $scope.$apply();
        $("#addHomeImgModal").modal('show');
    })

    $("#radioDiv input:radio").live("click", function () {
        if ($("#radioOrgan").attr('checked')) {
            $scope.organ_show = true;
            $scope.news_show = false;
            $scope.project_show = false;
            $scope.img_show = true;
            $scope.upload_show = true;
            $scope.$apply();
        } else if ($("#radioImg").attr('checked')) {
            $scope.organ_show = false;
            $scope.news_show = false;
            $scope.project_show = false;
            $scope.img_show = true;
            $scope.upload_show = true;
            $scope.$apply();
        } else if ($("#radioProject").attr('checked')) {
            $scope.organ_show = false;
            $scope.news_show = false;
            $scope.project_show = true;
            $scope.img_show = true;
            $scope.upload_show = false;
            $scope.$apply();
        } else if ($("#radioNews").attr('checked')) {
            $scope.organ_show = false;
            $scope.news_show = true;
            $scope.project_show = false;
            $scope.img_show = true;
            $scope.upload_show = true;
            $scope.$apply();
        }
    })


    //清空
    $("#resetSearchBtn").click(function () {
        $(':input', "#searchForm").not(':button,:submit,:reset').val('');
        var param = $("#searchForm").serialize();
        var oSettings = dataTable.fnSettings();
        oSettings._iDisplayStart = 0;
        oSettings.sAjaxSource = contextPath + "/admin/home_img/data.do?" + param;
        dataTable.fnClearTable(0);
        dataTable.fnDraw();
    });

    //禁用 btn
    $scope.disable = function (data) {
        if (!data) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }
        $.ajax({
            type: "post",
            url: contextPath + "/admin/home_img/status.do",
            data: {id: data, status: 0},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    var param = $("#searchForm").serialize();
                    var oSettings = dataTable.fnSettings();
                    oSettings.ajaxSource = "/admin/home_img/data.do?" + param;
                    dataTable._fnDraw(oSettings);
                } else {
                    popStatus(4, msg.data.error, 4, "", false);
                }
            }
        })
    }

    //启用
    $scope.enable = function (data) {
        if (!data) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }
        $.ajax({
            type: "post",
            url: contextPath + "/admin/home_img/status.do",
            data: {id: data, status: 1},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    var param = $("#searchForm").serialize();
                    var oSettings = dataTable.fnSettings();
                    oSettings.ajaxSource = "/admin/home_img/data.do?" + param;
                    dataTable._fnDraw(oSettings);
                } else {
                    popStatus(4, msg.data.error, 4, "", false);
                }
            }
        })
    }

    //删除
    $scope.delImg = function (id) {
        if (confirm("确定要删除此选项吗？")) {
            $.ajax({
                type: "post",
                url: contextPath + "/admin/home_img/del.do",
                data: {id: id},
                dataType: "json",
                async: false,
                success: function (msg) {
                    if (msg.code == 200) {
                        var param = $("#searchForm").serialize();
                        var oSettings = dataTable.fnSettings();
                        oSettings.ajaxSource = "/admin/home_img/data.do?" + param;
                        dataTable._fnDraw(oSettings);
                    } else {
                        popStatus(4, msg.data.error, 4, "", false);
                    }
                }
            })
        }
    }

    //修改
    $scope.updateImg = function (id) {
        $('.alert-danger', "#addHomeImgModal").hide().children("span").html('');
        $("#imgForm").removeClass('has-error');
        if (!id) {
            popStatus(4, "参数无效", 2, "", "");
            return;
        }

        $.ajax({
            type: "get",
            url: contextPath + "/admin/home_img/detail.do",
            data: {id: id},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    $("#addHomeImgModal").modal('show');
                    $scope.homeImg = msg.data.homeImg;
                    $scope.noLiveImgImgPath = getFullUrl($scope.homeImg.imgPath);
                    if (msg.data.homeImg && msg.data.homeImg.imgPath) {
                        $scope.imgShow = true;
                    }
                    if (msg.data.homeImg && msg.data.homeImg.type == 3) {
                        $("#radioImg").attr('checked', true);
                        $scope.img_show = true;
                        $scope.upload_show = true;
                        $scope.organ_show = false;
                        $scope.project_show = false;
                        $scope.news_show = false;
                    } else if (msg.data.homeImg && msg.data.homeImg.type == 4) {
                        $("#radioProject").attr('checked', true);
                        $scope.upload_show = false;
                        $scope.organ_show = false;
                        $scope.project_show = true;
                        $scope.news_show = false;
                    }else if (msg.data.homeImg && msg.data.homeImg.type == 5) {
                        $("#radioNews").attr('checked', true);
                        $scope.organ_show = false;
                        $scope.news_show = true;
                        $scope.project_show = false;
                    } else {
                        $("#radioOrgan").attr('checked', true);
                        $scope.upload_show = true;
                        $scope.project_show = false;
                        $scope.organ_show = true;
                        $scope.news_show = false;
                    }
                    $scope.img_show = true;
                } else {
                    popStatus(4, msg.data.error, 2, "", "");
                }
            }
        })

        $("#addHomeImgModal").modal('show');


    }

    // 提交
    $scope.addImgSubmit = function () {

        var type = 2;
        var content = "";
        $("#radioDiv input:radio").each(function () {
            if ($("#radioOrgan").attr('checked')) {
                type = 2;
            } else if ($("#radioImg").attr('checked')) {
                type = 3;
            } else if ($("#radioProject").attr('checked')) {
                type = 4;
            } else if ($("#radioNews").attr('checked')) {
                type = 5;
            }
        })

        //机构ID
        var organId = $('.select2_sample2').next().val();
        //项目ID
        var projectId = $('.select2_sample2_1').next().val();
        //资讯ID
        var newsId = $('.select2_sample2_2').next().val();

        //验证
        var s = [];
        var content = $('#note').val();
        var coverPath = $('#coverPath').val();
        if (!content) {
            s.push("<span>备注不能为空|</span>");
        }
        if (type == 2) {

            if (!organId) {
                s.push("<span>机构名称不能为空|</span>");
            }
            if (!coverPath) {
                s.push("<span>图片不能为空|</span>");
            }
        }
        if (type == 3) {
            if (!coverPath) {
                s.push("<span>图片不能为空|</span>");
            }
        }
        if (type == 4) {
            if (!projectId) {
                s.push("<span>项目名称不能为空|</span>");
            }
            organId = projectId;
        }
        if (type == 5) {
            if (!newsId) {
                s.push("<span>资讯名称不能为空|</span>");
            }
            organId = newsId;
        }

        if (s.join("")) {
            $('.alert-danger', "#addHomeImgModal").show().children("span").html(s);
            return;
        }

        $.ajax({
            type: "post",
            url: contextPath + "/admin/home_img/modify.do",
            data: {id: $("#homeImgId").val(), note: content, type: type, imgId: $("#coverId").val(), imgPath: coverPath, organId: organId},
            dataType: "json",
            async: false,
            success: function (msg) {
                if (msg.code == 200) {
                    $("#addHomeImgModal").modal('hide');
                    popStatus(1, "操作成功", 2, "", "");

                    var param = $("#searchForm").serialize();
                    var oSettings = dataTable.fnSettings();
                    oSettings._iDisplayStart = 0;
                    oSettings.sAjaxSource = contextPath + "/admin/home_img/data.do?" + param;
                    dataTable.fnClearTable(0);
                    dataTable.fnDraw();
                } else {
                    console.log(msg.error);
                    popStatus(4, msg.error, 2, "", "");
                }


            }
        })
    }

    $('#imgList').bind('draw.dt', function () {
        var linkFn = $compile($('#imgList'));
        linkFn($scope);
    });
})
