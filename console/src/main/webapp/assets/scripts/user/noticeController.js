/**
 * Created by Administrator on 2015/8/4.
 */

kanConsole.controller('noticeController',function($scope,$compile){

    $scope.saveNotice = function() {
        var content = ue.getContent();
        $("#notice").text(content);
        $.ajax({
            type:"post",
            url:contextPath + "/admin/modify_ticket_notice.do",
            data:{id:$("#id").val(),content:content},
            dataType:"json",
            async: false,
            success: function(msg){
                if(msg.code == 200) {
                    stopPop();
                    $scope.notice = {};
                    $scope.notice = msg.data.notice;
                    popStatus(1, '提交成功', 2, '', false);
                } else {
                    stopPop();
                    popStatus(4, msg.data.error, 2, '', false);
                }
            },before: function(){
                popStatus(3, '正在上传。。。', -1, '', true);
            }
        })

    }

    $("#restBtn").click(function(){
        ue.setContent('');
    })

    $(function(){
        $.ajax({
            type: "GET",
            url: contextPath + "/admin/get_ticket_notice.do",
            dataType: "json",
            async: false,
            success: function(msg){
                $scope.notice = msg.data.notice;
                $scope.$apply();
            }
        })
    });

})
