package com.baidu.ueditor.upload;

import com.baidu.ueditor.define.AppInfo;
import com.baidu.ueditor.define.BaseState;
import com.baidu.ueditor.define.State;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.kan.common.AliOSS;
import com.kan.common.SpringContext;
import com.kan.common.utils.Configuration;
import com.kan.common.utils.DateUtils;
import com.kan.common.utils.ShortUUID;
import com.kan.model.UeditorFile;
import com.kan.persistence.UeditorFileMapper;
import org.apache.commons.io.FileUtils;

public class StorageManager {
	public static final int BUFFER_SIZE = 8192;

	public StorageManager() {
	}

    public static State saveBinaryFile(byte[] data, String fileName, Map<String, Object> conf){
        State state;
        try {
            String[] path = AliOSS.upload(fileName, data);
            UeditorFileMapper ueditorFileMapper = SpringContext.getBean(UeditorFileMapper.class);
            UeditorFile ueditorFile = new UeditorFile();
            ueditorFile.setUserId((Integer)conf.get("userId"));
            ueditorFile.setUserType((Integer)conf.get("userType"));
            ueditorFile.setFileName(fileName);
            ueditorFile.setDir(0);
            ueditorFile.setParentId(0);
            ueditorFile.setPath(path[0]);
			ueditorFile.setBucket(path[1]);
            ueditorFileMapper.insert(ueditorFile);
            state = new BaseState(true, path[0]);
            state.putInfo( "url", path[0] );
            state.putInfo( "size", data.length );
            state.putInfo( "title", fileName );
        } catch (IOException e) {
            e.printStackTrace();
            return new BaseState(false, AppInfo.IO_ERROR);
        }

        return state;
    }

    //ue
    public static State saveFileByInputStream(InputStream is, String fileName, Map<String, Object> conf,
                                              long maxSize) {
        State state = null;

        File tmpFile = getTmpFile();

        byte[] dataBuf = new byte[ 2048 ];
        BufferedInputStream bis = new BufferedInputStream(is, StorageManager.BUFFER_SIZE);

        try {
            if (tmpFile.length() > maxSize) {
                tmpFile.delete();
                return new BaseState(false, AppInfo.MAX_SIZE);
            }

	        File filePath = new File("/data/media_img/snapshot");
	        if (!filePath.exists()) {
		        filePath.mkdirs();
		        filePath.setExecutable(true, false);
		        filePath.setReadable(true, false);
		        filePath.setWritable(true, false);
	        }

	        String suffix = fileName.substring(fileName.lastIndexOf("."));
	        String tempFileName = ShortUUID.generate() + DateUtils.formatMillisecond(System.currentTimeMillis(),"yyyyMMddHHmmss") + suffix;
	        String path = Configuration.getProperty("media.domain") +  tempFileName;

	        File destFile = new File(filePath, tempFileName);

	        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFile), StorageManager.BUFFER_SIZE);

	        int count = 0;
	        while ((count = bis.read(dataBuf)) != -1) {
		        bos.write(dataBuf, 0, count);
	        }
	        bos.flush();
	        bos.close();

	        /*File destFile = new File(filePath, fileName);
	        FileUtils.copyFile(tmpFile, destFile);*/

           // String[] path = AliOSS.upload(fileName, tmpFile.getAbsolutePath());
            UeditorFileMapper ueditorFileMapper = SpringContext.getBean(UeditorFileMapper.class);
            UeditorFile ueditorFile = new UeditorFile();
            ueditorFile.setUserId((Integer)conf.get("userId"));
            ueditorFile.setUserType((Integer)conf.get("userType"));
            ueditorFile.setFileName(fileName);
            ueditorFile.setDir(0);
            ueditorFile.setParentId(0);
            ueditorFile.setPath(path);
            ueditorFileMapper.insert(ueditorFile);
            state = new BaseState(true);
            state.putInfo( "url", path);
            state.putInfo( "size", tmpFile.length() );
            state.putInfo( "title", fileName);
            //state = saveTmpFile(tmpFile, path);

            if (!state.isSuccess()) {
                tmpFile.delete();
            }

            return state;

        } catch (IOException e) {
        }
        return new BaseState(false, AppInfo.IO_ERROR);
    }

    public static State saveFileByInputStream(InputStream is, String fileName, Map<String, Object> conf) {
        State state = null;

        File tmpFile = getTmpFile();

        byte[] dataBuf = new byte[ 2048 ];
        BufferedInputStream bis = new BufferedInputStream(is, StorageManager.BUFFER_SIZE);

        try {
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(tmpFile), StorageManager.BUFFER_SIZE);

            int count = 0;
            while ((count = bis.read(dataBuf)) != -1) {
                bos.write(dataBuf, 0, count);
            }
            bos.flush();
            bos.close();

            String[] path = AliOSS.upload(fileName, tmpFile.getAbsolutePath());
            UeditorFileMapper ueditorFileMapper = SpringContext.getBean(UeditorFileMapper.class);
            UeditorFile ueditorFile = new UeditorFile();
            ueditorFile.setUserId((Integer)conf.get("userId"));
            ueditorFile.setUserType((Integer)conf.get("userType"));
            ueditorFile.setFileName(fileName);
            ueditorFile.setDir(0);
            ueditorFile.setParentId(0);
            ueditorFile.setPath(path[0]);
			ueditorFile.setBucket(path[1]);
            ueditorFileMapper.insert(ueditorFile);
            state = new BaseState(true);
            state.putInfo( "url", path[0] );
            state.putInfo( "size", tmpFile.length() );
            state.putInfo( "title", tmpFile.getName() );
            //state = saveTmpFile(tmpFile, path);

            if (!state.isSuccess()) {
                tmpFile.delete();
            }

            return state;
        } catch (IOException e) {
        }
        return new BaseState(false, AppInfo.IO_ERROR);
    }

	/*public static State saveBinaryFile(byte[] data, String path) {
		File file = new File(path);

		State state = valid(file);

		if (!state.isSuccess()) {
			return state;
		}

		try {
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(file));
			bos.write(data);
			bos.flush();
			bos.close();
		} catch (IOException ioe) {
			return new BaseState(false, AppInfo.IO_ERROR);
		}

		state = new BaseState(true, file.getAbsolutePath());
		state.putInfo( "size", data.length );
		state.putInfo( "title", file.getName() );
		return state;
	}

	public static State saveFileByInputStream(InputStream is, String path,
			long maxSize) {
		State state = null;

		File tmpFile = getTmpFile();

		byte[] dataBuf = new byte[ 2048 ];
		BufferedInputStream bis = new BufferedInputStream(is, StorageManager.BUFFER_SIZE);

		try {
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(tmpFile), StorageManager.BUFFER_SIZE);

			int count = 0;
			while ((count = bis.read(dataBuf)) != -1) {
				bos.write(dataBuf, 0, count);
			}
			bos.flush();
			bos.close();

			if (tmpFile.length() > maxSize) {
				tmpFile.delete();
				return new BaseState(false, AppInfo.MAX_SIZE);
			}

			state = saveTmpFile(tmpFile, path);

			if (!state.isSuccess()) {
				tmpFile.delete();
			}

			return state;

		} catch (IOException e) {
		}
		return new BaseState(false, AppInfo.IO_ERROR);
	}

	public static State saveFileByInputStream(InputStream is, String path) {
		State state = null;

		File tmpFile = getTmpFile();

		byte[] dataBuf = new byte[ 2048 ];
		BufferedInputStream bis = new BufferedInputStream(is, StorageManager.BUFFER_SIZE);

		try {
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(tmpFile), StorageManager.BUFFER_SIZE);

			int count = 0;
			while ((count = bis.read(dataBuf)) != -1) {
				bos.write(dataBuf, 0, count);
			}
			bos.flush();
			bos.close();

			state = saveTmpFile(tmpFile, path);

			if (!state.isSuccess()) {
				tmpFile.delete();
			}

			return state;
		} catch (IOException e) {
		}
		return new BaseState(false, AppInfo.IO_ERROR);
	}*/

	private static File getTmpFile() {
		File tmpDir = FileUtils.getTempDirectory();
		String tmpFileName = (Math.random() * 10000 + "").replace(".", "");
		return new File(tmpDir, tmpFileName);
	}

	private static State saveTmpFile(File tmpFile, String path) {
		State state = null;
		File targetFile = new File(path);

		if (targetFile.canWrite()) {
			return new BaseState(false, AppInfo.PERMISSION_DENIED);
		}
		try {
			FileUtils.moveFile(tmpFile, targetFile);
		} catch (IOException e) {
			return new BaseState(false, AppInfo.IO_ERROR);
		}

		state = new BaseState(true);
		state.putInfo( "size", targetFile.length() );
		state.putInfo( "title", targetFile.getName() );
		
		return state;
	}

	private static State valid(File file) {
		File parentPath = file.getParentFile();

		if ((!parentPath.exists()) && (!parentPath.mkdirs())) {
			return new BaseState(false, AppInfo.FAILED_CREATE_FILE);
		}

		if (!parentPath.canWrite()) {
			return new BaseState(false, AppInfo.PERMISSION_DENIED);
		}

		return new BaseState(true);
	}
}
