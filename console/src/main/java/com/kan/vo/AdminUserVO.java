package com.kan.vo;

import java.io.Serializable;


public class AdminUserVO implements Serializable {

	private Integer id;

	private String userName;

	private String department;

	private String nickName;

	private Integer userId;



	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setDepartment(String department){
		this.department=department;
	}

	public String getDepartment(){
		return department;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
