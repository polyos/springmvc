package com.kan.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class User implements Serializable{

	private static final long serialVersionUID = 3111146861219355281L;

	private Integer id;

	private String phoneNumber;
	@JsonIgnore
	private String password;

	private Integer status;

	private String loginIp;

	private String nickName;

	private String realName;

	private Integer imgId;

	private String imgPath;

	private Integer backId;

	private String backPath;

	private Long birthday;

	private String identityCard;

	private Integer gender;

	private String description;

	private String label;

	private String preference;

	private String email;

	private String address;

	private Long loginTime;

	private Long createTime;

	private Long updateTime;

	private Integer horoscope;

	private Integer perStatus;

	private Integer income;

	private Integer job;

	private String receiver;

	private String receiverPhone;

	private Integer areaCode;

	private String areaName;

	private String zipCode;

	private String invoiceStatus;

	private String registerId;

	private int userType;

	private String openid;

	private String platform;

	//好看点
	private Integer balance;
	//购买票数
	private Integer TicketNum;

	private String gender0;

	private String horoscope0;

	private String perStatus0;

	private String income0;

	private String job0;

	private String text;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber=phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setPassword(String password){
		this.password=password;
	}

	public String getPassword(){
		return password;
	}

	public void setStatus(Integer status){
		this.status=status;
	}

	public Integer getStatus(){
		return status;
	}

	public void setLoginIp(String loginIp){
		this.loginIp=loginIp;
	}

	public String getLoginIp(){
		return loginIp;
	}

	public void setNickName(String nickName){
		this.nickName=nickName;
	}

	public String getNickName(){
		return nickName;
	}

	public void setRealName(String realName){
		this.realName=realName;
	}

	public String getRealName(){
		return realName;
	}

	public void setImgId(Integer imgId){
		this.imgId=imgId;
	}

	public Integer getImgId(){
		return imgId;
	}

	public void setImgPath(String imgPath){
		this.imgPath=imgPath;
	}

	public String getImgPath(){
		return imgPath;
	}

	public void setBirthday(Long birthday){
		this.birthday=birthday;
	}

	public Long getBirthday(){
		return birthday;
	}

	public void setIdentityCard(String identityCard){
		this.identityCard=identityCard;
	}

	public String getIdentityCard(){
		return identityCard;
	}

	public void setGender(Integer gender){
		this.gender=gender;
	}

	public Integer getGender(){
		return gender;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getDescription(){
		return description;
	}

	public void setLabel(String label){
		this.label=label;
	}

	public String getLabel(){
		return label;
	}

	public void setPreference(String preference){
		this.preference=preference;
	}

	public String getPreference(){
		return preference;
	}

	public void setEmail(String email){
		this.email=email;
	}

	public String getEmail(){
		return email;
	}

	public void setAddress(String address){
		this.address=address;
	}

	public String getAddress(){
		return address;
	}

	public void setLoginTime(Long loginTime){
		this.loginTime=loginTime;
	}

	public Long getLoginTime(){
		return loginTime;
	}

	public void setCreateTime(Long createTime){
		this.createTime=createTime;
	}

	public Long getCreateTime(){
		return createTime;
	}

	public void setUpdateTime(Long updateTime){
		this.updateTime=updateTime;
	}

	public Long getUpdateTime(){
		return updateTime;
	}

	public Integer getHoroscope() {
		return horoscope;
	}

	public void setHoroscope(Integer horoscope) {
		this.horoscope = horoscope;
	}

	public Integer getPerStatus() {
		return perStatus;
	}

	public void setPerStatus(Integer perStatus) {
		this.perStatus = perStatus;
	}

	public Integer getIncome() {
		return income;
	}

	public void setIncome(Integer income) {
		this.income = income;
	}

	public Integer getJob() {
		return job;
	}

	public void setJob(Integer job) {
		this.job = job;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getTicketNum() {
		return TicketNum;
	}

	public void setTicketNum(Integer ticketNum) {
		TicketNum = ticketNum;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public Integer getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(Integer areaCode) {
		this.areaCode = areaCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getRegisterId() {
		return registerId;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}

	public String getGender0() {
		return gender0;
	}

	public void setGender0(String gender0) {
		this.gender0 = gender0;
	}

	public String getHoroscope0() {
		return horoscope0;
	}

	public void setHoroscope0(String horoscope0) {
		this.horoscope0 = horoscope0;
	}

	public String getPerStatus0() {
		return perStatus0;
	}

	public void setPerStatus0(String perStatus0) {
		this.perStatus0 = perStatus0;
	}

	public String getIncome0() {
		return income0;
	}

	public void setIncome0(String income0) {
		this.income0 = income0;
	}

	public String getJob0() {
		return job0;
	}

	public void setJob0(String job0) {
		this.job0 = job0;
	}

	public String getText() {
		return phoneNumber;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Integer getBackId() {
		return backId;
	}

	public void setBackId(Integer backId) {
		this.backId = backId;
	}

	public String getBackPath() {
		return backPath;
	}

	public void setBackPath(String backPath) {
		this.backPath = backPath;
	}
}
