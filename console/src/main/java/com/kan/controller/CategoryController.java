package com.kan.controller;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.model.Category;
import com.kan.service.CategoryService;
import com.kan.utils.RestResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/8 16:34
 * @Description: 商品分类 controller
 */

@Controller
@RequestMapping("/admin")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	/**
	 * 设备 分类页面
	 * @param category
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/category/cateogry_modify.do",method = RequestMethod.POST)
	public RestResult saveOrUpdateCategory(Category category){
		try {
			if (category == null){
				return RestResult.ERROR_500().put("error","操作失败");
			}
			categoryService.saveOrupdateCategory(category);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS();
	}


	/**
	 * 分类展示 页面
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/category/cateogry_page.do",method = RequestMethod.GET)
	public String categoryPage(){
		return "/admin/categoryPage";
	}

	/**
	 * 删除 商品分类
	 * @param categoryId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/category/cateogry_delete.do",method = RequestMethod.POST)
	public RestResult deleteCategory(@RequestParam("categoryId") String categoryId){
		try {
			Category category = categoryService.findCategoryByCategoryId(categoryId);
			if (category != null){
				categoryService.deleteCategory(categoryId);
			}
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS();
	}

	/**
	 * 查询设备信息
	 * @param categoryId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/category/cateogry_info.do",method = RequestMethod.GET)
	public RestResult getCategoryInfo(String categoryId){
		Category category = categoryService.findCategoryByCategoryId(categoryId);
		return RestResult.SUCCESS().put("categoryInfo",category);
	}

	/**
	 * 分类列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/category/cateogry_list.do",method = RequestMethod.GET)
	public RestResult getCategoryList(){
		List<Category> categoryList = categoryService.findCategoryList();
		return RestResult.SUCCESS().put("categoryList",categoryList);
	}

	@ResponseBody
	@RequestMapping(value = "/category/cateogry_table.do",method = RequestMethod.GET)
	public DataTablePaginator getCategoryTable(String categoryName,DataTableParam param){
		DataTablePaginator p = categoryService.findCategoryTable(categoryName,param);
		return p;
	}
}
