package com.kan.controller;

import com.baidu.ueditor.upload.StorageManager;
import com.kan.common.AliOSS;
import com.kan.common.utils.*;
import com.kan.common.utils.image.ImageUtils;
import com.kan.model.UploadFile;
import com.kan.service.UploadFileService;
import com.kan.utils.RestResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FileUtils;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class FileUploadController {

	@Autowired
	private UploadFileService uploadFileService;

	// 缩略图默认裁剪尺寸
	private final int DEAFAULT_WIDTH = 200;
	private final int DEAFAULT_HEIGHT = 200;
	// 缩略图后缀
	private final String SUFFIX = "_TN";

	@RequestMapping(value = "/upload_file.do", method = RequestMethod.POST)
	public void uploadFile(MultipartFile uploadFile, String dir, String limitType, HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");

		try {
			String contentType = uploadFile.getContentType();
			if (org.apache.commons.lang3.StringUtils.isNotBlank(limitType) && !contentType.contains(limitType)) {
				if (limitType.equals("image")) {
					response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().setErrorMessage("请上传正确格式的图片文件")));
					return;
				}
				if (limitType.equals("audio")) {
					response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().setErrorMessage("请上传正确格式的音频文件")));
					return;
				}
				if (limitType.equals("video")) {
					// rmvb,swf
					if (!contentType.equals("application/octet-stream") && !contentType.equals("application/x-shockwave-flash")) {
						response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().setErrorMessage("请上传正确格式的视频文件")));
						return;
					}
				}
			}
			// byte[] fileByte = compressionImage(uploadFile.getBytes());
			byte[] fileByte = uploadFile.getBytes();
			String originalFileName = uploadFile.getOriginalFilename();

			if (fileByte == null) {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", "图片转码/压缩不成功，请重试，多次不成功，请联系管理员")));
				return;
			}
			//String[] newFileName = AliOSS.upload(originalFileName, fileByte);
			File filePath = new File("/data/media_img/snapshot");
			if (!filePath.exists()) {
				filePath.mkdirs();
				filePath.setExecutable(true, false);
				filePath.setReadable(true, false);
				filePath.setWritable(true, false);
			}

			String suffix = originalFileName.substring(originalFileName.lastIndexOf("."));
			String fileName = ShortUUID.generate() + DateUtils.formatMillisecond(System.currentTimeMillis(),"yyyyMMddHHmmss") + suffix;

			File destFile = new File(filePath, fileName);

			BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileByte));
			try {
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFile), StorageManager.BUFFER_SIZE);
				byte[] dataBuf = new byte[ 2048 ];
				int count = 0;
				while ((count = inputStream.read(dataBuf)) != -1) {
					bos.write(dataBuf, 0, count);
				}
				bos.flush();
				bos.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			UploadFile uploadFile1 = uploadFileService.addUploadFile(Configuration.getProperty("media.domain") + fileName, dir, originalFileName, "");
			Map<String, Object> map = new HashMap<>();
			map.put("path", uploadFile1.getPath());
			//map.put("path", "http://168.235.105.23:8888/snapshot/5Z6Stvec20190331231046.jpg");
			map.put("fileName", originalFileName);
			map.put("uploadFileId", uploadFile1.getId());
			if (contentType.contains("audio/")) {
				// 音频转码
				map.put("duration", getAudioTime(uploadFile));
			}
			response.getWriter().print(StringUtils.toJson(RestResult.SUCCESS().put("uploadFile", map)));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500()));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private int getAudioTime(MultipartFile uploadFile) {
		int time = 0;
		CommonsMultipartFile cf = (CommonsMultipartFile) uploadFile;
		DiskFileItem fi = (DiskFileItem) cf.getFileItem();
		String str = fi.getStoreLocation().getAbsolutePath();
		int index = str.lastIndexOf("\\");
		String subUrl = str.substring(0, index + 1);
		String fullUrl = subUrl + cf.getOriginalFilename();
		File f = new File(fullUrl);
		try {
			f.createNewFile();
			OutputStream os = new FileOutputStream(f);
			byte[] bb = new byte[1024];
			int n = 0;
			InputStream in = uploadFile.getInputStream();
			while ((n = in.read(bb)) > 0) {
				os.write(bb, 0, n);
			}
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			time = AudioProcessUtils.getMp3Second(f);
		} catch (CannotReadException e) {
			e.printStackTrace();
		} catch (TagException e) {
			e.printStackTrace();
		} catch (ReadOnlyFileException e) {
			e.printStackTrace();
		} catch (InvalidAudioFrameException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (f.exists()) {
			f.delete();
		}

		return time;

	}

	/**
	 * description: 同时返回图片的宽、高
	 *
	 * @param uploadFile
	 * @param dir
	 * @param response
	 * @author don
	 * @date 2015年9月11日 下午1:35:36
	 */
	@RequestMapping(value = "/upload_file_recommend.do", method = RequestMethod.POST)
	public void uploadFileSize(MultipartFile uploadFile, String dir, HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");
		try {
			byte[] fileByte = compressionImageRecommend(uploadFile.getBytes());
			String originalFileName = uploadFile.getOriginalFilename();

			if (fileByte == null) {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", "图片转码/压缩不成功，请重试，多次不成功，请联系管理员")));
				return;
			}
			String[] newFileName = AliOSS.upload(originalFileName, fileByte);
			UploadFile uploadFile1 = uploadFileService.addUploadFile(newFileName[0], dir, originalFileName, newFileName[1]);
			Map<String, Object> map = new HashMap<>();
			map.put("path", uploadFile1.getPath());
			map.put("fileName", originalFileName);
			map.put("uploadFileId", uploadFile1.getId());
			int size[] = ImageUtils.getWHSize(fileByte);
			map.put("width", size[0]);
			map.put("height", size[1]);
			response.getWriter().print(StringUtils.toJson(RestResult.SUCCESS().put("uploadFile", map)));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", e.getMessage())));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * 推荐海报
	 *
	 * @param uploadFile
	 * @param dir
	 * @param response
	 */
	@RequestMapping(value = "/upload_file_size.do", method = RequestMethod.POST)
	public void uploadFileRecommend(MultipartFile uploadFile, String dir, HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");
		try {
			byte[] fileByte = compressionImage(uploadFile.getBytes());
			String originalFileName = uploadFile.getOriginalFilename();

			if (fileByte == null) {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", "图片转码/压缩不成功，请重试，多次不成功，请联系管理员")));
				return;
			}
			String[] newFileName = AliOSS.upload(originalFileName, fileByte);
			UploadFile uploadFile1 = uploadFileService.addUploadFile(newFileName[0], dir, originalFileName, newFileName[1]);
			Map<String, Object> map = new HashMap<>();
			map.put("path", uploadFile1.getPath());
			map.put("fileName", originalFileName);
			map.put("uploadFileId", uploadFile1.getId());
			int size[] = ImageUtils.getWHSize(fileByte);
			map.put("width", size[0]);
			map.put("height", size[1]);
			response.getWriter().print(StringUtils.toJson(RestResult.SUCCESS().put("uploadFile", map)));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", e.getMessage())));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * description: 缩略图&原图
	 *
	 * @param uploadFile
	 * @param dir
	 * @param response
	 * @param width
	 * @param height
	 * @author don
	 * @date 2015年9月6日 下午4:22:33
	 */
	@RequestMapping(value = "/upload_file_TN.do", method = RequestMethod.POST)
	public void uploadFileTN(MultipartFile uploadFile, String dir, HttpServletResponse response, Integer width, Integer height) {
		response.setContentType("text/plain; charset=utf-8");
		try {
			// 原图
			byte[] fileByte = compressionImage(uploadFile.getBytes());
			String originalFileName = uploadFile.getOriginalFilename();
			if (fileByte == null) {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500().put("error", "图片转码/压缩不成功，请重试，多次不成功，请联系管理员")));
				return;
			}

			String filePath = "/data/media_img/snapshot" ;
			if (!new File(filePath).exists()) {
				new File(filePath).mkdirs();
			}


			String path = filePath + File.separator + originalFileName;

			String suffix = originalFileName.substring(originalFileName.length() - originalFileName.lastIndexOf(".")-1);

			String fileName = ShortUUID.generate() + DateUtils.formatMillisecond(System.currentTimeMillis(),"yyyyMMddHHmmss") + suffix;

			File destFile = new File(filePath, fileName);

			BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileByte));
			try {
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFile), StorageManager.BUFFER_SIZE);
				byte[] dataBuf = new byte[ 2048 ];
				int count = 0;
				while ((count = inputStream.read(dataBuf)) != -1) {
					bos.write(dataBuf, 0, count);
				}
				bos.flush();
				bos.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			//String[] newFileName = AliOSS.upload(originalFileName, fileByte);
			UploadFile uploadFile1 = uploadFileService.addUploadFile(fileName, dir, originalFileName, "");
			Map<String, Object> map = new HashMap<>();

			map.put("path", uploadFile1.getPath());
			map.put("fileName", originalFileName);
			map.put("uploadFileId", uploadFile1.getId());
			// 缩略图
			if (width == null) {
				width = DEAFAULT_WIDTH;
			}
			if (height == null) {
				height = DEAFAULT_HEIGHT;
			}

			map.put("path" + SUFFIX, uploadFile1.getPath());
			map.put("fileName" + SUFFIX, originalFileName);
			map.put("uploadFileId" + SUFFIX, uploadFile1.getId());

			response.getWriter().print(StringUtils.toJson(RestResult.SUCCESS().put("uploadFile", map)));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500()));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@RequestMapping(value = ("/get_upload_file.do"), method = RequestMethod.GET)
	@ResponseBody
	public void getUploadFile(Integer id, HttpServletResponse response, HttpServletRequest request) throws UnsupportedEncodingException {
		UploadFile uploadFile = uploadFileService.getUploadFileById(id);
		if (uploadFile != null) {
			String fileName = uploadFile.getName();
			String agent = request.getHeader("USER-AGENT");
			if (agent != null && agent.indexOf("MSIE") == -1) {// FF
				String enableFileName = "=?UTF-8?B?" + (new String(Base64.encodeBase64(fileName.getBytes("UTF-8")))) + "?=";
				response.setHeader("Content-Disposition", "attachment; filename=" + enableFileName);
			} else { // IE
				String enableFileName = new String(fileName.getBytes("GBK"), "ISO-8859-1");
				response.setHeader("Content-Disposition", "attachment; filename=" + enableFileName);
			}
			try {
				byte[] b = AliOSS.download(uploadFile.getPath());
				response.getOutputStream().write(b, 0, b.length);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@RequestMapping(value = "/upload_file_big.do", method = RequestMethod.POST)
	public void uploadBigFile(MultipartFile uploadFile, String dir, HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");
		try {
			String originalFileName = uploadFile.getOriginalFilename();

			String[] newFileName = AliOSS.upload(originalFileName, uploadFile.getSize(), uploadFile.getInputStream());
			UploadFile uploadFile1 = uploadFileService.addUploadFile(newFileName[0], dir, originalFileName, newFileName[1]);
			Map<String, Object> map = new HashMap<>();
			map.put("path", uploadFile1.getPath());
			map.put("fileName", originalFileName);
			map.put("uploadFileId", uploadFile1.getId());
			response.getWriter().print(StringUtils.toJson(RestResult.SUCCESS().put("uploadFile", map)));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.getWriter().print(StringUtils.toJson(RestResult.ERROR_500()));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * description: 图片压缩
	 *
	 * @param source
	 * @return 返回压缩后的字节流
	 * @author don
	 * @date 2015年12月7日 下午2:45:28
	 */
	public byte[] compressionImage(byte[] source) {
		// 碎片图片，最大不超过500K，1280*720
		if (source.length > ImageUtils.MAX_IMAGE_SIZE_PROJECT) {
			return ImageUtils.resize(source, 1920, 1920);
		}
		return source;
	}

	public byte[] compressionImageRecommend(byte[] source) {
		// 碎片图片，最大不超过500K，1280*720
		if (source.length > ImageUtils.MAX_IMAGE_SIZE_PROJECT) {
			return ImageUtils.resize(source, ImageUtils.RECOMMEND_WIDTH, ImageUtils.RECOMMEND_HEIGHT);
		}
		return source;
	}

}
