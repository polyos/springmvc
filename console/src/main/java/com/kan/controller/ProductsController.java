package com.kan.controller;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.model.Message;
import com.kan.model.Products;
import com.kan.persistence.GalleryMapper;
import com.kan.service.ProductsService;
import com.kan.utils.RestResult;
import com.kan.utils.StringUtils;
import com.kan.vo.MessageVO;
import com.kan.vo.ProductsVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/8 16:34
 * @Description: 商品分类 controller
 */

@Controller
@RequestMapping("/admin")
public class ProductsController {

	@Autowired
	private ProductsService productsService;

	/**
	 * 商品展示 页面
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/products/products_page.do",method = RequestMethod.GET)
	public String categoryPage(){
		return "/admin/productsPage";
	}

	/**
	 * 添加、更新 设备
	 * @param products
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/products/products_modify.do",method = RequestMethod.POST)
	public RestResult saveOrUpdateProducts(ProductsVO products){
		ProductsVO p= new ProductsVO();
		try {
			/*if (StringUtils.isBlank(products.getContent())){
				return RestResult.ERROR_500().put("error","内容不能为空");
			}*/
			p = productsService.saveOrUpdateProducts(products);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS().put("productInfo",p);
	}

	/**
	 * 设备详情
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/products/products_info.do",method = RequestMethod.GET)
	public RestResult productsInfo(@RequestParam("productId") String productId){
		ProductsVO product = productsService.getProductInfoByProductId(productId);
		return RestResult.SUCCESS().put("productInfo",product);
	}

	/**
	 * 删除 设备
	 * @param productId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/products/products_delete.do",method = RequestMethod.POST)
	public RestResult deleteProducts(@RequestParam("productId") String productId){
		try {
			Products product= productsService.getProductByProductId(productId);
			if (product != null){
				productsService.deleteProducts(product);
			}
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS();
	}

	/**
	 * 添加 设备 图片集
	 * @param productId
	 * @param galleryId
	 * @param imgUrl
	 * @param desc
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/products/gallery_add.do",method = RequestMethod.POST)
	public RestResult addGallery(@RequestParam("productId") String productId,@RequestParam("galleryId") Integer galleryId,
	                             @RequestParam("imgUrl") String imgUrl,@RequestParam("desc") String desc){
		try {
			galleryId = productsService.saveOrUpdateGalleryOfProduct(galleryId, productId, imgUrl, desc);
			return RestResult.SUCCESS().put("galleryId",galleryId);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
	}

	/**
	 * 删除 设备 图片集
	 * @param galleryId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/products/gallery_delete.do",method = RequestMethod.POST)
	public RestResult delGallery(@RequestParam("galleryId") Integer galleryId){
		try {
			productsService.deleteGalleryOfProduct(galleryId);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS();
	}

	@ResponseBody
	@RequestMapping(value = "/products/products_table.do",method = RequestMethod.GET)
	public DataTablePaginator getProductsTable(String categoryId,DataTableParam param){
		DataTablePaginator p = productsService.findProductsTable(categoryId,param);
		return p;
	}

	@ResponseBody
	@RequestMapping(value = "/products/message_delete.do",method = RequestMethod.POST)
	public RestResult deleteMessage(@RequestParam("messageId") Integer messageId){
		try {
			productsService.deleteMessage(messageId);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
		return RestResult.SUCCESS();
	}

	@ResponseBody
	@RequestMapping(value = "/products/message_is_read.do",method = RequestMethod.GET)
	public RestResult updateMessage(@RequestParam(value = "messageId",required = true) Integer messageId,@RequestParam("isRead")Integer isRead){
		try {
			MessageVO vo = new MessageVO();
			Message message = productsService.updateMessage(messageId, isRead);
			if (message != null){
				BeanUtils.copyProperties(message,vo);
				if (!StringUtils.isBlank(message.getProductIds())){
					List<String> productNameByIds = productsService.getProductNameByIds(message.getProductIds());
					vo.setProductName(productNameByIds);
				}
			}
			return RestResult.SUCCESS().put("message",vo);
		}catch (Exception e){
			e.printStackTrace();
			return RestResult.ERROR_500();
		}
	}

	@ResponseBody
	@RequestMapping(value = "/products/message_table.do",method = RequestMethod.GET)
	public DataTablePaginator getMessageTable(String startTime,String endTime,DataTableParam param){
		DataTablePaginator p = productsService.findMessageTable(startTime,endTime,param);
		return p;
	}
}
