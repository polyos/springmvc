package com.kan.controller;

import com.kan.interceptor.ExcludeInterceptor;
import com.kan.model.Category;
import com.kan.service.CategoryService;
import com.kan.service.ProductsService;
import com.kan.vo.ProductsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/21 21:37
 * @Description:
 */

@Controller(value = "/highvist")
public class WebController {

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductsService productsService;

	@RequestMapping(value = "/category/list", method = RequestMethod.GET)
	@ExcludeInterceptor
	public List<Category> getCategoryList() {
		return categoryService.findCategoryList();
	}

	@RequestMapping(value = "/products/list", method = RequestMethod.GET)
	@ExcludeInterceptor
	public List<ProductsVO> getProductsList() {
		return productsService.findProductsVOList();
	}
}
