package com.kan.controller;

import com.kan.common.Paginator;
import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.common.rest.RestClient;

import com.kan.common.spring.cache.CacheConstant;
import com.kan.interceptor.ExcludeInterceptor;
import com.kan.model.AdminUser;

import com.kan.service.AdminUserService;
import com.kan.utils.RestResult;
import com.kan.utils.UserHolder;

import com.puff.cache.CacheClient;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Created by zxw on 2015/9/1.
 */
@Controller
public class AdminUserController {

    @Autowired
    private CacheClient cacheClient;

    @Autowired
    private RestClient restClient;

    @Autowired
    private AdminUserService adminUserService;

    //管理员账号管理页面
    @RequestMapping(value = "/admin/admin_user_list.do", method = RequestMethod.GET)
    public String userList() {
        return "/admin/userList";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/admin_user_list_data.do", method = RequestMethod.GET)
    public DataTablePaginator userListData(DataTableParam param, String username, String department) {
        DataTablePaginator p = adminUserService.getUserList(username, department, param);
        return p;
    }

    //添加/修改管理员账号
    @ResponseBody
    @RequestMapping(value = "/admin/add_admin_user.do", method = RequestMethod.POST)
    public RestResult addAdminUser(AdminUser adminUser) {
        try {
            if (adminUser.getId() == null || adminUser.getId() == 0) {
                adminUserService.addUser(adminUser);
            } else {
                adminUserService.updateUser(adminUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return RestResult.ERROR_500().put("error", "出错了，请刷新页面重试");
        }
        return RestResult.SUCCESS();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/get_admin_user.do", method = RequestMethod.GET)
    public RestResult getAdminUser(Integer id) {
        AdminUser adminUser = adminUserService.getUser(id);
        return RestResult.SUCCESS().put("adminUser", adminUser);
    }

    @RequestMapping(value = "/login.do", method = RequestMethod.GET)
    @ExcludeInterceptor
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/admin/login.do", method = RequestMethod.POST)
    @ResponseBody
    @ExcludeInterceptor
    public RestResult login(String username, String password, HttpServletRequest request, HttpServletResponse response) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Long time = cal.getTimeInMillis() / 1000;
        Long now = System.currentTimeMillis() / 1000;
        int expire = Long.valueOf(time - now).intValue();
        //获取 ip、ip登录次数
        String ip = getIpAddress(request);
        int loginIpCount = 0;
        Object loginIpCountVal = cacheClient.get(CacheConstant.CACHE_NAME_LOGIN, CacheConstant.LOGIN_CONSOLE_ERROR_IP_COUNT + ip);
        if (null != loginIpCountVal) {
            loginIpCount = Integer.valueOf(loginIpCountVal.toString());
            if (loginIpCount > 100) {
                return RestResult.ERROR_400().put("error", "登录错误次数太多,账号被临时锁定。");
            }
        }
        //cookie 记录登录登录错误次数，不能超过5次
        int cookieLoginCount = 0;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(CacheConstant.LOGIN_CONSOLE_ERROR_COUNT)) {
                    String value = cookie.getValue();
                    cookieLoginCount = Integer.valueOf(value);
                    break;
                }
            }
        }
        if (cookieLoginCount >= 4) {
            return RestResult.ERROR_400().put("error", "账号或密码错误，输入错误次数超过5次账号被锁定");
        }
        AdminUser adminUser = adminUserService.getAdminUserByName(username);
        String cacheKey = "";
        cacheKey = UUID.randomUUID().toString();
        if (adminUser != null && DigestUtils.md5Hex(password).equals(adminUser.getPassword())) {
            if (adminUser.getStatus().equals(10)) {
                return RestResult.ERROR_400().put("error", "您暂无权限，请联系管理员");
            }
            try {
                cacheClient.put(CacheConstant.CACHE_NAME_CONSOLE, cacheKey, adminUser, 36000);
                Cookie cookie = new Cookie(UserHolder.ADMIN_USER_SESSION_KEY,
                        cacheKey);
                cookie.setPath("/");
                //if (StringUtils.isNotBlank(webDomain)) {
                //    cookie.setDomain(webDomain);
                //}
                response.addCookie(cookie);

                //删除记录错误登录次数的 cookie、ip
                Cookie temp = new Cookie(CacheConstant.LOGIN_CONSOLE_ERROR_COUNT, "0");
                temp.setMaxAge(0);
                response.addCookie(temp);
                cacheClient.remove(CacheConstant.CACHE_NAME_LOGIN, CacheConstant.LOGIN_CONSOLE_ERROR_IP_COUNT + ip);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return RestResult.SUCCESS().put("msg", "success");
        }
        Cookie cookie = new Cookie(CacheConstant.LOGIN_CONSOLE_ERROR_COUNT, String.valueOf(cookieLoginCount + 1));
        cookie.setPath("/");
        cookie.setMaxAge(expire);
        response.addCookie(cookie);
        cacheClient.put(CacheConstant.CACHE_NAME_LOGIN, CacheConstant.LOGIN_CONSOLE_ERROR_IP_COUNT + ip, loginIpCount + 1, expire);
        if (loginIpCount >= 4) {
            loginIpCount = 4;
        }
        return RestResult.ERROR_400().put("error", "账号或密码错误，还可输入" + (4 - loginIpCount) + "次");
    }

    @RequestMapping(value = "/admin/logout.do", method = RequestMethod.GET)
    @ExcludeInterceptor
    public String logout(HttpServletRequest request,
                         HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        Integer userId = UserHolder.getAdminUser().getId();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(UserHolder.ADMIN_USER_SESSION_KEY)) {
                    String key = cookie.getValue();
                    if (StringUtils.isNotBlank(key)) {
                        try {
                            cacheClient.remove(CacheConstant.CACHE_NAME_CONSOLE, key);
                            cacheClient.remove(CacheConstant.CACHE_NAME_CONSOLE, CacheConstant.PRIVILEGE_USER_SYSTEM_ID + userId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        request.removeAttribute("adminUserSession");
                    }
                }
            }
        }
        return "redirect:/login.do";
    }

    @RequestMapping(value = "/privilege/jump_to_system.do", method = RequestMethod.GET)
    public String start(ModelMap map) {
        Integer userId = UserHolder.getAdminUser().getId();
        return "/index";
    }

    @RequestMapping(value = "/admin/message_list.do", method = RequestMethod.GET)
    public String userList(ModelMap map) {
        return "admin/msgList";
    }

    //用户DataTable
    @ResponseBody
    @RequestMapping(value = "/admin/user_page.do", method = RequestMethod.GET)
    public DataTablePaginator userPage(String nickName, String phoneNumber, String identityCard, Integer gender, String startTime, String endTime,
                                       Integer status, Integer userType, Integer numStart, Integer numEnd, DataTableParam param) {
        DataTablePaginator dataTablePaginator = new DataTablePaginator(param);
        Paginator paginator = new Paginator();
        Map<String, Object> map = new HashMap<>();
        map.put("pageNum", param.getiDisplayStart() / param.getiDisplayLength() + 1);
        map.put("pageSize", param.getiDisplayLength());

        dataTablePaginator.setAaData(null);
        dataTablePaginator.setiTotalRecords(paginator.getTotalCount());
        dataTablePaginator.setiTotalDisplayRecords(paginator.getTotalCount());
        return dataTablePaginator;
    }

    @RequestMapping(value = "/admin/clearBuffer.do")
    @ResponseBody
   /* public RestResult clearBuffer(String cacheName) {
        cacheClient.clear(cacheName);
        return RestResult.SUCCESS();
    }*/

    /**
     * 获取登录IP
     *
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-Real-IP");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            return ip;
        }
        ip = request.getHeader("X-Forwarded-For");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个IP值，第一个为真实IP。
            int index = ip.indexOf(',');
            if (index != -1) {
                return ip.substring(0, index);
            } else {
                return ip;
            }
        } else {
            return request.getRemoteAddr();
        }
    }

}
