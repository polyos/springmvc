package com.kan.persistence;

import com.kan.common.mybatis.BaseMapper;
import com.kan.model.Products;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/9 9:46
 * @Description:
 */
public interface ProductsMapper extends BaseMapper<Products> {

	/**
	 * 查询分类下的设备
	 * @param categoryId
	 * @param first
	 * @param max
	 * @return
	 */
	List<Products> getProductsList(@Param("categoryId")String categoryId,@Param("first")Integer first, @Param("max")Integer max);

	/**
	 * 查询 分类下的设备个数
	 * @param categoryId
	 * @return
	 */
	int getProductsCount(@Param("categoryId")String categoryId);

	List<String> getProductNameByIds(@Param("productIds") List<String> productIds);
}
