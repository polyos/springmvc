package com.kan.persistence;

import com.kan.common.mybatis.BaseMapper;
import com.kan.model.Message;
import com.kan.model.Products;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/9 9:46
 * @Description:
 */
public interface MessageMapper extends BaseMapper<Message> {

	/**
	 * 查询 消息列表
	 * @param first
	 * @param max
	 * @return
	 */
	List<Message> getMessageList( @Param("start")Long start, @Param("end")Long end, @Param("first")Integer first, @Param("max")Integer max);

	/**
	 * 查询 消息总数
	 * @return
	 */
	int getMessageCount(@Param("start")Long start, @Param("end")Long end);
}
