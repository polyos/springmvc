package com.kan.persistence;

import com.kan.model.AdminUser;
import com.kan.vo.AdminUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface AdminUserMapper extends com.kan.common.mybatis.BaseMapper<AdminUser> {

    public List<AdminUser> getUserList(@Param("username")String username,@Param("first")Integer first,@Param("max")Integer max);

    public Integer getUserCount(@Param("username")String username);

    public List<AdminUser> getAdminUserList();

}
