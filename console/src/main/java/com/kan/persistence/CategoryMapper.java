package com.kan.persistence;

import com.kan.common.mybatis.BaseMapper;
import com.kan.model.Category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/9 9:46
 * @Description:
 */
public interface CategoryMapper extends BaseMapper<Category> {
	/**
	 * 分类列表
	 * @param categoryName
	 * @param first
	 * @param max
	 * @return
	 */
	List<Category> getCategoryList(@Param("categoryName")String categoryName,@Param("first")Integer first, @Param("max")Integer max);
	int getCategoryCount(@Param("categoryName")String categoryName);
}
