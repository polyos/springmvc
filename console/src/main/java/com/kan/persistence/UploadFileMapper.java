package com.kan.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kan.common.mybatis.BaseMapper;
import com.kan.model.UploadFile;

/**
 * Created by ZXW on 2014/9/3.
 */
public interface UploadFileMapper extends BaseMapper<UploadFile>{

    public void addUploadFile(UploadFile uploadFile);

    public UploadFile getUploadFile(Integer id);

    public List<UploadFile> getUploadFiles(@Param("ids") String ids);

	public Integer updateFile(UploadFile uploadFile);

}
