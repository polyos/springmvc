package com.kan.velocity;

import com.kan.common.utils.DateUtils;

public class SessionVelocity {

	private Integer id;
	private Integer stadiumId;
	private Integer regionId;
	private Integer projectId;
	private String projectName;
	private String regionName;
	private String stadiumName;
	private Long startTime;
	private Long sellStartTime;
	private Long sellEndTime;
	private Integer status;
	private String stopReason;
	private Integer ticketTemplateId;
	private Integer selectSeatMode;
	private Integer programPrice;
	private Long programSellTime;
	private Integer stadiumImageId;
	private String stadiumImageUrl;
	private Long createTime;
	private Integer sellOutRatio;

	public Integer getSellOutRatio() {
		return sellOutRatio;
	}

	public void setSellOutRatio(Integer sellOutRatio) {
		this.sellOutRatio = sellOutRatio;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setStadiumId(Integer stadiumId) {
		this.stadiumId = stadiumId;
	}

	public Integer getStadiumId() {
		return stadiumId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRegionName() {
		return regionName;
	}

	public String getStadiumName() {
		return stadiumName;
	}

	public void setStadiumName(String stadiumName) {
		this.stadiumName = stadiumName;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getStartTime() {
		return DateUtils.getDatebyTimeMillis(startTime, "yyyy-MM-dd HH:mm");
	}

	public void setSellStartTime(Long sellStartTime) {
		this.sellStartTime = sellStartTime;
	}

	public String getSellStartTime() {
		return DateUtils.getDatebyTimeMillis(sellStartTime, "yyyy-MM-dd HH:mm");
	}

	public void setSellEndTime(Long sellEndTime) {
		this.sellEndTime = sellEndTime;
	}

	public String getSellEndTime() {
		return DateUtils.getDatebyTimeMillis(sellEndTime, "yyyy-MM-dd HH:mm");
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStopReason(String stopReason) {
		this.stopReason = stopReason;
	}

	public String getStopReason() {
		return stopReason;
	}

	public void setTicketTemplateId(Integer ticketTemplateId) {
		this.ticketTemplateId = ticketTemplateId;
	}

	public Integer getTicketTemplateId() {
		return ticketTemplateId;
	}

	public void setSelectSeatMode(Integer selectSeatMode) {
		this.selectSeatMode = selectSeatMode;
	}

	public Integer getSelectSeatMode() {
		return selectSeatMode;
	}

	public Integer getProgramPrice() {
		return programPrice;
	}

	public void setProgramPrice(Integer programPrice) {
		this.programPrice = programPrice;
	}

	public String getProgramSellTime() {

		return DateUtils.getDatebyTimeMillis(programSellTime, "yyyy-MM-dd HH:mm");
	}

	public void setProgramSellTime(Long programSellTime) {
		this.programSellTime = programSellTime;
	}

	public Integer getStadiumImageId() {
		return stadiumImageId;
	}

	public void setStadiumImageId(Integer stadiumImageId) {
		this.stadiumImageId = stadiumImageId;
	}

	public String getStadiumImageUrl() {
		return stadiumImageUrl;
	}

	public void setStadiumImageUrl(String stadiumImageUrl) {
		this.stadiumImageUrl = stadiumImageUrl;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getCreateTime() {
		return DateUtils.getDatebyTimeMillis(createTime, "yyyy-MM-dd HH:mm");
	}

}
