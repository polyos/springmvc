package com.kan.velocity;

import com.kan.common.utils.DateUtils;
import com.kan.utils.StringUtils;

/**
 * description: velocity二次解析类
 * @author don
 * @date 2015年10月27日 下午2:39:08
 *
 */
public class UserTicketVelocity {

	private Integer id;
	private Integer isReal;
	private String reserve;
	private String ticketNo;
	private String areaName;
	private Integer ssaId;
	private Integer seatNo;
	private Integer rowNo;
	private Integer row;
	private Integer column;
	private String buyerName;
	private String buyerCardNo;
	private String buyerPhone;
	private Integer orderId;
	private Integer sessionId;
	private Integer userId;
	private String username;
	private Long createTime;
	private Double ticketPrice;
	private Double actualPrice;
	private Integer pointAmount;
	private Integer status;
	private Integer isPrint;
	private String securityCode;
	private String qrCode;
	private String alias;
	private Integer ticketId;
	private Integer isUsed;
	private Integer usedTime;
	private Integer belong;
	private Integer isUserDelete;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setIsReal(Integer isReal) {
		this.isReal = isReal;
	}

	public Integer getIsReal() {
		return isReal;
	}

	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	public String getReserve() {
		return reserve;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setSeatNo(Integer seatNo) {
		this.seatNo = seatNo;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getSsaId() {
		return ssaId;
	}

	public void setSsaId(Integer ssaId) {
		this.ssaId = ssaId;
	}

	public Integer getSeatNo() {
		return seatNo;
	}

	public Integer getRowNo() {
		return rowNo;
	}

	public void setRowNo(Integer rowNo) {
		this.rowNo = rowNo;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getRow() {
		return row;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public Integer getColumn() {
		return column;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerCardNo(String buyerCardNo) {
		this.buyerCardNo = buyerCardNo;
	}

	public String getBuyerCardNo() {
		return buyerCardNo;
	}

	public void setBuyerPhone(String buyerPhone) {
		this.buyerPhone = buyerPhone;
	}

	public String getBuyerPhone() {
		return buyerPhone;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getCreateTime() {
		return DateUtils.getDatebyTimeMillis(createTime,"yyyy-MM-dd HH:mm");
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Double getActualPrice() {
		return actualPrice;
	}

	public void setPointAmount(Integer pointAmount) {
		this.pointAmount = pointAmount;
	}

	public Integer getPointAmount() {
		return pointAmount;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setIsPrint(Integer isPrint) {
		this.isPrint = isPrint;
	}

	public Integer getIsPrint() {
		return isPrint;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getQrCode() {
		return qrCode;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}

	public Integer getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(Integer usedTime) {
		this.usedTime = usedTime;
	}

	public Integer getBelong() {
		return belong;
	}

	public void setBelong(Integer belong) {
		this.belong = belong;
	}

	public Integer getIsUserDelete() {
		return isUserDelete;
	}

	public void setIsUserDelete(Integer isUserDelete) {
		this.isUserDelete = isUserDelete;
	}
}
