package com.kan.model;

import com.kan.common.AliOSS;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "sys_products")
public class Products implements Serializable {

	private transient static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	@Column(name="product_id")
	private String productId;
	@Column(name="category_id")
	private String categoryId;
	@Column(name="product_name")
	private String productName;
	@Column(name="product_img")
	private String productImg;
	@Column(name="img_id")
	private Integer imgId;
	@Column(name="create_time")
	private Long createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductImg() {
		return productImg;
	}

	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Integer getImgId() {
		return imgId;
	}

	public void setImgId(Integer imgId) {
		this.imgId = imgId;
	}
}
