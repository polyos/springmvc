package com.kan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "sys_ueditor_file")
public class UeditorFile {

	@Id
	@Column(name="id")
	private Integer id;
	@Column(name="file_name")
	private String fileName;
	@Column(name="path")
	private String path;
	@Column(name="parent_id")
	private Integer parentId;
	@Column(name="dir")
	private Integer dir;
    @Column(name="user_id")
    private Integer userId;
    @Column(name="user_type")
    private Integer userType;
	@Column(name="bucket")
	private String bucket;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setFileName(String fileName){
		this.fileName=fileName;
	}

	public String getFileName(){
		return fileName;
	}

	public void setPath(String path){
		this.path=path;
	}

	public String getPath(){
		return path;
	}

	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}

	public Integer getParentId(){
		return parentId;
	}

	public void setDir(Integer dir){
		this.dir=dir;
	}

	public Integer getDir(){
		return dir;
	}

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
}
