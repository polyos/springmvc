package com.kan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "sys_category")
public class Category implements Serializable{

	@Id
	@Column(name="id")
	private Integer id;
	@Column(name="category_id")
	private String categoryId;
	@Column(name="category_name")
	private String categoryName;
	@Column(name="category_icon")
	private String categoryIcon;
	@Column(name="create_time")
	private long createTime;
	@Column(name="is_valide")
	private int isValide;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryIcon() {
		return categoryIcon;
	}

	public void setCategoryIcon(String categoryIcon) {
		this.categoryIcon = categoryIcon;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public int getIsValide() {
		return isValide;
	}

	public void setIsValide(int isValide) {
		this.isValide = isValide;
	}
}
