package com.kan.utils;

import com.kan.common.utils.Configuration;

/**
 * Created by zxw on 2015/7/22.
 */
public class ServiceUrls {

	public static String getUserUrl() {
		return Configuration.getProperty("service.user.url");
	}

	public static String getShowSupportUrl() {
		return Configuration.getProperty("service.show_support.url");
	}

	public static String getShowUrl() {
		return Configuration.getProperty("service.show.url");
	}

	public static String getMarketUrl() {
		return Configuration.getProperty("service.market.url");
	}

	public static String getLiveUrl() {
		return Configuration.getProperty("service.live.url");
	}

	public static String getOpenfireUrl() {
		return Configuration.getProperty("service.openfire.url");
	}

	public static String getOrganizationUrl() {
		return Configuration.getProperty("service.organization.url");
	}

	public static String getPhoneWeb() {
		return Configuration.getProperty("service.phone_web.url");
	}

	public static String getShopUrl() {
		return Configuration.getProperty("service.shop.url");
	}

	public static String getShowCollection(){
		return Configuration.getProperty("service.show.collection");
	}
	
	public static String getSchedulerUrl(){
		return Configuration.getProperty("service.scheduler.url");
	}
	
	public static String getUserRelation() {
		return Configuration.getProperty("service.user_relation.url");
	}
}
