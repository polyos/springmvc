package com.kan.utils;

import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * 
 * @use java给图片添加水印文字
 * @JDK 1.6.0 </br>
 * @Version 1.0 </br>
 */
public class ImageUtils {
	
	//静态常量,判断选用哪一种加水印方式
	public static final int TYPEMARK_OLD = 0;
	public static final int TYPEMARK_NEW = 1;
	public static Random random = new Random();

	/**
	 * 图片添加水印
	 * 
	 * @param srcImgPath
	 *            需要添加水印的图片的路径
	 * @param outImgPath
	 *            添加水印后图片输出路径
	 * @param markContentColor
	 *            水印文字的颜色
	 * @param waterMarkContent
	 *            水印的文字
	 */
	public static void mark(String srcImgPath, String outImgPath, Color markContentColor, String waterMarkContent) {
		try {
			// 读取原图片信息
			File srcImgFile = new File(srcImgPath);
			Image srcImg = ImageIO.read(srcImgFile);
			int srcImgWidth = srcImg.getWidth(null);
			int srcImgHeight = srcImg.getHeight(null);
			// 加水印
			BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bufImg.createGraphics();
			g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
			Font font = new Font("Courier New", Font.PLAIN, 18);
			// Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 50);
			g.setColor(markContentColor); // 根据图片的背景设置水印颜色

			g.setFont(font);
			int x = srcImgWidth - getWatermarkLength(waterMarkContent, g) - 20;
			// int y = srcImgHeight - 3;
			int y = 30;
			// int x = (srcImgWidth - getWatermarkLength(watermarkStr, g)) / 2;
			// int y = srcImgHeight / 2;
			g.drawString(waterMarkContent, x, y);
			g.dispose();
			// 输出图片
			FileOutputStream outImgStream = new FileOutputStream(outImgPath);
			ImageIO.write(bufImg, "jpg", outImgStream);
			outImgStream.flush();
			outImgStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	public static void mark(File srcFile, Color markContentColor, String dateStr, String dpetName, String sname) {
		try {
			if(srcFile==null){
				return ;
			}
			// 读取原图片信息
			File srcImgFile = srcFile;
			Image srcImg = ImageIO.read(srcImgFile);
			int srcImgWidth = srcImg.getWidth(null);
			int srcImgHeight = srcImg.getHeight(null);
			// 加水印
			BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bufImg.createGraphics();
			g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
			// 字体大小
			int n = srcImgWidth / 31;
			Font font = new Font("微软雅黑", Font.BOLD , n);
//			if(srcImgWidth<500){
//				n = srcImgWidth / 25;
//				font = new Font("微软雅黑", Font.BOLD , n);
//			}
			g.setColor(markContentColor); // 根据图片的背景设置水印颜色
			g.setFont(font);
//			FontRenderContext  frc = g.getFontRenderContext();
			int dateLength = 0; // 日期长度
			int watermarkHeight = srcImgHeight / 15; // 上边距
			int gapWidth = 20; // 左右边距
			// 添加日期水印
			if(StringUtils.isNotBlank(dateStr)){
				dateLength = getWatermarkLength(dateStr, g);
				int rightWidth = srcImgWidth - dateLength - gapWidth;
				g.drawString(dateStr, rightWidth, watermarkHeight);
			}
			String result="";
			// 添加门店/场景水印
			if(StringUtils.isNotBlank(dpetName)&& StringUtils.isNotBlank(sname)){
				result = dpetName+" - "+sname;
			} else if (StringUtils.isNotBlank(sname)) {
				result = sname;
			} else if (StringUtils.isNotBlank(dpetName)) {
				result = dpetName;
			}
			if (!"".equals(result)) {
				int maxOsLength = srcImgWidth - gapWidth * 2 - dateLength;
				int osLength = getWatermarkLength(result, g);
				// 超过长度，需要截取，考虑到后面的一般是关键信息，截断前面的字符
				if (osLength > maxOsLength) {
					int length = result.length() * maxOsLength / osLength;
					// 最终长度是length + 3(三个点)
					result = "..." + result.substring(result.length() - length, result.length());
				}
				g.drawString(result, gapWidth,watermarkHeight);
			}
			g.dispose();
			// 输出图片
			FileOutputStream outImgStream = new FileOutputStream(srcImgFile);
			ImageIO.write(bufImg, "jpg", outImgStream);
			outImgStream.flush();
			outImgStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 给图片添加中心水印的方法
	 * @param srcFile 源文件路径
	 * @param markContentColor 水印颜色
	 * @param date 日期:年,月,日,星期
	 * @param dpetName 店名(地点)
	 * @param time 时刻:小时+分钟
	 */
	public static void mark_new(String srcFile, Color markContentColor, String date, String dpetName, String time) {
		try {
			if(srcFile==null){
				return ;
			}
			// 读取原图片信息
			File srcImgFile = new File(srcFile);
			Image srcImg = ImageIO.read(srcImgFile);
			int srcImgWidth = srcImg.getWidth(null);
			int srcImgHeight = srcImg.getHeight(null);
			// 加水印
			BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bufImg.createGraphics();
			g.drawImage(srcImg, 0,0, srcImgWidth, srcImgHeight, null);
			/*设置上面一行时间time的字体和颜色*/
			int n = 0;
			//判断图片是竖版还是横版
			if(srcImgHeight>=srcImgWidth*1.5){
				n = srcImgWidth / 10;
			}else {
				n = srcImgWidth / 15;
			}
			Font font = new Font("微软雅黑", Font.PLAIN , n);
			g.setColor(markContentColor); // 根据图片的背景设置水印颜色
			g.setFont(font);
			int watermarkHeight = srcImgHeight*4 / 5; // 上边距
			int gapWidth = (srcImgWidth-getWatermarkLength(time, g))/2; // 左边距
			String result="";
			/*绘制上面一行添加 时间time 水印*/
			if(StringUtils.isNotBlank(time)){
				g.drawString(time, gapWidth,watermarkHeight);
			}
			if(StringUtils.isNotBlank(date)&& StringUtils.isNotBlank(dpetName)){
				result = date + "    " + dpetName;
			}else if(StringUtils.isNotBlank(date)){
				result = date+"    ";
			}else if(StringUtils.isNotBlank(dpetName)){
				result = "    " + dpetName;
			}
			
			if (!"".equals(result)) {
				int resultLength = getWatermarkLength(result, g);
				/*设置下面一行 日期+图标+店名 */
				if(srcImgHeight>=srcImgWidth*1.5){
					n = srcImgWidth / 25;
				}else {
					n = srcImgWidth / 35;
				}
				font = new Font("微软雅黑", Font.PLAIN , n);
				g.setColor(markContentColor); // 根据图片的背景设置水印颜色
				g.setFont(font);
				/**绘制下面一行添加 日期+图标+店名 水印**/
				//判断是不是竖版图片
				if(srcImgHeight>=srcImgWidth*1.5){
					g.drawString(date, (srcImgWidth-getWatermarkLength(date, g))/2,watermarkHeight+2*n);
					//如果店名过长,则进行截取
					result =  dpetName;
					if(getWatermarkLength(result, g)>=srcImgWidth){
						int maxOsLength = srcImgWidth - getWatermarkLength("...",g);
						int osLength = getWatermarkLength(result, g);
						int length = result.length() * maxOsLength / osLength;
						result = "..."+result.substring(0,length-2);
					}
					resultLength = getWatermarkLength(result,g);
					gapWidth = (srcImgWidth-resultLength)/2; // 重置左边距
					g.drawString(result, gapWidth,watermarkHeight+4*n);
				}else{
					//如果店名过长,则进行截取
					if(getWatermarkLength(result, g)>=srcImgWidth){
						int maxOsLength = srcImgWidth - getWatermarkLength(date, g)-getWatermarkLength("    ...", g);
						int osLength = getWatermarkLength(dpetName, g);
						int length = dpetName.length() * maxOsLength / osLength;
						result = date + "    ..."+dpetName.substring(0,length);
					}
					resultLength = getWatermarkLength(result, g);
					gapWidth = (srcImgWidth-resultLength)/2 ; // 重置左边距
					g.drawString(result, gapWidth ,watermarkHeight+2*n);
				}
			}
			/** 将水印图标绘到图片上 **/
			try {
				String path = ImageUtils.class.getResource("/").getPath();
				System.out.println("path=======================" + path);
				ImageIcon imgIcon = new ImageIcon(path+"tip.png");
				Image img = imgIcon.getImage();// 得到Image对象。
				// 判断是不是竖版图片
				if (srcImgHeight >= srcImgWidth * 1.5) {
						g.drawImage(img, gapWidth - n, watermarkHeight + 3 * n +n/4, n/2, n/2+n/3, null);
				} else {
					g.drawImage(img, gapWidth + getWatermarkLength(date, g)+n/2, watermarkHeight + n + n/4, n/2, n/2+n/3, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				g.dispose();
			}
			// 输出图片
			FileOutputStream outImgStream = new FileOutputStream(srcImgFile);
			ImageIO.write(bufImg, "jpg", outImgStream);
			outImgStream.flush();
			outImgStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 public static String splitString(String str,int len) throws Exception {
         if (StringUtils.isBlank(str)) {
                return "";
         }     
         byte[] strByte = str.getBytes("UTF-8");
         int strLen =strByte.length;
         if (len >= strLen || len < 1) {
                return str;
          }     
         int count = 0;
         for (int i = 0; i < len; i++) {
                int value = (int) strByte[i];
                if (value < 0) {
             	   count++; 
                 } 
          }
          if (count % 2 != 0) {
                 len = len -1;
          }
         return new String(strByte, 0, len,"UTF-8");
   }
	/**
	 * 获取水印文字总长度
	 * 
	 * @param waterMarkContent
	 *            水印的文字
	 * @param g
	 * @return 水印文字总长度
	 */
	private static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
		return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
	}
 

	private static int r(int min, int max) {
		int num = 0;
		num = random.nextInt(max - min) + min;
		return num;
	}

	public static BufferedImage createValidateCodePic(String id) throws IOException {
		// 在内存中创建一副图片
		int w = 120;
		int h = 50;
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		// 在图片上画一个矩形当背景
		Graphics g = img.getGraphics();
		g.setColor(new Color(r(50, 250), r(50, 250), r(50, 250)));
		g.fillRect(0, 0, w, h);

		String str = "aqzxswedcfrvgtbhyujklp23456789";
		StringBuffer code = new StringBuffer("");
		Graphics2D g2 = (Graphics2D) g;
		for (int i = 0; i < 4; i++) {
			g2.setStroke(new BasicStroke(3.0f));
			g2.setColor(new Color(r(50, 180), r(50, 180), r(50, 180)));
			g2.setFont(new Font("微软雅黑", Font.PLAIN, 40));
			char c = str.charAt(r(0, str.length()));
			g2.drawString(String.valueOf(c), 10 + i * 30, r(h - 30, h));
			code.append(c);
		}
		// 把code存入redis中
		//ValidateCodeRedisManager.putCode(id, code.toString());
		// 画随机线
		g2.setStroke(new BasicStroke(5.0f));
		g2.setColor(new Color(r(50, 180), r(50, 180), r(50, 180)));
		g2.drawLine(7, 24, 110,22);
		for (int i = 0; i < 2; i++) {
			g2.setStroke(new BasicStroke(2.0f));
			g2.setColor(new Color(r(50, 180), r(50, 180), r(50, 180)));
			g2.drawLine(r(0, w), r(0, h), r(0, w), r(0, h));
		}
		// 把内存中创建的图像输出到文件中
		// File file = new File(PhotoPo.PIC_URL_ROOT + "validate/" +
		// "vcode.png");
		// ImageIO.write(img, "png", file);
		System.out.println("图片输出完成: "+code);
		return img;

	}
     public static void main(String[] args) throws Exception {
    	 ImageUtils.mark_new("D:\\test5.jpg",Color.WHITE,"2017-11-16 星期四","！万店长测试万店长测试万店长店长测试万店长测试万店长测试万店长店长测试万店长测试万店长测试万店长店长测试万店长测试万店长测试万店长店长测试", "09:26");
    	// System.out.println(new String("    ...").length());
     }
    
}
