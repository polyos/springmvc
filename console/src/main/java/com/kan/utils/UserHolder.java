package com.kan.utils;

import com.kan.model.AdminUser;

/**
 * Created with IntelliJ IDEA. User: ZXW Date: 14-4-2 Time: 下午2:47 To change
 * this template use File | Settings | File Templates.
 */
public class UserHolder {

	public static final String ADMIN_USER_SESSION_KEY = "_ADMIN_USER_SESSION_KEY";
	private static ThreadLocal<AdminUser> tl = new ThreadLocal<>();

	public static AdminUser getAdminUser() {
		return tl.get();
	}

	public static void clear() {
        tl.remove();
	}

    public static void setAdminUser(AdminUser User){
        tl.set(User);
	}

}
