package com.kan.service;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.model.Message;
import com.kan.model.Products;
import com.kan.model.ProductsInfo;
import com.kan.vo.ProductsVO;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/9 17:11
 * @Description:
 */
public interface ProductsService {
	/**
	 * 添加、更新设备
	 * @param products
	 * @return
	 */
	ProductsVO saveOrUpdateProducts(ProductsVO products);

	/**
	 * 通过 设备ID 查询
	 * @param productId
	 * @return
	 */
	Products getProductByProductId(String productId);
	ProductsVO getProductInfoByProductId(String productId);

	/**
	 * 查询设备名称
	 * @param productIds
	 * @return
	 */
	List<String> getProductNameByIds(String productIds);

	/**
	 * 删除设备
	 * @param products
	 * @return
	 */
	int deleteProducts(Products products);

	/**
	 * 查询分类下商品
	 * @param categoryId
	 * @return
	 */
	DataTablePaginator findProductsTable(String categoryId,DataTableParam param);

	/**
	 * 通过设备ID查询 设备信息
	 * @param productsId
	 * @return
	 */
	List<ProductsInfo> findProductsInfoByProductId(String productsId);

	/**
	 * 查询 设备的某种类型的信息
	 * @param productsId
	 * @param type
	 * @return
	 */
	ProductsInfo findProductsInfoByProductIdAndType(String productsId,int type);

	/**
	 * 查询所有 商品
	 * @return
	 */
	List<ProductsVO> findProductsVOList();

	/**
	 * 保存、修改设备图片集
	 * @param id
	 * @param productId
	 * @param imgPath
	 * @param description
	 * @return
	 */
	Integer saveOrUpdateGalleryOfProduct(Integer id,String productId,String imgPath,String description);

	/**
	 * 删除 图片集
	 * @param id
	 * @return
	 */
	int deleteGalleryOfProduct(Integer id);

	/**
	 * 更新状态为 已读
	 * @param messageId
	 * @return
	 */
	Message updateMessage(Integer messageId, Integer isRead);

	/**
	 * 删除消息
	 * @param messageId
	 * @return
	 */
	int deleteMessage(Integer messageId);

	/**
	 * 查询消息列表
	 * @param startTime
	 * @param endTime
	 * @param param
	 * @return
	 */
	DataTablePaginator findMessageTable(String startTime,String endTime,DataTableParam param);
}
