package com.kan.service.impl;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.common.mybatis.WhereBuilder;
import com.kan.common.utils.DateUtils;
import com.kan.common.utils.ShortUUID;
import com.kan.model.*;
import com.kan.persistence.*;
import com.kan.service.ProductsService;
import com.kan.vo.ProductsVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author mameng
 * @Date 2019/3/9 17:12
 * @Description:
 */
@Service
public class ProductsServiceImpl implements ProductsService {

	@Autowired
	private ProductsMapper productsMapper;
	@Autowired
	private ProductsInfoMapper productsInfoMapper;
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private UploadFileMapper uploadFileMapper;
	@Autowired
	private GalleryMapper galleryMapper;
	@Autowired
	private MessageMapper messageMapper;

	@Override
	public ProductsVO saveOrUpdateProducts(ProductsVO productsVO) {
		Products products = null;
		//更新商品基本信息
		if (StringUtils.isNotBlank(productsVO.getProductId())){
			products = getProductByProductId(productsVO.getProductId());
			if (products != null){
				if (null != productsVO.getImgId()){
					products.setImgId(productsVO.getImgId());
				}
				if (StringUtils.isNotBlank(productsVO.getProductImg())){
					products.setProductImg(productsVO.getProductImg());
				}
				products.setProductName(productsVO.getProductName());
				products.setCreateTime(System.currentTimeMillis()/1000);
				products.setCategoryId(productsVO.getCategoryId());
				productsMapper.update(products,"categoryId","productName","imgId","productImg","createTime");
			}
		}else {
			//新增
			products = new Products();
			products.setProductId(ShortUUID.generate());
			products.setProductName(productsVO.getProductName());
			products.setCategoryId(productsVO.getCategoryId());
			products.setImgId(productsVO.getImgId());
			products.setProductImg(productsVO.getProductImg());
			products.setCreateTime(System.currentTimeMillis()/1000);
			productsMapper.insert(products);
		}
		if (products != null){
			deleteProductsInfoByProductIdAndType(productsVO.getProductId(),productsVO.getType());
			productsVO.setProductId(products.getProductId());
			productsVO.setId(products.getId());
			addProductsInfo(productsVO);
			List<ProductsInfo> productsInfoByProductId = findProductsInfoByProductId(products.getProductId());
			for (ProductsInfo info : productsInfoByProductId){
				if (info.getInfoType() ==1){
					productsVO.setBenefit(info.getDescription());
				}else if (info.getInfoType() ==2){
					productsVO.setTechnicalData(info.getDescription());
				}else {
					productsVO.setOverview(info.getDescription());
				}
			}
		}
		return productsVO;
	}

	//查询 图片信息
	private UploadFile findUploadFileById(Integer id){
		if (null != id){
			return uploadFileMapper.get(id,UploadFile.class);
		}
		return null;
	}

	@Override
	public Products getProductByProductId(String productId) {
		Products products = new Products();
		products.setProductId(productId);
		return productsMapper.getBy(products, WhereBuilder.builder().and("productId").build(), Products.class);
	}

	@Override
	public ProductsVO getProductInfoByProductId(String productId) {
		ProductsVO vo = new ProductsVO();
		Products products = getProductByProductId(productId);
		if (products != null){
			BeanUtils.copyProperties(products,vo);
			List<ProductsInfo> productsInfoList = findProductsInfoByProductId(productId);
			if (null != productsInfoList && productsInfoList.size() > 0){
				for (ProductsInfo info : productsInfoList){
					if (info.getInfoType() ==1){
						vo.setBenefit(info.getDescription());
					}else if (info.getInfoType() ==2){
						vo.setTechnicalData(info.getDescription());
					}else {
						vo.setOverview(info.getDescription());
					}
				}
			}
			if (products.getImgId() != null){
				UploadFile uploadFileById = findUploadFileById(products.getImgId());
				if (uploadFileById != null){
					vo.setProductImg(uploadFileById.getPath());
					vo.setImgId(uploadFileById.getId());
				}
			}
			List<Gallery> listByProductId = getGalleryListByProductId(productId);
			vo.setGalleryList(listByProductId);
		}
		return vo;
	}

	private List<Gallery> getGalleryListByProductId(String productId){
		Gallery gallery = new Gallery();
		gallery.setProductId(productId);
		return galleryMapper.list(gallery, WhereBuilder.builder().and("productId").build(), null, null, null, null, Gallery.class);
	}

	@Override
	public List<String> getProductNameByIds(String productIds) {
		String[] split = productIds.split(",");
		return productsMapper.getProductNameByIds(Arrays.asList(split));
	}

	@Override
	public int deleteProducts(Products products) {
		productsMapper.delete(products, WhereBuilder.builder().and("productId").build());
		ProductsInfo info = new ProductsInfo();
		info.setProductId(products.getProductId());
		return productsInfoMapper.delete(info, WhereBuilder.builder().and("productId").build());
	}

	@Override
	public DataTablePaginator findProductsTable(String categoryId,DataTableParam param) {
		List<Products> list = productsMapper.getProductsList(categoryId, param.getiDisplayStart(), param.getiDisplayLength());

		List<ProductsVO> returnList = new ArrayList<>();
		Integer count = productsMapper.getProductsCount(categoryId);

		if (null != list && list.size() > 0){
			List<Category> categoryList = categoryMapper.getCategoryList("", null, null);
			Map<String,String> categoryIdAndName = new HashMap<>();
			if (null != categoryList){
				for (Category temp: categoryList){
					categoryIdAndName.put(temp.getCategoryId(),temp.getCategoryName());
				}
			}
			for (Products p : list){
				ProductsVO vo = new ProductsVO();
				BeanUtils.copyProperties(p,vo);
				if (StringUtils.isNotBlank(p.getCategoryId())){
					vo.setCategoryName(categoryIdAndName.get(p.getCategoryId()));
				}
				returnList.add(vo);
			}
		}

		DataTablePaginator paginator = new DataTablePaginator(param);
		paginator.setAaData(returnList);
		paginator.setiTotalRecords(count);
		paginator.setiTotalDisplayRecords(count);

		return paginator;
	}

	@Override
	public List<ProductsInfo> findProductsInfoByProductId(String productsId) {
		ProductsInfo info = new ProductsInfo();
		info.setProductId(productsId);
		return  productsInfoMapper.list(info,WhereBuilder.builder().and("productId").build(),null,null,null,null,ProductsInfo.class);
	}

	@Override
	public ProductsInfo findProductsInfoByProductIdAndType(String productsId, int type) {
		ProductsInfo info = new ProductsInfo();
		info.setProductId(productsId);
		info.setInfoType(type);
		return productsInfoMapper.getBy(info,WhereBuilder.builder().and("productId").and("infoType").build(),ProductsInfo.class);
	}

	@Override
	public List<ProductsVO> findProductsVOList() {
		List<ProductsVO> voList = new ArrayList<>();
		List<Products> productsList = productsMapper.getProductsList(null, null, null);
		List<ProductsInfo> productsInfoList = productsInfoMapper.list(null,null,null,null,null,null,ProductsInfo.class);
		Map<String,List<ProductsInfo>> productIdAndInfoMap = new HashMap<>();
		if (null != productsInfoList && productsInfoList.size() > 0){
			for (ProductsInfo info : productsInfoList){
				if (productIdAndInfoMap.get(info.getProductId()) != null){
					List<ProductsInfo> subList = productIdAndInfoMap.get(info.getProductId());
					subList.add(info);
				}else {
					List<ProductsInfo> subList = new ArrayList<>();
					subList.add(info);
					productIdAndInfoMap.put(info.getProductId(),subList);
				}
			}
		}
		if (null != productsList && productsList.size() > 0){
			for (Products temp : productsList){
				ProductsVO vo = new ProductsVO();
				BeanUtils.copyProperties(temp,vo);
				List<ProductsInfo> infoListOfProducts = productIdAndInfoMap.get(temp.getProductId());
				if (null != infoListOfProducts){
					for (ProductsInfo p : infoListOfProducts){
						if (p.getInfoType() ==1){
							vo.setBenefit(p.getDescription());
						}else if (p.getInfoType() ==2){
							vo.setTechnicalData(p.getDescription());
						}else {
							vo.setOverview(p.getDescription());
						}
					}
				}
				voList.add(vo);
			}
		}
		return voList;
	}

	@Override
	public Integer saveOrUpdateGalleryOfProduct(Integer id, String productId, String imgPath, String description) {
		Gallery gallery = new Gallery();
		if (id != null){
			gallery.setId(id);
			gallery.setImgPath(imgPath);
			gallery.setDescription(description);
			gallery.setCreateTime(System.currentTimeMillis()/1000);
			galleryMapper.update(gallery,"imgPath","description","createTime");
		}else {
			gallery.setImgPath(imgPath);
			gallery.setProductId(productId);
			gallery.setDescription(description);
			gallery.setCreateTime(System.currentTimeMillis()/1000);
			galleryMapper.insert(gallery);
		}
		return gallery.getId();
	}

	@Override
	public int deleteGalleryOfProduct(Integer id) {
		Gallery gallery = new Gallery();
		gallery.setId(id);
		return galleryMapper.delete(gallery,WhereBuilder.builder().and("id").build());
	}

	@Override
	public Message updateMessage(Integer messageId,Integer isRead) {
		Message message = messageMapper.get(messageId, Message.class);
		if (message != null && isRead == 0){
			message.setIsRead(1);
			messageMapper.update(message);
		}
		return message;
	}

	@Override
	public int deleteMessage(Integer messageId) {
		Message message = new Message();
		message.setId(messageId);
		return messageMapper.delete(message,WhereBuilder.builder().and("id").build());
	}

	@Override
	public DataTablePaginator findMessageTable(String startTime,String endTime,DataTableParam param) {
		Long start = null;
		Long end = null;
		if (StringUtils.isNotBlank(startTime)){
			String s = startTime + " 00:00:00";
			start = DateUtils.timeStr2Timestamp(s);
		}
		if (StringUtils.isNotBlank(endTime)){
			String e = endTime + " 23:59:59";
			end = DateUtils.timeStr2Timestamp(e);
		}

		int messageCount = messageMapper.getMessageCount(start,end);
		List<Message> messageList = messageMapper.getMessageList(start,end,param.getiDisplayStart(), param.getiDisplayLength());

		DataTablePaginator paginator = new DataTablePaginator(param);
		paginator.setAaData(messageList);
		paginator.setiTotalRecords(messageCount);
		paginator.setiTotalDisplayRecords(messageCount);

		return paginator;
	}


	private int addProductsInfo(ProductsVO productsVO){
		ProductsInfo info = new ProductsInfo();
		info.setInfoType(productsVO.getType());
		info.setProductId(productsVO.getProductId());
		info.setDescription("");
		if (productsVO.getType() ==1){
			info.setDescription(productsVO.getBenefit());
		}else if (productsVO.getType() ==2){
			info.setDescription(productsVO.getTechnicalData());
		}else {
			info.setDescription(productsVO.getOverview());
		}
		return productsInfoMapper.insert(info);
	}

	private int deleteProductsInfoByProductIdAndType(String productsId,int type){
		ProductsInfo info = new ProductsInfo();
		info.setProductId(productsId);
		info.setInfoType(type);
		return productsInfoMapper.delete(info,WhereBuilder.builder().and("productId").and("infoType").build());
	}

}
