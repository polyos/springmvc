package com.kan.service.impl;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.common.mybatis.WhereBuilder;
import com.kan.common.rest.RestClient;
import com.kan.model.AdminUser;
import com.kan.persistence.AdminUserMapper;
import com.kan.service.AdminUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by zxw on 2015/9/1.
 */
@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Autowired
    private AdminUserMapper adminUserMapper;
    @Autowired
    private RestClient restClient;

    @Override
    public AdminUser getAdminUserByName(String username) {
        Map<String, String> param = new HashMap<>();
        param.put("username", username);
        return adminUserMapper.getBy(param, WhereBuilder.builder().and("username").build(), AdminUser.class);
    }

    @Override
    public DataTablePaginator getUserList(String username, String department, DataTableParam param) {
        List<AdminUser> list = adminUserMapper.getUserList(username, param.getiDisplayStart(), param.getiDisplayLength());
        Integer count = adminUserMapper.getUserCount(username);

        DataTablePaginator paginator = new DataTablePaginator(param);
        paginator.setAaData(list);
        paginator.setiTotalRecords(count);
        paginator.setiTotalDisplayRecords(count);

        return paginator;
    }

    @Override
    public List<AdminUser> getUserList() {
        List<AdminUser> list = adminUserMapper.getAdminUserList();
        return list;
    }

    @Override
    public AdminUser getUser(Integer id) {
        AdminUser adminUser = adminUserMapper.get(id, AdminUser.class);
        return adminUser;
    }

    @Override
    public void addUser(AdminUser adminUser) {
        adminUser.setPassword(DigestUtils.md5Hex(adminUser.getPassword()));
        adminUser.setCreateTime(System.currentTimeMillis() / 1000);
        adminUserMapper.insert(adminUser, "username", "password", "createTime", "nick_name", "status");
    }

    @Override
    public void updateUser(AdminUser adminUser) {
        adminUser.setPassword(DigestUtils.md5Hex(adminUser.getPassword()));
        adminUserMapper.updateWhere(adminUser, WhereBuilder.builder().and("id").build(), "username", "password");
    }

}
