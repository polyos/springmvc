package com.kan.service.impl;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.common.mybatis.ParamBuilder;
import com.kan.common.mybatis.WhereBuilder;
import com.kan.common.mybatis.WhereCondition;
import com.kan.common.utils.ShortUUID;
import com.kan.model.Category;
import com.kan.persistence.CategoryMapper;
import com.kan.service.CategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/8 16:36
 * @Description:
 */

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryMapper categoryMapper;

	@Override
	public String saveOrupdateCategory(Category category) {
		category.setCreateTime(System.currentTimeMillis()/1000);
		if (StringUtils.isNotBlank(category.getCategoryId())){
			WhereCondition whereCondition = WhereBuilder.builder().and("categoryId").build();
			categoryMapper.updateWhere(category,whereCondition,"categoryName","categoryIcon","createTime");
		}else {
			category.setCategoryId(ShortUUID.generate());
			category.setIsValide(1);
			categoryMapper.insert(category);
		}
		return category.getCategoryId();
	}

	@Override
	public int deleteCategory(String categoryId) {
		Category category = new Category();
		category.setCategoryId(categoryId);
		category.setIsValide(0);
		return categoryMapper.updateWhere(category,WhereBuilder.builder().and("categoryId").build(),"isValide");
	}

	@Override
	public Category findCategoryByCategoryId(String categoryId) {
		return categoryMapper.getBy(ParamBuilder.builder().put("categoryId",categoryId).build(),WhereBuilder.builder().and("categoryId").build(),Category.class);
	}

	@Override
	public List<Category> findCategoryList() {
		return categoryMapper.getCategoryList("",null,null);
	}

	@Override
	public DataTablePaginator findCategoryTable(String categoryName,DataTableParam param) {
		List<Category> list = categoryMapper.getCategoryList(categoryName,param.getiDisplayStart(), param.getiDisplayLength());
		Integer count = categoryMapper.getCategoryCount(categoryName);

		DataTablePaginator paginator = new DataTablePaginator(param);
		paginator.setAaData(list);
		paginator.setiTotalRecords(count);
		paginator.setiTotalDisplayRecords(count);

		return paginator;
	}
}
