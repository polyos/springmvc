package com.kan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kan.model.UploadFile;
import com.kan.persistence.UploadFileMapper;
import com.kan.service.UploadFileService;

/**
 * Created by ZXW on 2014/9/3.
 */
@Service
public class UploadFileServiceImpl implements UploadFileService {

    @Autowired
    private UploadFileMapper uploadFileMapper;

    @Override
    @Transactional
    public UploadFile addUploadFile(String fileName, String dir, String oldName, String bucket) {
        UploadFile uploadFile = new UploadFile();
        uploadFile.setName(oldName);
        uploadFile.setDir(dir);
        uploadFile.setState(10);
        uploadFile.setPath(fileName);
        uploadFile.setCreateTime(System.currentTimeMillis()/1000);
        uploadFile.setBucket(bucket);
        uploadFileMapper.addUploadFile(uploadFile);
        return uploadFile;
    }

    @Override
    public UploadFile getUploadFileById(Integer id){
        return uploadFileMapper.getUploadFile(id);
    }

    @Override
    public List<UploadFile> getUploadFiles(String ids){
        return uploadFileMapper.getUploadFiles(ids);
    }

	@Override
	public String getFilePath(Integer imgId) {
		  return uploadFileMapper.getUploadFile(imgId).getPath();
	}

	@Override
	public boolean deleteImg(String id) {
		if(Integer.parseInt(id)>0){
			 UploadFile uploadFile = new UploadFile();
			 uploadFile.setId(Integer.parseInt(id));
			 uploadFile.setState(0);
			return uploadFileMapper.updateFile(uploadFile)>0;
		}else{
			return false;
		}
		
	}
    
    
}
