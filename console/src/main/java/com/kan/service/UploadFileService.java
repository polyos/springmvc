package com.kan.service;

import java.util.List;

import com.kan.model.UploadFile;

/**
 * Created by ZXW on 2014/9/3.
 */
public interface UploadFileService {

    public UploadFile addUploadFile(String fileName, String dir, String oldName, String bucket);

    public UploadFile getUploadFileById(Integer id);

    public List<UploadFile> getUploadFiles(String ids);

	public String getFilePath(Integer imgId);

	public boolean deleteImg(String id);

}
