package com.kan.service;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.model.Category;

import java.util.List;

/**
 * @author mameng
 * @Date 2019/3/8 16:35
 * @Description: 商品分类
 */
public interface CategoryService {

	/**
	 * 保存、更新 商品分类
	 * @param category
	 * @return
	 */
	String saveOrupdateCategory(Category category);

	/**
	 * 删除 分类
	 * @param categoryId
	 * @return
	 */
	int deleteCategory(String categoryId);

	/**
	 * 查询
	 * @param categoryId
	 * @return
	 */
	Category findCategoryByCategoryId(String categoryId);

	/**
	 * 查询分类的列表
	 * @return
	 */
	List<Category> findCategoryList();

	/**
	 * 查询 商品分类列表
	 * @return
	 */
	DataTablePaginator findCategoryTable( String categoryName,DataTableParam param);
}
