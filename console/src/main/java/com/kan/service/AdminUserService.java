package com.kan.service;

import com.kan.common.datatable.DataTablePaginator;
import com.kan.common.datatable.DataTableParam;
import com.kan.common.rest.exception.RestException;
import com.kan.model.AdminUser;
import com.kan.vo.AdminUserVO;

import java.util.List;

/**
 * Created by zxw on 2015/9/1.
 */
public interface AdminUserService {

    public AdminUser getAdminUserByName(String username);

    /**
     * 用户列表
     *
     * @param username
     * @param department
     * @param param
     * @return
     */
    public DataTablePaginator getUserList(String username, String department, DataTableParam param);

    /**
     * 获取用户
     *
     * @return
     */
    public List<AdminUser> getUserList();


    /**
     * ID查询
     *
     * @param id
     * @return
     */
    public AdminUser getUser(Integer id);

    /**
     * 添加用户
     *
     * @param adminUser
     */
    public void addUser(AdminUser adminUser);

    /**
     * 修改用户信息
     *
     * @param adminUser
     */
    public void updateUser(AdminUser adminUser);


}
