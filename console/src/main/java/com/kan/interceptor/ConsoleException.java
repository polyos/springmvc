package com.kan.interceptor;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kan.common.rest.exception.RestException;
import com.kan.common.validate.ValidateException;
import com.kan.utils.RestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ConsoleException extends SimpleMappingExceptionResolver {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {
        response.setCharacterEncoding("UTF-8");
        HandlerMethod hm = (HandlerMethod) o;
        if (e != null) {
            response.setContentType("application/json");
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonGenerator jsonGenerator = mapper.getFactory().createGenerator(
                        response.getOutputStream(), JsonEncoding.UTF8);
                response.setStatus(200);
                RestResult rr;
                if (e instanceof RestException) {
                    RestException re = (RestException) e;
                    rr = new RestResult(re.getCode());
                    rr.setErrorMessage(re.getReason());
                } else if (e instanceof TypeMismatchException) {
                    TypeMismatchException te = (TypeMismatchException) e;
                    rr = new RestResult(30002);
                    rr.setErrorMessage(te.getMessage());
                } else if (e instanceof MissingServletRequestParameterException) {
                    MissingServletRequestParameterException pe = (MissingServletRequestParameterException) e;
                    rr = new RestResult(30001);
                    rr.setErrorMessage("参数 " + pe.getParameterName() + " 不能为空，类型为" + pe.getParameterType());
                } else if (e instanceof BindException) {
                    BindException be = (BindException) e;
                    rr = new RestResult(30002);
                    List<String> list = new ArrayList<>();
                    for (FieldError fe : be.getFieldErrors()) {
                        list.add(fe.getDefaultMessage());
                    }
                    rr.setErrorMessage(list.toString());
                } else if (e instanceof ValidateException) {
                    rr = new RestResult(30002);
                    rr.setErrorMessage(e.getMessage());
                } else if (e instanceof HttpRequestMethodNotSupportedException) {
                    rr = new RestResult(400);
                    rr.setErrorMessage(e.getMessage());
                } else {
                    rr = new RestResult(-1);
                    rr.setErrorMessage("未知错误");
                    logger.error("未知错误", e);
                }
                mapper.writeValue(jsonGenerator, rr);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return new ModelAndView();
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
