package com.mam.mvc.service.impl;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ServiceLoader;

/**
 * @author mameng
 * @Date 16:32 2018/3/19
 * @Description:
 */
@Service
public class AwareServiceImpl implements BeanNameAware,ResourceLoaderAware{
	private String beanName;
	private ResourceLoader resourceLoader;

	@Override
	public void setBeanName(String name) {
		this.beanName = name;
	}

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	public void outPutResult(){
		System.out.println("Bean Name :" + beanName);
		Resource resource = resourceLoader.getResource("classpath:config/test.txt");
		try {
			System.out.println(IOUtils.toString(resource.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
