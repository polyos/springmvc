/*
Navicat MySQL Data Transfer

Source Server         : highvista_db
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : springcloud_highvista

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2019-03-07 08:58:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for is_category
-- ----------------------------
DROP TABLE IF EXISTS `is_category`;
CREATE TABLE `is_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(16) DEFAULT NULL,
  `category_name` varchar(200) DEFAULT NULL,
  `category_icon` varchar(200) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `is_valide` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of is_category
-- ----------------------------

-- ----------------------------
-- Table structure for is_contact_info
-- ----------------------------
DROP TABLE IF EXISTS `is_contact_info`;
CREATE TABLE `is_contact_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `websit` varchar(200) DEFAULT NULL,
  `products` text,
  `country` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `comment` text,
  `create_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of is_contact_info
-- ----------------------------

-- ----------------------------
-- Table structure for is_prodcuts_info
-- ----------------------------
DROP TABLE IF EXISTS `is_prodcuts_info`;
CREATE TABLE `is_prodcuts_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `product_type` int(4) DEFAULT NULL,
  `info_type` int(4) DEFAULT '0' COMMENT '0 overview 1 feature 2 technical data ',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of is_prodcuts_info
-- ----------------------------

-- ----------------------------
-- Table structure for is_products
-- ----------------------------
DROP TABLE IF EXISTS `is_products`;
CREATE TABLE `is_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `product_img` varchar(200) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of is_products
-- ----------------------------

-- ----------------------------
-- Table structure for is_users
-- ----------------------------
DROP TABLE IF EXISTS `is_users`;
CREATE TABLE `is_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(16) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `is_valide` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of is_users
-- ----------------------------
INSERT INTO `is_users` VALUES ('1', null, null, null, null, null, null);
