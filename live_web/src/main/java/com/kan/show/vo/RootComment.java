package com.kan.show.vo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.kan.common.utils.StringUtils;

/**
 * description: 组合评论模型
 * 
 * @author don
 * @date 2016年2月19日 下午2:28:34
 *
 */
public class RootComment extends CommentShow {
	private List<CommentShow> childCommentList;

	public List<CommentShow> getChildCommentList() {
		return childCommentList;
	}

	public void setChildCommentList(List<CommentShow> childCommentList) {
		if (childCommentList.isEmpty()) {
			this.childCommentList = new ArrayList<>();
		} else {
			this.childCommentList = StringUtils.toObject(StringUtils.toJson(childCommentList), new TypeReference<List<CommentShow>>() {
			});
		}
	}

}
