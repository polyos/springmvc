package com.kan.show.vo;

import javax.validation.constraints.NotNull;

public class BrowseHistoryVO {

//	@NotNull
	private Integer sessionId;
	private Integer userId;
	@NotNull
	private Integer projectId;
	private String projectThumbnailUrl;
	private String title;
	private Integer liveStatus;

	public Integer getLiveStatus() {
		return liveStatus;
	}

	public void setLiveStatus(Integer liveStatus) {
		this.liveStatus = liveStatus;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectThumbnailUrl(String projectThumbnailUrl) {
		this.projectThumbnailUrl = projectThumbnailUrl;
	}

	public String getProjectThumbnailUrl() {
		return projectThumbnailUrl;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

}
