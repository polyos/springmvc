package com.kan.show.vo;

import com.kan.common.datatable.DataTablePaginator;

/**
 * Created by mam on 2016/4/15.
 */
public class ProjectHopePageVO {
    private int isHaveProject;
    private String city;
    private DataTablePaginator page;

    public int getIsHaveProject() {
        return isHaveProject;
    }

    public void setIsHaveProject(int isHaveProject) {
        this.isHaveProject = isHaveProject;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public DataTablePaginator getPage() {
        return page;
    }

    public void setPage(DataTablePaginator page) {

        this.page = page;
    }
}
