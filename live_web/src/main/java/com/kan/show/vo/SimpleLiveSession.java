package com.kan.show.vo;

import com.kan.common.utils.StringUtils;

import java.io.Serializable;

/**
 * description: APP 直播演出 相关类
 * @author don
 * @date 2015年11月17日 下午3:41:10
 *
 */
public class SimpleLiveSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer regionId;
	private Integer projectId;
	private String projectName;
	private Integer status;// 演出状态
	private String subtitle;
	private String recWord;
	private Long liveTime;

	private Integer praiseNum;
	private Integer browseNum;
	private Integer followNum;

	private Integer isSellTicket;
	private Integer liveStatus;// 直播状态 null 为不直播 ；其余0 ，1 ，2
	private String pubNode;
	private String roomName;
	private String roomPassword;

	private Integer projectType;
	private String projectTypeUrl;// 分类ICON url
	private String projectTypeName;
	private String regionName;
	private String stadiumName;

	private Integer coverWidth;
	private Integer coverHeight;
	private String coverPath;
	private String landscapeUrl;
	private String posterUrl;// 更多期待，项目海报，多图
	private String linkUrl;

	private Integer isLive;

	public Integer getIsLive() {
		return isLive;
	}

	public void setIsLive(Integer isLive) {
		this.isLive = isLive;
	}

	public String getProjectTypeUrl() {
		return  projectTypeUrl;
		/*return ServiceUrls.getPhoneWeb() + this.projectTypeUrl;*/
	}

	public void setProjectTypeUrl(String projectTypeUrl) {
		this.projectTypeUrl = projectTypeUrl;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Integer getCoverWidth() {
		return coverWidth;
	}

	public void setCoverWidth(Integer coverWidth) {
		this.coverWidth = coverWidth;
	}

	public Integer getCoverHeight() {
		return coverHeight;
	}

	public void setCoverHeight(Integer coverHeight) {
		this.coverHeight = coverHeight;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public String getLandscapeUrl() {
		return landscapeUrl;
	}

	public void setLandscapeUrl(String landscapeUrl) {
		this.landscapeUrl = landscapeUrl;
	}

	public Integer getProjectType() {
		return projectType;
	}

	public void setProjectType(Integer projectType) {
		this.projectType = projectType;
	}

	public String getProjectTypeName() {
		return projectTypeName;
	}

	public void setProjectTypeName(String projectTypeName) {
		this.projectTypeName = projectTypeName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomPassword() {
		return roomPassword;
	}

	public void setRoomPassword(String roomPassword) {
		this.roomPassword = roomPassword;
	}

	public String getPubNode() {
		return pubNode;
	}

	public void setPubNode(String pubNode) {
		this.pubNode = pubNode;
	}

	public String getPosterUrl() {
		return posterUrl;
	}

	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}

	public Integer getPraiseNum() {
		return praiseNum;
	}

	public void setPraiseNum(Integer praiseNum) {
		this.praiseNum = praiseNum;
	}

	public Integer getBrowseNum() {
		return browseNum;
	}

	public void setBrowseNum(Integer browseNum) {
		this.browseNum = browseNum;
	}

	public Integer getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Integer followNum) {
		this.followNum = followNum;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getRecWord() {
		return recWord;
	}

	public void setRecWord(String recWord) {
		this.recWord = recWord;
	}

	public Integer getIsSellTicket() {
		return isSellTicket;
	}

	public void setIsSellTicket(Integer isSellTicket) {
		this.isSellTicket = isSellTicket;
	}

	public Integer getLiveStatus() {
		return liveStatus;
	}

	public void setLiveStatus(Integer liveStatus) {
		this.liveStatus = liveStatus;
	}

	public Long getLiveTime() {
		return liveTime;
	}

	public void setLiveTime(Long liveTime) {
		this.liveTime = liveTime;
	}

	/**
	 * description:  初始化统计参数
	 * @param browseNum
	 * @param praiseNum
	 * @param followNum
	 *
	 * @author don
	 * @date 2015年11月18日 下午2:50:19
	 */
	public void setNum(int browseNum, int praiseNum, int followNum) {
		this.browseNum = browseNum;
		this.praiseNum = praiseNum;
		this.followNum = followNum;
	}

	/**
	 * description:  设置海报信息
	 * @param poster
	 *
	 * @author don
	 * @date 2016年1月7日 下午5:59:07
	 */
	public void setPoster(Poster poster) {
		if (poster != null) {
			this.setCoverHeight(poster.getCoverHeight());
			this.setCoverWidth(poster.getCoverWidth());
			this.setCoverPath(poster.getCoverPath());
			this.setPosterUrl(poster.getPosterUrl());
			this.setLandscapeUrl(poster.getLandscapeUrl());
		} else {
			// 默认海报图
			this.setCoverHeight(null);
			this.setCoverWidth(null);
			this.setCoverPath(null);
			this.setLandscapeUrl(null);
			this.setPosterUrl(null);
		}

	}

	public <T> T conver(Class<T> tClass) {
		T t = null;

		try {
			t = tClass.newInstance();
			String json = StringUtils.toJson(this);
			t = StringUtils.toObject(json, tClass);
		} catch (InstantiationException | IllegalAccessException e) {
		}
		return t;
	}

	public String getStadiumName() {
		return stadiumName;
	}

	public void setStadiumName(String stadiumName) {
		this.stadiumName = stadiumName;
	}
}
