package com.kan.show.vo;

import java.io.Serializable;

/**
 * description: APP 直播演出 相关类
 * @author don
 * @date 2015年11月17日 下午3:41:10
 *
 */
public class HistorySimpleLiveSession extends SimpleLiveSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer isPraise;

	public Integer getIsPraise() {
		return isPraise;
	}

	public void setIsPraise(Integer isPraise) {
		this.isPraise = isPraise;
	}

}
