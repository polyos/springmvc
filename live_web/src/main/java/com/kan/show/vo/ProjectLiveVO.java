package com.kan.show.vo;

/**
 * 直播项目VO Created by mam on 2016/1/14.
 */
public class ProjectLiveVO {
	private Integer id;
	private String projectName;
	private String subTitle;
	private String regionName;
	private String coverPath;
	private String landscapeUrl;
	private Long liveTime;
	private Integer sessionId;

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Long getLiveTime() {
		return liveTime;
	}

	public void setLiveTime(Long liveTime) {
		this.liveTime = liveTime;
	}

	public String getLandscapeUrl() {
		return landscapeUrl;
	}

	public void setLandscapeUrl(String landscapeUrl) {
		this.landscapeUrl = landscapeUrl;
	}
}
