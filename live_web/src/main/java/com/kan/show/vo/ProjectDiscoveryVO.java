package com.kan.show.vo;

/**
 * 发现 项目 VO
 * Created by mam on 2016/1/14.
 */
public class ProjectDiscoveryVO {
    private Integer id;
    private String projectName;
    private String word;
    private String landscapeUrl;
    private String coverPath;
    private String linkUrl;
    private String projectType;
    private Integer followNum;
    private Integer browseNum;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLandscapeUrl() {
        return landscapeUrl;
    }

    public void setLandscapeUrl(String landscapeUrl) {
        this.landscapeUrl = landscapeUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public Integer getFollowNum() {
        return followNum;
    }

    public void setFollowNum(Integer followNum) {
        this.followNum = followNum;
    }

    public Integer getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
