package com.kan.show.vo;

public class ProjectHopeVO {


	private Integer id;
	private String projectName;
	private Integer projectType;
	private Integer projectNature;
	private Integer manageNature;
	private Integer salesMode;
	private String sponsor;
	private String organizer;
	private Long startDate;
	private Long endDate;
	private Long createTime;
	private Integer isShow;

	private String projectTypeName;

	//项目是否卖票
	private int isSell;


	public void setProjectName(String projectName){
		this.projectName=projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setProjectType(Integer projectType){
		this.projectType=projectType;
	}

	public Integer getProjectType(){
		return projectType;
	}

	public void setProjectNature(Integer projectNature){
		this.projectNature=projectNature;
	}

	public Integer getProjectNature(){
		return projectNature;
	}

	public void setManageNature(Integer manageNature){
		this.manageNature=manageNature;
	}

	public Integer getManageNature(){
		return manageNature;
	}

	public void setSalesMode(Integer salesMode){
		this.salesMode=salesMode;
	}

	public Integer getSalesMode(){
		return salesMode;
	}

	public void setSponsor(String sponsor){
		this.sponsor=sponsor;
	}

	public String getSponsor(){
		return sponsor;
	}

	public void setOrganizer(String organizer){
		this.organizer=organizer;
	}

	public String getOrganizer(){
		return organizer;
	}

	public void setStartDate(Long startDate){
		this.startDate=startDate;
	}

	public Long getStartDate(){
		return startDate;
	}

	public void setEndDate(Long endDate){
		this.endDate=endDate;
	}

	public Long getEndDate(){
		return endDate;
	}

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getProjectTypeName() {
		return projectTypeName;
	}

	public void setProjectTypeName(String projectTypeName) {
		this.projectTypeName = projectTypeName;
	}

	public int getIsSell() {
		return isSell;
	}

	public void setIsSell(int isSell) {
		this.isSell = isSell;
	}
}
