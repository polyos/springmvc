package com.kan.show.vo;

/**
 * Created by mam on 2015/11/20.
 */
public class SenderChipVO {
	private Integer id;
	private String thumbnail;
	private Integer duration;
	private String url;
	private String urlH;
	private String mimeType;
	private String videoTime;

	public String getVideoTime() {
		return videoTime;
	}

	public void setVideoTime(String videoTime) {
		this.videoTime = videoTime;
	}

	// 是否被收藏
	private Integer isCollected;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		int hour = (int) duration / 3600;
		int min = (duration % 3600) / 60;
		int sec = duration % 60;
		if (hour > 0) {
			setVideoTime(hour + ":" + min + ":" + sec);
		} else {
			setVideoTime(min + ":" + sec);
		}
		this.duration = duration;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public Integer getIsCollected() {
		return isCollected;
	}

	public void setIsCollected(Integer isCollected) {
		this.isCollected = isCollected;
	}

	public String getUrlH() {
		return urlH;
	}

	public void setUrlH(String urlH) {
		this.urlH = urlH;
	}
}
