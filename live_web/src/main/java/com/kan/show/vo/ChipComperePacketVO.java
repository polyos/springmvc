package com.kan.show.vo;

import java.util.List;

public class ChipComperePacketVO {

    private Integer id;
    private String title;
    private String topicTitle;
    private String topicType;
    private String roomName;
    private Integer browerNum;
    private Integer barrageNum;
    private String userName;
    private Integer userType;
    private Integer userId;
    private String userIcon;
    private Long createTime;
    private Integer coverId;

    private List<SenderChipVO> senderChipVOList;

    //奖励金额
    private Double balance;
    //状态 1未保存 11保存后，未审核 22 审核通过
    private Integer status;

    //是否已发送
    private Integer isSend;


    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setBrowerNum(Integer browerNum) {
        this.browerNum = browerNum;
    }

    public Integer getBrowerNum() {
        return browerNum;
    }

    public void setBarrageNum(Integer barrageNum) {
        this.barrageNum = barrageNum;
    }

    public Integer getBarrageNum() {
        return barrageNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Integer getCoverId() {
        return coverId;
    }

    public void setCoverId(Integer coverId) {
        this.coverId = coverId;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public List<SenderChipVO> getSenderChipVOList() {
        return senderChipVOList;
    }

    public void setSenderChipVOList(List<SenderChipVO> senderChipVOList) {
        this.senderChipVOList = senderChipVOList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTopicType() {
        return topicType;
    }

    public void setTopicType(String topicType) {
        this.topicType = topicType;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Integer getIsSend() {
        return isSend;
    }

    public void setIsSend(Integer isSend) {
        this.isSend = isSend;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
