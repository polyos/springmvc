package com.kan.show.vo;

import java.io.Serializable;

public class LiveShow implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer sessionId;
	private Long liveTime;
	private Integer status;
	private String roomName;
	private String roomPassword;

	private String pubNode;

	private Integer projectId;

	private String projectName;
	private String recWord;
	private String subtitle;

	private Integer projectType;
	private Integer regionId;
	private String regionName;

	private Integer isShow;

	public Integer getProjectType() {
		return projectType;
	}

	public void setProjectType(Integer projectType) {
		this.projectType = projectType;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getRecWord() {
		return recWord;
	}

	public void setRecWord(String recWord) {
		this.recWord = recWord;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getPubNode() {
		return pubNode;
	}

	public void setPubNode(String pubNode) {
		this.pubNode = pubNode;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setLiveTime(Long liveTime) {
		this.liveTime = liveTime;
	}

	public Long getLiveTime() {
		return liveTime;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomPassword(String roomPassword) {
		this.roomPassword = roomPassword;
	}

	public String getRoomPassword() {
		return roomPassword;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}
}
