package com.kan.show.vo;

import java.io.Serializable;

public class Project implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String projectName;
	private Integer projectType;
	private Integer projectNature;
	private Integer manageNature;
	private Integer salesMode;
	private String sponsor;
	private String organizer;
	private Long startDate;
	private Long endDate;
	private Long createTime;
	private Integer isShow;
	private Integer isTopRec;
	private Integer organId;

	public Integer getOrganId() {
		return organId;
	}

	public void setOrganId(Integer organId) {
		this.organId = organId;
	}

	public Integer getIsTopRec() {
		return isTopRec;
	}

	public void setIsTopRec(Integer isTopRec) {
		this.isTopRec = isTopRec;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectType(Integer projectType) {
		this.projectType = projectType;
	}

	public Integer getProjectType() {
		return projectType;
	}

	public void setProjectNature(Integer projectNature) {
		this.projectNature = projectNature;
	}

	public Integer getProjectNature() {
		return projectNature;
	}

	public void setManageNature(Integer manageNature) {
		this.manageNature = manageNature;
	}

	public Integer getManageNature() {
		return manageNature;
	}

	public void setSalesMode(Integer salesMode) {
		this.salesMode = salesMode;
	}

	public Integer getSalesMode() {
		return salesMode;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getCreateTime() {
		return createTime;
	}

}
