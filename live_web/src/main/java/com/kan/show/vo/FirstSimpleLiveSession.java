package com.kan.show.vo;

import java.io.Serializable;

/**
 * description: APP 直播演出 相关类
 * @author don
 * @date 2015年11月17日 下午3:41:10
 *
 */
public class FirstSimpleLiveSession extends SimpleLiveSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private String lat;
	private String lon;
	private Integer isPraise;

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public Integer getIsPraise() {
		return isPraise;
	}

	public void setIsPraise(Integer isPraise) {
		this.isPraise = isPraise;
	}

}
