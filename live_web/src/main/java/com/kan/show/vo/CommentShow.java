package com.kan.show.vo;

import com.kan.common.AliOSS;

public class CommentShow {

	private Integer id;
	private Integer sendId;
	private String sendName;
	private Integer photoId;
	private String photoUrl;
	private Integer praiseNum;
	private Integer receiveId;
	private String receiveName;
	private String content;
	private Integer sessionId;
	private Long createTime;
	private Integer parentId;
	private Integer childNum;
	private Integer status;
	private Integer projectId;

	private Integer isRecommend;
	private Integer rootId;

	private Integer type;
	private Integer audioTime;
	private String audioUrl;

	private Integer topWeight;
	private Integer isEssence;

	private Integer isPraise;
	private Double projectScore;

	public Integer getIsPraise() {
		return isPraise;
	}

	public void setIsPraise(Integer isPraise) {
		this.isPraise = isPraise;
	}

	public Double getProjectScore() {
		return projectScore;
	}

	public void setProjectScore(Double projectScore) {
		this.projectScore = projectScore;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getAudioTime() {
		return audioTime;
	}

	public void setAudioTime(Integer audioTime) {
		this.audioTime = audioTime;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public Integer getTopWeight() {
		return topWeight;
	}

	public void setTopWeight(Integer topWeight) {
		this.topWeight = topWeight;
	}

	public Integer getIsEssence() {
		return isEssence;
	}

	public void setIsEssence(Integer isEssence) {
		this.isEssence = isEssence;
	}

	public Integer getRootId() {
		return rootId;
	}

	public void setRootId(Integer rootId) {
		this.rootId = rootId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public CommentShow() {

	}

	public Integer getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(Integer isRecommend) {
		this.isRecommend = isRecommend;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setSendId(Integer sendId) {
		this.sendId = sendId;
	}

	public Integer getSendId() {
		return sendId;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSendName() {
		return sendName;
	}

	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}

	public Integer getPhotoId() {
		return photoId;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getPhotoUrl() {
		if (photoUrl == null) {
			return null;
		}
		if (photoUrl.startsWith("http")) {
			return photoUrl;
		}
		return AliOSS.fullUrl(photoUrl);
	}

	public void setPraiseNum(Integer praiseNum) {
		this.praiseNum = praiseNum;
	}

	public Integer getPraiseNum() {
		return praiseNum;
	}

	public void setReceiveId(Integer receiveId) {
		this.receiveId = receiveId;
	}

	public Integer getReceiveId() {
		return receiveId;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setChildNum(Integer childNum) {
		this.childNum = childNum;
	}

	public Integer getChildNum() {
		return childNum;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

}
