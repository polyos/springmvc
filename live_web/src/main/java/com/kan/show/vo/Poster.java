package com.kan.show.vo;

import com.kan.common.AliOSS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Poster implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer projectId;
	private Integer posterId;
	private String posterUrl;
	private String showIntroduce;
	private String showIntroduceText;
	private Integer thumbnailId;
	private String thumbnailUrl;
	private Integer coverWidth;
	private Integer coverHeight;
	private Integer coverId;
	private String coverPath;
	private String landscapeUrl;
	private List<String> coverPathList;

	public List<String> getCoverPathList() {
		return coverPathList;
	}

	public void setCoverPathList(List<String> coverPathList) {
		this.coverPathList = coverPathList;
	}

	public String getLandscapeUrl() {
		if (landscapeUrl == null) {
			return null;
		}
		return AliOSS.fullUrl(landscapeUrl);
	}

	public void setLandscapeUrl(String landscapeUrl) {
		this.landscapeUrl = landscapeUrl;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setPosterId(Integer posterId) {
		this.posterId = posterId;
	}

	public Integer getPosterId() {
		return posterId;
	}

	public void setPosterUrl(String posterUrl) {
		coverPathList = new ArrayList<>();
		String[] urls = posterUrl.split("\\|");
		for (String string : urls) {
			coverPathList.add(string);
		}
		this.posterUrl = posterUrl;
	}

	public String getPosterUrl() {
		if (posterUrl == null) {
			return "";
		}
		String[] url = posterUrl.split("\\|");
		StringBuilder postSb = new StringBuilder();
		for (String string : url) {
			postSb.append(AliOSS.fullUrl(string));
			postSb.append("|");
		}
		return postSb.substring(0, postSb.length() - 1);
	}

	public void setShowIntroduce(String showIntroduce) {
		this.showIntroduce = showIntroduce;
	}

	public String getShowIntroduce() {
		return showIntroduce;
	}

	public void setThumbnailId(Integer thumbnailId) {
		this.thumbnailId = thumbnailId;
	}

	public Integer getThumbnailId() {
		return thumbnailId;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getThumbnailUrl() {
		if (thumbnailUrl == null) {
			return "";
		}
		String[] url = thumbnailUrl.split("\\|");
		StringBuilder postSb = new StringBuilder();
		for (String string : url) {
			postSb.append(AliOSS.fullUrl(string));
			postSb.append("|");
		}
		return postSb.substring(0, postSb.length() - 1);
	}

	public void setCoverWidth(Integer coverWidth) {
		this.coverWidth = coverWidth;
	}

	public Integer getCoverWidth() {
		return coverWidth;
	}

	public void setCoverHeight(Integer coverHeight) {
		this.coverHeight = coverHeight;
	}

	public Integer getCoverHeight() {
		return coverHeight;
	}

	public void setCoverId(Integer coverId) {
		this.coverId = coverId;
	}

	public Integer getCoverId() {
		return coverId;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public String getCoverPath() {
		if (coverPath == null) {
			return null;
		}
		return AliOSS.fullUrl(coverPath);
	}

	public String getCoverPathWithOutDomain() {
		return coverPath;
	}

	public String getShowIntroduceText() {
		return showIntroduceText;
	}

	public void setShowIntroduceText(String showIntroduceText) {
		this.showIntroduceText = showIntroduceText;
	}
}
