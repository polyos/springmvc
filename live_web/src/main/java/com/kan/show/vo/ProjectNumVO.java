package com.kan.show.vo;

import java.io.Serializable;

/**
 * description: 项目浏览、点赞、关注统计数
 * @author don
 * @date 2016年1月6日 下午6:42:38
 *
 */
public class ProjectNumVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int browseNum;
	private int praiseNum;
	private int followNum;

	private int projectId;

	public int getBrowseNum() {
		return browseNum;
	}

	public void setBrowseNum(int browseNum) {
		this.browseNum = browseNum;
	}

	public int getPraiseNum() {
		return praiseNum;
	}

	public void setPraiseNum(int praiseNum) {
		this.praiseNum = praiseNum;
	}

	public int getFollowNum() {
		return followNum;
	}

	public void setFollowNum(int followNum) {
		this.followNum = followNum;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
}
