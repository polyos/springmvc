package com.kan.show.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kan.common.AliOSS;
import com.kan.utils.Tools;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Video implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String picUrl;
	private Integer type;
	private String videoUrl;
	private Integer showId;

	private Integer thumbnailId;
	private String thumbnailUrl;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPicUrl() {
		if (picUrl == null) {
			return null;
		}
		if (picUrl.startsWith("http")) {
			return picUrl;
		}
		return AliOSS.fullUrl(picUrl);
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getType() {
		return type;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setShowId(Integer showId) {
		this.showId = showId;
	}

	public Integer getShowId() {
		return showId;
	}

	public Integer getThumbnailId() {
		return thumbnailId;
	}

	public void setThumbnailId(Integer thumbnailId) {
		this.thumbnailId = thumbnailId;
	}

	public String getThumbnailUrl() {
		if (thumbnailUrl == null) {
			return null;
		}
		return AliOSS.fullUrl(thumbnailUrl);
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
}
