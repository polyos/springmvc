package com.kan.service;

import com.kan.common.rest.exception.RestException;
import com.kan.show.vo.HaokanLiveVO;

import java.util.List;

/**
 * Created by mam on 2016/1/14.
 */
public interface HaoKanLiveService {

    /**
     * 好看回顾
     * @return
     * @throws RestException
     */
    public List<HaokanLiveVO> kanLiveList() throws RestException;

}
