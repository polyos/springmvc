package com.kan.service;

import com.kan.show.vo.HistorySimpleLiveSession;

import java.util.List;

/**
 * Created by mam on 2016/3/4.
 */

public interface ProjectHistoryService {

    /**
     *获取直播回放 记录
     * @return
     */
    public List<HistorySimpleLiveSession> historyList();
}
