package com.kan.service;

import com.kan.show.vo.SimpleLiveSession;

import java.util.List;

/**
 * Created by mam on 2016/3/3.
 */

public interface ProjectDiscoveryService {

    public List<SimpleLiveSession> projectDiscoveryList();
}
