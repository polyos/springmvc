package com.kan.service.Impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.kan.common.Paginator;
import com.kan.common.memcached.CacheKeyConstant;
import com.kan.common.mybatis.ParamBuilder;
import com.kan.common.rest.RestClient;
import com.kan.common.rest.exception.RestException;
import com.kan.service.ProjectDiscoveryService;
import com.kan.show.vo.*;
import com.kan.utils.ServiceUrls;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by mam on 2016/3/3.
 */

@Service
public class ProjectDiscoveryServiceImpl implements ProjectDiscoveryService {

	@Autowired
	private RestClient restClient;
/*	@Autowired
	private CacheClient cacheClient;*/

	@Override
	public List<SimpleLiveSession> projectDiscoveryList() {
		List<SimpleLiveSession> temp = new ArrayList<>();
		// 1 即将上映 2 历史回顾
		Integer timeType = 1;
		// 排序类型：0：场次的开场时间，1：浏览量
		Integer sortType = 0;

		Map<String, Object> argMap = new HashMap<String, Object>();
		argMap.put("pageNum", 0);
		argMap.put("pageSize", 9);
		argMap.put("timeType", timeType);

		Paginator paginator = null;
		ProjectHopePageVO phpv = null;
		try {
			phpv = restClient.get(ServiceUrls.getShowUrl() + "/project/hope/not_start", ProjectHopePageVO.class, argMap);
			paginator = new Paginator();
			paginator.setPageNum(1);
			paginator.setPageSize(9);
			paginator.setTotalCount(phpv.getPage().getiTotalDisplayRecords());
			List<ProjectHopeVO> projectList = com.kan.common.utils.StringUtils.toObject(com.kan.common.utils.StringUtils.toJson(phpv.getPage().getAaData()),
					new TypeReference<List<ProjectHopeVO>>() {
					});
			paginator.setResults(projectList);

			if (null == projectList || projectList.size() == 0) {
				return temp;
			}
			// 封装 projectId
			StringBuilder sb = new StringBuilder();
			for (ProjectHopeVO project : projectList) {
				sb.append(project.getId() + ",");
			}

			// 海报信息
			Map<String, Object> map = new HashMap<>();

			// 拼接海报
			map.put("projectIds", sb.toString());
			Map<Integer, Poster> posterMap = getPosterList(sb);
			Map<Integer, Object> sessionMap = getSessionByProjectIds(sb.toString());
			Iterator<ProjectHopeVO> ite = projectList.iterator();
			while (ite.hasNext()) {
				SimpleLiveSession simpleLiveSession = new SimpleLiveSession();
				ProjectHopeVO projectHopeVO = ite.next();
				Map<String, Object> stringMap = (Map<String, Object>) sessionMap.get(projectHopeVO.getId().toString());
				ProjectNumVO projectNum = getProjectNum(projectHopeVO.getId());

				// 浏览数、点赞数 统计
				if (null != projectNum) {
					simpleLiveSession.setNum(projectNum.getBrowseNum(), projectNum.getPraiseNum(), projectNum.getFollowNum());
				} else {
					simpleLiveSession.setNum(0, 0, 0);
				}
				simpleLiveSession.setPoster(posterMap.get(projectHopeVO.getId()));
				simpleLiveSession.setProjectId(projectHopeVO.getId());
				simpleLiveSession.setProjectName(projectHopeVO.getProjectName());
				simpleLiveSession.setProjectType(projectHopeVO.getProjectType());
				simpleLiveSession.setProjectTypeName(projectHopeVO.getProjectTypeName());
				if (null != stringMap) {
					if (null != stringMap.get("subtitle") && "" != stringMap.get("subtitle")) {
						simpleLiveSession.setSubtitle(stringMap.get("subtitle").toString());
					}
					if (null != stringMap.get("recWord") && "" != stringMap.get("recWord")) {
						simpleLiveSession.setRecWord(stringMap.get("recWord").toString());
					}
				}
				temp.add(simpleLiveSession);
			}
			paginator.setResults(temp);

		} catch (RestException e) {

		}
		return temp;
	}

	public Map<Integer, Poster> getPosterList(StringBuilder projectIds) throws RestException {
		Map<Integer, Poster> posterMap = new HashMap<Integer, Poster>();

		StringBuilder missIds = new StringBuilder();
		List<Integer> projectIdList = new ArrayList<>();

		// 查询数据
		if (!StringUtils.isNotBlank(projectIds.toString())) {
			return new HashMap<Integer, Poster>();
		}
		String[] ids = projectIds.toString().split(",");

		for (String id : ids) {
			int projectId;
			try {
				projectId = Integer.parseInt(id);
			} catch (Exception e) {
				continue;
			}
			projectIdList.add(projectId);
			/*Poster poster = cacheClient.get(CacheConstant.CACHE_NAME_SHOW, CacheConstant.SHOW_POSTER + projectId);*/
			/*if (poster != null) {
				posterMap.put(projectId, poster);
			} else {
				missIds.append(projectId + ",");
			}*/
		}

		if (missIds.length() > 0) {
			List<Poster> missPosterList = restClient.list(ServiceUrls.getShowUrl() + "/project/poster/list", Poster.class,
					ParamBuilder.builder().put("projectIds", missIds).build());
			for (Poster t : missPosterList) {
				Integer projectId = t.getProjectId();
//				cacheClient.put(CacheConstant.CACHE_NAME_SHOW, CacheConstant.SHOW_POSTER + projectId, t, CacheConstant.ASL_CACHE_TIME);
				posterMap.put(projectId, t);
			}
		}
		return posterMap;
	}

	// 查询 副标题、推荐词
	public Map<Integer, Object> getSessionByProjectIds(String projectIds) throws RestException {
		return restClient.get(ServiceUrls.getShowUrl() + "/session/first/{projectIds}", HashMap.class, projectIds);
	}

	// 统计 浏览数、点赞数
	public ProjectNumVO getProjectNum(Integer id) {
		String key = CacheKeyConstant.APP_USER_PROJECTNUMS + id;
		ProjectNumVO sunVO = null;
		/*ProjectNumVO sunVO = cacheClient.get(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_PROJECTNUMS);*/
		if (sunVO != null) {
			return sunVO;
		}
		try {
			sunVO = restClient.get(ServiceUrls.getUserUrl() + "/user/project/num/{projectId}", ProjectNumVO.class, id);
		} catch (RestException e) {
			e.printStackTrace();
			sunVO = new ProjectNumVO();
			sunVO.setBrowseNum(0);
			sunVO.setFollowNum(0);
			sunVO.setPraiseNum(0);
			sunVO.setProjectId(id);
			return sunVO;
		}
		/*cacheClient.put(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_PROJECTNUMS + id, sunVO, CacheConstant.APP_CACHE_TIME);*/
		return sunVO;
	}
}
