package com.kan.service.Impl;

import com.kan.common.rest.RestClient;
import com.kan.common.rest.exception.RestException;
import com.kan.service.HaoKanLiveService;
import com.kan.show.vo.HaokanLiveVO;
import com.kan.utils.ServiceUrls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mam on 2016/1/14.
 */

@Service
public class HaoKanLiveServiceImpl implements HaoKanLiveService {

    @Autowired
    private RestClient restClient;

    @Override
    public List<HaokanLiveVO> kanLiveList() throws RestException {
        return restClient.list(ServiceUrls.getShowUrl() + "/kan_live_list", HaokanLiveVO.class, null);
    }
}
