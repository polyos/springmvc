package com.kan.service.Impl;

import com.kan.common.Paginator;
import com.kan.common.mybatis.ParamBuilder;
import com.kan.common.rest.RestClient;
import com.kan.common.rest.exception.RestException;
import com.kan.service.ProjectHistoryService;
import com.kan.show.vo.*;
import com.kan.utils.ServiceUrls;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mam on 2016/3/4.
 */

@Service
public class ProjectHistoryServiceImpl implements ProjectHistoryService {

	@Autowired
	private RestClient restClient;
	/*@Autowired
	private CacheClient cacheClient;*/

	@Override
	@SuppressWarnings("unchecked")
	public List<HistorySimpleLiveSession> historyList() {

		// 是否倒序：0：否，1是（不传则默认为1）
		Integer reverse = 1;

		Map<String, Object> argMap = new HashMap<String, Object>();
		argMap.put("pageNum", 0);
		argMap.put("pageSize", 9);
		argMap.put("reverse", reverse);

		List<HistorySimpleLiveSession> sessionList = new ArrayList<>();
		try {
			StringBuilder sb = new StringBuilder();
			List<LiveShow> liveList = new ArrayList<>();
			// 时间排序
			Paginator paginator = restClient.page(ServiceUrls.getLiveUrl() + "/session/live/history/page", LiveShow.class, argMap);
			liveList = paginator.getResults();

			if (liveList == null || liveList.size() == 0) {
				return sessionList;
			}
			Map<Integer, LiveShow> liveMap = new HashMap<>();
			sb = new StringBuilder();
			for (LiveShow liveShow : liveList) {
				sb.append(liveShow.getSessionId() + ",");
				liveMap.put(liveShow.getSessionId(), liveShow);
			}

			argMap = new HashMap<String, Object>();
			argMap.put("liveIds", sb);
			argMap.put("isIn", 1);

			sessionList = getSimpleSessionList(argMap, HistorySimpleLiveSession.class);
			if (sessionList == null || sessionList.size() == 0) {
				return sessionList;
			}

			// 查询当前用户的点赞记录
			/*
			 * Map<Integer, Integer> praiseUserMap = new HashMap<>(); try {
			 * Integer userId = ClientInstanceHolder.getUserId(); List<Integer>
			 * session_idList = liveUserService.getUserPraiseList(userId); for
			 * (Integer integer : session_idList) { praiseUserMap.put(integer,
			 * 1); } } catch (RestException e) { }
			 */

			// 查询统计数据并封装
			Map<String, Object> pb = ParamBuilder.builder().put("sessionIds", sb).build();

			Map<Integer, SessionUserNumVO> sessionNumsMap = getSessionNumList(sb);

			sb = new StringBuilder();
			for (HistorySimpleLiveSession simpleLiveSession : sessionList) {
				Integer id = simpleLiveSession.getId();
				sb.append(simpleLiveSession.getProjectId() + ",");

				// 拼接user浏览属性
				SessionUserNumVO sessionNum = sessionNumsMap.get(id);
				if (null != sessionNum) {
					simpleLiveSession.setNum(sessionNum.getBrowseNum(), sessionNum.getPraiseNum(), sessionNum.getFollowNum());
				} else {
					simpleLiveSession.setNum(0, 0, 0);
				}

				/*
				 * if (praiseUserMap.get(id) == null) {
				 * simpleLiveSession.setIsPraise(0); } else {
				 * simpleLiveSession.setIsPraise(1); }
				 */
				// 拼接 直播属性
				/*
				 * LiveShow ls = liveMap.get(id); if (ls != null) {
				 * simpleLiveSession.setLiveStatus(ls.getStatus());
				 * simpleLiveSession.setLiveTime(ls.getLiveTime());
				 * simpleLiveSession.setRoomName(ls.getRoomName());
				 * simpleLiveSession.setPubNode(ls.getPubNode()); }
				 */

			}

			// 拼接 项目海报
			pb.put("projectIds", sb);
			Map<Integer, Poster> posterMap = getPosterList(sb);
			for (HistorySimpleLiveSession simpleLiveSession : sessionList) {
				simpleLiveSession.setPoster(posterMap.get(simpleLiveSession.getProjectId()));
			}

		} catch (RestException e) {

		}
		return sessionList;
	}

	public <T> List<T> getSimpleSessionList(Map<String, Object> argMap, Class<T> clazz) throws RestException {
		Map<Integer, SimpleLiveSession> simpleMap = new HashMap<Integer, SimpleLiveSession>();
		StringBuilder missIds = new StringBuilder();
		List<Integer> sessionIdList = new ArrayList<>();
		// 查询数据
		String sessionIds = ((StringBuilder) argMap.get("liveIds")).toString();
		String[] ids = sessionIds.split(",");
		for (String id : ids) {
			int sessionId;
			try {
				sessionId = Integer.parseInt(id);
			} catch (Exception e) {
				continue;
			}
			sessionIdList.add(sessionId);
			/*SimpleLiveSession simpleSession = cacheClient.get(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_SIMPLE_SESSION + sessionId);*/
			SimpleLiveSession simpleSession = null;
			if (simpleSession != null) {
				simpleMap.put(sessionId, simpleSession);
			} else {
				missIds.append(sessionId + ",");
			}
		}
		if (missIds.length() > 0) {
			List<SimpleLiveSession> missLiveList = restClient.list(ServiceUrls.getShowUrl() + "/show/smpsession/ids/list", SimpleLiveSession.class, argMap);
			for (SimpleLiveSession t : missLiveList) {
				Integer sessionId = t.getId();
				/*cacheClient.put(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_SIMPLE_SESSION + sessionId, t, CacheConstant.APP_CACHE_TIME);*/
				simpleMap.put(sessionId, t);
			}
		}
		// 排序后输出
		List<T> list = new ArrayList<>();
		for (Integer sessionId : sessionIdList) {
			SimpleLiveSession t = simpleMap.get(sessionId);
			if (t != null) {
				list.add(t.conver(clazz));
			}
		}
		return list;

	}

	// 场次的 浏览、点赞 信息
	public Map<Integer, SessionUserNumVO> getSessionNumList(StringBuilder sessionIds) throws RestException {
		Map<Integer, SessionUserNumVO> sessionNumsMap = new HashMap<Integer, SessionUserNumVO>();

		StringBuilder missIds = new StringBuilder();
		List<Integer> sessionIdList = new ArrayList<>();

		// 查询数据
		if (!org.apache.commons.lang3.StringUtils.isNotBlank(sessionIds.toString())) {
			return new HashMap<Integer, SessionUserNumVO>();
		}

		String[] ids = sessionIds.toString().split(",");
		for (String id : ids) {
			int sessionId;
			try {
				sessionId = Integer.parseInt(id);
			} catch (Exception e) {
				continue;
			}
			sessionIdList.add(sessionId);
			/*SessionUserNumVO sunVO = cacheClient.get(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_SESSIONNUMS + sessionId);*/
			SessionUserNumVO sunVO = null;
			if (sunVO != null) {
				sessionNumsMap.put(sessionId, sunVO);
			} else {
				missIds.append(sessionId + ",");
			}
		}

		if (missIds.length() > 0) {
			List<SessionUserNumVO> missLiveList = restClient.list(ServiceUrls.getUserUrl() + "/user/session/nums/list", SessionUserNumVO.class, ParamBuilder
					.builder().put("sessionIds", missIds).build());

			for (SessionUserNumVO t : missLiveList) {
				Integer sessionId = t.getSessionId();
				sessionNumsMap.put(sessionId, t);
			}
		}

		return sessionNumsMap;
	}

	// 场次的 海报 信息
	public Map<Integer, Poster> getPosterList(StringBuilder projectIds) throws RestException {
		Map<Integer, Poster> posterMap = new HashMap<Integer, Poster>();

		StringBuilder missIds = new StringBuilder();
		List<Integer> projectIdList = new ArrayList<>();

		// 查询数据
		if (!StringUtils.isNotBlank(projectIds.toString())) {
			return new HashMap<Integer, Poster>();
		}
		String[] ids = projectIds.toString().split(",");

		for (String id : ids) {
			int projectId;
			try {
				projectId = Integer.parseInt(id);
			} catch (Exception e) {
				continue;
			}
			projectIdList.add(projectId);
			/*Poster poster = cacheClient.get(CacheConstant.CACHE_NAME_SHOW, CacheConstant.SHOW_POSTER + projectId);*/
			Poster poster = null;

			if (poster != null) {
				posterMap.put(projectId, poster);
			} else {
				missIds.append(projectId + ",");
			}
		}

		if (missIds.length() > 0) {
			List<Poster> missPosterList = restClient.list(ServiceUrls.getShowUrl() + "/project/poster/list", Poster.class,
					ParamBuilder.builder().put("projectIds", missIds).build());
			for (Poster t : missPosterList) {
				Integer projectId = t.getProjectId();
//				cacheClient.put(CacheConstant.CACHE_NAME_SHOW, CacheConstant.SHOW_POSTER + projectId, t, CacheConstant.ASL_CACHE_TIME);
				posterMap.put(projectId, t);
			}
		}
		return posterMap;
	}
}
