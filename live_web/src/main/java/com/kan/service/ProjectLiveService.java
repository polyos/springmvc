package com.kan.service;

import com.kan.common.rest.exception.RestException;
import com.kan.show.vo.LiveShow;

/**
 * Created by mam on 2016/3/3.
 */

public interface ProjectLiveService {

    /**
     * description:  获取今日现场的liveshow对象
     * @return
     *
     * @author don
     * @date 2016年1月6日 下午3:21:41
     */
    public LiveShow getFirstLiveShow() throws RestException;

    /**
     * description:  通过sessionId获取SimpleLiveSession
     * @param sessionId
     * @return
     *
     * @author don
     * @date 2015年12月16日 下午2:17:51
     */
    public <T> T getSimpleSessionBySessionId(Class<T> tClass, Integer sessionId) throws RestException;

}
