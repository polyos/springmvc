package com.kan.utils;

import com.kan.common.utils.Configuration;

/**
 * Created by zxw on 2015/8/8.
 */
public class Tools {

    private static String mediaDomain = null;

    public static String getMediaDomain(){
        if(mediaDomain == null){
            mediaDomain = Configuration.getProperty("media.domain");
        }
        return mediaDomain;
    }

}
