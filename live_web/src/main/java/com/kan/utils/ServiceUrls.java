package com.kan.utils;

import com.kan.common.utils.Configuration;

/**
 * Created by zxw on 2015/7/22.
 */
public class ServiceUrls {

    public static String getUserUrl(){
        return Configuration.getProperty("service.user.url");
    }

    public static String getShowSupportUrl(){
        return Configuration.getProperty("service.show_support.url");
    }

    public static String getShowUrl(){
        return Configuration.getProperty("service.show.url");
    }

    public static String getMarketing(){
        return Configuration.getProperty("service.marketing.url");
    }

    public static String getUserRelationUrl(){
        return Configuration.getProperty("service.user_relation.url");
    }

	public static String getConsoleUrl() {
		return Configuration.getProperty("service.console.url");
	}
    public static String getLiveUrl() {
        return Configuration.getProperty("service.live.url");
    }

}
