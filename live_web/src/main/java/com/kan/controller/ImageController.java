package com.kan.controller;

import com.kan.common.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by zxw on 2016/2/24.
 */
@Controller
public class ImageController {

	@Autowired
	private RestClient restClient;
/*	@Autowired
	private CacheClient cacheClient;*/

	@RequestMapping(value = "/project_cover_lw.htm")
	public void projectCover(Integer projectId, HttpServletResponse response) {
		response.setContentType("image/jpeg");
		if (projectId != null) {
			/*try {
				Object val = cacheClient.get(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_COVER_GAUSS_CACHE+projectId);
				if (val != null) {
					response.getOutputStream().write((byte[]) val);
				} else {
					synchronized (CacheConstant.LIVEWEB_COVER_GAUSS_CACHE + projectId) {
						val = cacheClient.get(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_COVER_GAUSS_CACHE+projectId);
						if (val == null) {
							Poster poster = restClient.get(ServiceUrls.getShowUrl() + "/project/poster/{projectId}", Poster.class, projectId);
							if (poster == null) {
								return;
							}
							byte[] bytes = AliOSS.download(poster.getCoverPathWithOutDomain());
							byte[] target = ImageUtils.addBlur(bytes, 40, 600, 600);
							if (target != null) {
								cacheClient.put(CacheConstant.CACHE_NAME_LIVE_WEB, CacheConstant.LIVEWEB_COVER_GAUSS_CACHE+projectId,600);
								response.getOutputStream().write(target);
							}
						} else {
							response.getOutputStream().write((byte[]) val);
						}
					}
				}
			} catch (RestException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}*/
		}
	}

}
