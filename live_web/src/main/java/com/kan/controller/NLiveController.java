package com.kan.controller;

import com.kan.common.Paginator;
import com.kan.common.mybatis.ParamBuilder;
import com.kan.common.rest.AsyncRestClient;
import com.kan.common.rest.RestClient;
import com.kan.common.rest.exception.RestException;
import com.kan.common.utils.DateUtils;
import com.kan.service.HaoKanLiveService;
import com.kan.service.ProjectDiscoveryService;
import com.kan.service.ProjectHistoryService;
import com.kan.show.vo.*;
import com.kan.utils.ServiceUrls;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author don description:新版web页 控制器
 * @date 2016年3月2日 下午5:00:32
 */
@Controller
public class NLiveController {
	@Autowired
	private HaoKanLiveService liveService;
	@Autowired
	private ProjectDiscoveryService discoveryService;
	@Autowired
	private ProjectHistoryService historyService;
	@Autowired
	private RestClient restClient;
	@Autowired
	private AsyncRestClient asyncRestClient;

	/*@Autowired
	private CacheClient cacheClient;*/

	/**
	 * description: 首页
	 * 
	 * @param map
	 * @return
	 * @author don
	 * @date 2016年3月2日 下午5:36:25
	 */
	@RequestMapping(value = "/index.htm", method = RequestMethod.GET)
	public String index(ModelMap map) {
		/*ProjectLiveVO project = getProjectLiveInfo();*/
		ProjectLiveVO project = null;
		map.put("project", project);
		map.put("plusTime", project.getLiveTime() - System.currentTimeMillis() / 1000);
		return "/res-v/pages/index";
	}

	/**
	 * description: 精彩回顾
	 * 
	 * @param map
	 * @return
	 *
	 * @author don
	 * @date 2016年3月4日 下午3:55:05
	 */
	@RequestMapping(value = "/history.htm", method = RequestMethod.GET)
	public String history(ModelMap map) {
		List<HistorySimpleLiveSession> historyList = historyService.historyList();
		map.put("historyList", historyList);
		return "/res-v/pages/history";
	}

	@RequestMapping(value = "/discover.htm", method = RequestMethod.GET)
	public String discover(ModelMap map) {
		List<SimpleLiveSession> projectList = discoveryService.projectDiscoveryList();
		map.put("projectList", projectList);
		return "/res-v/pages/discover";
	}

	@RequestMapping(value = "/aboutus.htm", method = RequestMethod.GET)
	public String aboutus(ModelMap map) {
		return "/res-v/pages/about";
	}

	@RequestMapping(value = "/company.htm", method = RequestMethod.GET)
	public String companyShow(ModelMap map) {
		return "/res-v/pages/company";
	}

	@RequestMapping(value = "/download.htm", method = RequestMethod.GET)
	public String appDownload(ModelMap map) {
		return "/res-v/pages/download";
	}

	/**
	 * description: 直播详情
	 * 
	 * @param map
	 * @param from
	 *            1从首页过来，2从历史回顾过来
	 * @return
	 *
	 * @author don
	 * @date 2016年3月4日 下午3:31:20
	 */
	@RequestMapping(value = "/live/details.htm", method = RequestMethod.GET)
	public String liveDetails(@RequestParam(required = true) Integer sessionId, Integer from, ModelMap map) {

		try {

			/*
			 * private Integer sessionId; private Integer userId;
			 * 
			 * @NotNull private Integer projectId; private String
			 * projectThumbnailUrl; private String title; private Integer
			 * liveStatus;
			 */

			BrowseHistoryVO bhvo = new BrowseHistoryVO();

			/*SimpleLiveSession simpleSession = projectLiveService.getSimpleSessionBySessionId(SimpleLiveSession.class, sessionId);*/
			SimpleLiveSession simpleSession = null;
			if (simpleSession != null) {
				Poster poster = restClient.get(ServiceUrls.getShowUrl() + "/project/poster/{projectId}", Poster.class, simpleSession.getProjectId());

				bhvo.setSessionId(sessionId);
				bhvo.setProjectId(simpleSession.getProjectId());
				if (poster != null) {
					bhvo.setProjectThumbnailUrl(poster.getCoverPath());
				}
				bhvo.setTitle(simpleSession.getProjectName());
				bhvo.setLiveStatus(2);
				asyncRestClient.post(ServiceUrls.getUserUrl() + "/user/session/browse", Integer.class, com.kan.common.utils.StringUtils.BeanToMap(bhvo));
				/*CacheHandler.del(memcachedClient, CacheKeyConstant.APP_USER_SESSIONNUMS + sessionId);*/
				map.put("poster", poster);
				Future<Paginator> fChipPage = asyncRestClient.page(ServiceUrls.getLiveUrl() + "/get_chip_history_app", ChipComperePacketVO.class, ParamBuilder
						.builder().put("userId", 0).put("sessionId", sessionId).put("pageSize", 100).put("pageNum", 1).put("order", null).put("time", null)
						.build());
				map.put("fChipList", fChipPage.get().getResults());
			}
			map.put("session", simpleSession);
		} catch (RestException | InterruptedException | ExecutionException | IllegalAccessException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
		}
		if (from != null && from == 2) {
		} else {
			from = 1;
		}

		map.put("from", from);
		map.put("DateUtils", new DateUtils());
		return "/res-v/pages/in-pages/live";
	}

	/**
	 * description: 项目详情
	 * 
	 * @param projectId
	 * @param map
	 * @return
	 *
	 * @author don
	 * @date 2016年3月5日 下午3:29:30
	 */
	@RequestMapping(value = "/show/details.htm", method = RequestMethod.GET)
	public String showDetails(@RequestParam(required = true) Integer projectId, ModelMap map) {
		try {
			Future<String> sessionIds = asyncRestClient.get(ServiceUrls.getShowUrl() + "/project/session_ids", String.class,
					ParamBuilder.builder().put("projectId", projectId).build());
			Future<Paginator> fRootComment = asyncRestClient.page(ServiceUrls.getShowSupportUrl() + "/comment/show/project_id", RootComment.class, ParamBuilder
					.builder().put("projectId", projectId).build());

			Future<Paginator> fVideo;
			if (null != sessionIds.get()) {
				fVideo = asyncRestClient.page(ServiceUrls.getShowSupportUrl() + "/project/video", Video.class,
						ParamBuilder.builder().put("sessionIds", sessionIds.get()).put("pageNum", 1).put("pageSize", Integer.MAX_VALUE).build());
			} else {
				fVideo = null;
			}
			if (fVideo != null) {
				Paginator page = fVideo.get();
				if (page != null && page.getTotalCount() > 0) {
					map.put("videos", page.getResults());
				}
			}

			Poster poster = restClient.get(ServiceUrls.getShowUrl() + "/project/poster/{projectId}", Poster.class, projectId);
			if (poster != null) {
				map.put("poster", poster);
				List<String> postList = poster.getCoverPathList();
				String postImage = null;
				if (postList != null && postList.size() > 0) {
					postImage = postList.get(0);
				}
				if (StringUtils.isNotBlank(postImage)) {
					map.put("postImage", postImage);
				} else {
					map.put("postImage", poster.getCoverPath());
				}
			}
			Project project = restClient.get(ServiceUrls.getShowUrl() + "/project/{id}", Project.class, projectId);

			// 添加浏览记录
			BrowseHistoryVO bhvo = new BrowseHistoryVO();
			bhvo.setProjectId(projectId);
			if (poster != null) {
				bhvo.setProjectThumbnailUrl(poster.getCoverPath());
			}
			bhvo.setTitle(project.getProjectName());
			asyncRestClient.post(ServiceUrls.getUserUrl() + "/user/session/browse", Integer.class, com.kan.common.utils.StringUtils.BeanToMap(bhvo));
			/*CacheHandler.del(memcachedClient, CacheKeyConstant.APP_USER_SESSIONNUMS + projectId);*/
			map.put("rootComments", fRootComment.get().getResults());
			map.put("fProject", project);
		} catch (RestException | InterruptedException | ExecutionException | IllegalAccessException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
		}
		map.put("projectId", projectId);
		map.put("DateUtils", new DateUtils());
		return "/res-v/pages/in-pages/show";
	}

	// 直播场次信息

	/*public ProjectLiveVO getProjectLiveInfo() {
		ProjectLiveVO projectLiveVO = new ProjectLiveVO();
		try {
			LiveShow liveshow = projectLiveService.getFirstLiveShow();
			if (liveshow == null) {
				return projectLiveVO;
			} else {
				FirstSimpleLiveSession simpleSession = projectLiveService.getSimpleSessionBySessionId(FirstSimpleLiveSession.class, liveshow.getSessionId());
				Poster poster = restClient.get(ServiceUrls.getShowUrl() + "/project/poster/{projectId}", Poster.class, liveshow.getProjectId());

				if (null != simpleSession) {
					String projectName = simpleSession.getProjectName();
					int num = 0;
					if (StringUtils.isNotBlank(projectName) && (num = projectName.indexOf("|")) > -1) {
						projectLiveVO.setProjectName(projectName.substring(0, num));
						projectLiveVO.setSubTitle(projectName.substring(num + 1));
					} else {
						projectLiveVO.setProjectName(projectName);
					}
					projectLiveVO.setSessionId(simpleSession.getId());
					projectLiveVO.setLiveTime(liveshow.getLiveTime());
					projectLiveVO.setRegionName(simpleSession.getRegionName());
				}

				if (poster != null) {
					projectLiveVO.setCoverPath(poster.getCoverPath());
					projectLiveVO.setLandscapeUrl(poster.getLandscapeUrl());
				}
			}
		} catch (RestException e) {
		}
		return projectLiveVO;
	}*/

}
