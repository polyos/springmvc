package com.kan.controller;

import com.kan.common.AliOSS;
import com.kan.common.rest.RestClient;
import com.kan.common.rest.exception.RestException;
import com.kan.common.utils.StringUtils;
import com.kan.service.HaoKanLiveService;
import com.kan.show.vo.HaokanLiveVO;
import com.kan.utils.Tools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mam on 2016/1/13.
 */


public class LiveController {
    @Autowired
    private HaoKanLiveService liveService;
    @Autowired
    private RestClient restClient;

    //首页
    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    //介绍页
    @RequestMapping(value = "/show.htm", method = RequestMethod.GET)
    public String show(ModelMap modelMap) {
        List<HaokanLiveVO> kanList = new ArrayList<>();
        try {
            kanList = liveService.kanLiveList();
        } catch (RestException e) {
            e.printStackTrace();
        }
        if (null != kanList && kanList.size() > 0) {
            for (HaokanLiveVO haokanLiveVO : kanList) {
                String temp = haokanLiveVO.getProjectName();
                if (org.apache.commons.lang3.StringUtils.isNotBlank(temp)) {
                    int index = temp.indexOf("|");
                    if (index > -1) {
                        haokanLiveVO.setProjectName(getStringByBit(temp.substring(0, index), 22));
                        haokanLiveVO.setSubTitle(getStringByBit(temp.substring(index + 1), 22));
                    } else {
                        haokanLiveVO.setProjectName(getStringByBit(temp, 40));

                    }
                }
            }
        }
        modelMap.put("kanList", kanList);
        modelMap.put("aliOSS", new AliOSS());
        modelMap.put("StringUtils", new StringUtils());
        return "show";
    }

    //发现
   /* @RequestMapping(value = "/discovery.htm", method = RequestMethod.GET)
    public String discovery(Integer id, ModelMap modelMap) {
        ProjectDiscoveryVO project = new ProjectDiscoveryVO();
        try {
            project = liveService.queryProjectDiscovery(id);
        } catch (RestException e) {
            e.printStackTrace();
        }
        if (null == project) {
            return "redirect:/show.htm";
        }
        String str = "";
        try {
            str = restClient.get(project.getLinkUrl(), String.class, null);
        } catch (RestException e) {
            e.printStackTrace();
        }
        *//*project.setLinkUrl(str);*//*
        modelMap.put("project", project);
        modelMap.put("domain", Tools.getMediaDomain());
        return "discovery";
    }*/

    //关于 页
    @RequestMapping(value = "/about.htm", method = RequestMethod.GET)
    public String about() {
        return "about";
    }

    @RequestMapping(value = "/company.htm", method = RequestMethod.GET)
    public String company() {
        return "company";
    }

    //下载页
    @RequestMapping(value = "/download.htm", method = RequestMethod.GET)
    public String download() {
        return "download";
    }

    public static String getStringByBit(String str, int byteLength) {
        try {
            if(str.equals(new String(str.getBytes("iso8859-1"), "iso8859-1")))
                str = new String(str.getBytes("iso8859-1"), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String strs[] = str.split("");
        StringBuffer stringBuffer = new StringBuffer();
        int hasRead = 0; // 已读取的字节数
        if (str.getBytes().length <= byteLength) {
            return str;
        } else {
            for (int i = 0; i < strs.length; i++) {
                if (strs[i].getBytes().length == 1) {
                    hasRead++;
                    str += strs[i];
                    stringBuffer.append(strs[i]);
                }
                if (strs[i].getBytes().length == 2) {
                    hasRead += 2;
                    str += strs[i];
                    stringBuffer.append(strs[i]);
                }
                if (hasRead >= byteLength)
                    break;
            }
            if (str.getBytes().length > byteLength) {
                stringBuffer.append("...");
            }
        }
        return stringBuffer.toString();
    }


}
