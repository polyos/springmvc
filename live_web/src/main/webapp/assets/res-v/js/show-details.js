var img_swiper;
var curMedia;
$(document).ready(function() {
	$(".simple-audio-player").player({
		progressbarWidth : '100px',
		progressbarHeight : '5px',
		progressbarColor : '#22ccff',
		progressbarBGColor : '#eeeeee',
	});
	// $(".help-debug").text($(window).width());
	img_swiper = new Swiper('.show-details-swipe', {
		centeredSlides : true,
		autoHeight : true,
		onSlideChangeStart : function(swiper) {
			$(".show-nav-ul>li").eq(swiper.activeIndex).addClass("nav-li-active").siblings().removeClass("nav-li-active");
		}
	});

	$(".show-nav-ul>li").click(function() {
		img_swiper.slideTo($(this).index());
		$(this).addClass("nav-li-active").siblings().removeClass("nav-li-active");
	})

	// $(window).resize(function() {
	// $(".help-debug").text($(window).width())
	// })

	$(".comment-time").each(function() {
		var cTime = parseInt($.trim($(this).text()));
		var newTime = "";
		if (isNaN(cTime) || ("" + cTime).length == 4) {

		} else {
			var now = parseInt(new Date().getTime() / 1000);
			var plus = now - cTime;
			var today = new Date().getDate();
			var plusDay = today - new Date(cTime * 1000).getDate();

			newTime = new Date(cTime * 1000).format("yyyy/MM/dd hh:ss");
			if (plusDay > 0 && plusDay <= 2) {
				if (plusDay == 1) {
					newTime = "昨天" + new Date(cTime * 1000).format(" hh:ss");
				} else {
					newTime = "前天" + new Date(cTime * 1000).format(" hh:ss");
				}
			} else {
				if (plus > 0 && plus < 3600) {
					newTime = parseInt(plus / 60) + "分钟前";
				} else if (plus >= 3600 && plus < 36000) {
					newTime = parseInt(plus / 3600) + "小时前";
				}
			}
		}
		$(this).html(newTime);
	})

	$(".replace-project-name").each(function() {
		$(this).text($.trim($(this).text()).replace(/\|/g, ""))
	})

	$(".live-play-btn").click(function() {
		if (curMedia) {
			if (curMedia.type == "video") {
				curMedia.media.trigger("pause").css({
					"z-index" : -1,
					"height" : "auto",
				});
			} else {
				curMedia.media.siblings(".live-chip-audio").trigger("pause");
			}
		}
		$(this).siblings(".live-chip-video").trigger("play").css({
			"z-index" : 300,
			"max-height" : "none",
			"height" : "100%",
		});
		curMedia = {
			media : $(this).siblings(".live-chip-video"),
			type : "video"
		}
	})
})

/**
 * 时间对象的格式化;
 */
Date.prototype.format = function(format) {
	/*
	 * eg:format="yyyy-MM-dd hh:mm:ss";
	 */
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	// millisecond
	}

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}

	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;

}

function playEnd(target) {
	$(target).css({
		"z-index" : -1
	});
}

function videoCanPlay(target){
//	alert(1);
}
function videoOnAbort(target){
//	alert(1);
}