$(document).ready(function() {
	// new Date("month dd,yyyy hh:mm:ss");
	initJcSlider();
	setInterval(function() {
		runBackCount(--plusTime);
	}, 1000);
})

function initJcSlider() {
	$("#demo1").jcSlider({
		loadPath : contextPath + '/assets/res-v/img/loading.gif'
	});

	$("#demo2").jcSlider({
		speed : "easeInOutQuart",
		Default : 1,
		setMode : 'fade',
		loadPath : contextPath + '/assets/res-v/img/loading.gif',
		setloadSize : {
			loadWidth : 32,
			loadHeight : 32
		},
		autoPlay : true,
		autoTime : 500,
		arrow : false,
		numBtn : true,
		numBtnEvent : 'mouseover',
		numBtnPos : 'right',
		setNumBtn : {
			x : 0,
			y : 275
		},
		scaling : true
	});

	$("#demo3").jcSlider({
		speed : "easeInOutExpo",
		loadPath : 'img/loading.gif',
		Default : 1,
		setMode : 'y',
		autoPlay : true,
		autoTime : 500,
		arrow : true,
		arrowOffsetX : 90,
		subtle : {
			prev : 6,
			next : 10
		},
		numBtn : false,
		scaling : false
	});
}

function runBackCount(plusTime) {
	var plus = plusTime;
	if (!isNaN(plus) && plus > 0) {
		setTimeS(plus);
	} else if (plus <= 0) {
		$("#show-back").hide();
		$(".index-enter").show();
	} else {
		$("#back-colock").text("00:00:00");

	}
}

function setTimeS(plus) {
	var hour = parseInt(plus / 3600);
	var min = parseInt((plus % 3600) / 60);
	var sec = parseInt(plus % 60);
	var s = "";
	if (hour == 0) {
		s = "00";
	} else if (hour > 0 && hour < 10) {
		s = "0" + hour;
	} else if (hour > 99) {
		s = "99";
	} else {
		s = "" + hour;
	}
	s += ":";
	if (min == 0) {
		s += "00";
	} else if (min > 0 && min < 10) {
		s += "0" + min;
	} else {
		s += "" + min;
	}
	s += ":";
	if (sec == 0) {
		s += "00";
	} else if (sec > 0 && sec < 10) {
		s += "0" + sec;
	} else {
		s += "" + sec;
	}
	$("#back-colock").text(s);
}
