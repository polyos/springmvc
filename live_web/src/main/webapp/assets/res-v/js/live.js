var img_swiper;
var curMedia;
$(document).ready(function() {
	img_swiper = new Swiper('.chip-media-swiper', {
		pagination : '.swiper-pagination',
		paginationType : 'fraction',
		prevButton : '.swiper-button-prev',
		nextButton : '.swiper-button-next',
		paginationType : 'fraction',
		centeredSlides : true,
		paginationClickable : true,
		// Disable preloading of all images
		preloadImages : false,
		// Enable lazy loading
		lazyLoading : true
	});

	$(".chip-log-time").each(function() {
		var duration = parseInt($(this).text());

		if (duration == null || isNaN(duration)) {
			$(this).html("  ");
		}

		var hour = parseInt(duration / 3600);
		var min = parseInt((duration % 3600) / 60);
		var sec = parseInt(duration % 60);

		if (hour > 0) {
			$(this).html(hour + ":" + min + ":" + sec);
		} else {
			min = min < 10 ? "0" + min : min;
			sec = sec < 10 ? "0" + sec : sec;
			$(this).html(min + ":" + sec);
		}
	});

	$(".live-play-btn").click(function() {
		if (curMedia) {
			if (curMedia.type == "video") {
				curMedia.media.trigger("pause").css({
					"z-index" : -1,
					"height" : "auto",
				});
			} else {
				curMedia.media.siblings(".live-chip-audio").trigger("pause");
			}
		}
		$(this).siblings(".live-chip-video").trigger("play").css({
			"z-index" : 300,
			"height" : "100%",
			"max-height" : "none",
		});
		curMedia = {
			media : $(this).siblings(".live-chip-video"),
			type : "video",
			packetid : parseInt($(this).attr("packetid")),
		}
	})

	$(".audio-btn").click(function() {
		var packetid = parseInt($(this).parent().siblings(".live-chip-audio").attr("packetid"));
		if (curMedia) {
			if (curMedia.type == "video") {
				curMedia.media.trigger("pause").css({
					"z-index" : -1
				});
			} else {
				if (packetid == curMedia.packetid) {
					if (curMedia.status == "play") {
						curMedia.media.siblings(".live-chip-audio").trigger("pause");
						curMedia.status = "pause";
						return;
					} else {
						curMedia.media.siblings(".live-chip-audio").trigger("play");
						curMedia.status = "play";
						return;
					}
				} else {
					curMedia.media.siblings(".live-chip-audio").trigger("pause");
				}
			}
		}
		$(this).parent().siblings(".live-chip-audio").trigger("play");
		curMedia = {
			media : $(this).parent(),
			type : "audio",
			packetid : parseInt($(this).parent().siblings(".live-chip-audio").attr("packetid")),
			status : "play",
		}
	})

	$(".replace-project-name").each(function() {
		$(this).text($.trim($(this).text()).replace(/\|/g, ""))
	})

	$(".swiper-button-prev,.swiper-button-next").click(function(event) {
		event.preventDefault();
		event.stopPropagation();
	});
	$(".chip-media-group").click(function() {
		$(this).css({
			"visibility" : "hidden",
		});
		$("body").css({
			overflow : "auto"
		});
	})

})

function noChipImg(target) {
	$(target).remove();
}

function playEnd(target) {
	$(target).css({
		"z-index" : -1
	});
}

function openImageSwipe(packetId) {

	var item = '<div class="swiper-slide"><img data-src="IMAGEURL" class="swiper-lazy"><div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div></div>';
	var appendDom = new Array();

	$("#chip-" + packetId).children().each(function() {
		appendDom.push(item.replace(/IMAGEURL/g, $.trim($(this).text())));
	})
	img_swiper.removeAllSlides();
	img_swiper.appendSlide(appendDom);
	$(".chip-media-group").css({
		"top" : $(document).scrollTop() + "px",
		"visibility" : "visible",
	});

	$("body").css({
		overflow : "hidden"
	});
	img_swiper.update();
	img_swiper.slideTo(1);
	setTimeout(function() {
		img_swiper.slideTo(0);
	}, 50)
}

function videoCanPlay(target) {
	// alert("videoCanPlay");
}
function videoOnAbort(target) {
	// alert("videoOnAbort");
}
function videoOnpause(target) {
	// alert("videoOnpause");
}
function onWaiting(target) {
	// alert("onWaiting");
}
