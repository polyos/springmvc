package com.kan.common.memcached;

/**
 * description: 缓存key
 * 
 * @author don
 * @date 2016年1月6日 上午11:10:44
 *
 */
public class CacheKeyConstant {

	// app_api缓存时间
	public static final int APP_CACHE_TIME = 5 * 60;

	// asl缓存时间
	public static final int ASL_CACHE_TIME = 60 * 60;

	// key命名规则： 项目名_子项目名_业务（可多级）

	// session
	public static final String APP_SHOW_BASE_SESSION = "APP_SHOW_BASE_SESSION1_";
	// SimpleSession
	public static final String APP_SHOW_SIMPLESESSION = "APP_SHOW_SIMPLESESSION_";
	// app地图页面的场次坐标列表
	public static final String APP_SHOW_MAPLOCATION_LIST = "APP_SHOW_MAPLOCATION_LIST_";
	// poster
	public static final String APP_SHOW_POSTER = "APP_SHOW_POSTER_";
	// stadium
	public static final String APP_SHOW_STADIUM = "APP_SHOW_STADIUM_";

	// video page
	public static final String APP_SS_VIDEO_PAGE = "APP_SS_VIDEO_PAGE_";

	// 今日 liveshow
	public static final String APP_LIVE_FIRSTLIVE = "APP_LIVE_FIRSTLIVE_";
	// liveshow
	public static final String APP_LIVE_LIVESHOW = "APP_LIVE_LIVESHOW_";
	// app地图页面的统计数据分页
	public static final String APP_LIVE_REGIONMAP_PAGE = "APP_LIVE_REGIONMAP_PAGE_";

	// 场次与用户的交互统计数据
	public static final String APP_USER_SESSIONNUMS = "APP_USER_SESSIONNUMS_";

	// 项目的 数据 -- 新加（2016-03-04）
	public static final String APP_USER_PROJECTNUMS = "APP_USER_PROJECTNUMS_";

	// 获取用户点过赞的场次id列表
	public static final String APP_USER_PRAISES = "APP_USER_PRAISES_";

	/**
	 * 机构
	 */
	// 机构信息
	public static final String APP_ORGAN_INFO = "APP_ORGAN_INFO_";

	// 商品备注信息
	public static final String SHOP_ORDER_NOTE = "SHOP_ORDER_NOTE_";
	// 订单库存验证缓存
	public static final String ORDER_VALIDATE_KEY = "ORDER_VALIDATE_KEY_";
	// 订单商品
	public static final String ORDER_PRODUCT_KEY = "ORDER_PRODUCT_KEY_";

	// 商城场次售票状态
	public static final String SHOP_SESSION_TICKET_STATUS = "SHOP_SESSION_TICKET_STATUS_";
	
	
	// 机器人随机数上限
	public static final String CONSOLE_SUPPORT_ROBOT_LIMIT_NUM = "CONSOLE_SUPPORT_ROBOT_LIMIT_NUM_";
	
	// 机器人id串
	public static final String USER_ROBOT_LIST = "USER_ROBOT_LIST_";

}
