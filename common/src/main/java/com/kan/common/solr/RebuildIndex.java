package com.kan.common.solr;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;

/**
 * Created by zxw on 2016/5/19.
 */
public interface RebuildIndex {

    void rebuild(SolrClient solrClient, String collection) throws IOException, SolrServerException;

}
