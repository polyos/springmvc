package com.kan.common.solr;

import org.apache.http.client.HttpClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.common.params.MapSolrParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zxw on 2016/5/19.
 */
public class SolrClientFactory {

    private String zkHost;

    private String maxConnections;
    private String maxConnectionsPerHost;
    private String retry;
    //����
    private String socketTimeout;
    private String connTimeout;

    public SolrClient getCloudSolrClient(){
        Map<String, String> map = new HashMap<>();
        map.put("maxConnections", maxConnections);
        map.put("maxConnectionsPerHost", maxConnectionsPerHost);
        map.put("retry", retry);
        map.put("socketTimeout", socketTimeout);
        map.put("connTimeout", connTimeout);
        HttpClient httpClient = HttpClientUtil.createClient(new MapSolrParams(map));
        return new CloudSolrClient(zkHost, httpClient);
    }

    /*public SolrClient getSolrClient(){
        return new HttpSolrClient();
    }*/

    public String getZkHost() {
        return zkHost;
    }

    public void setZkHost(String zkHost) {
        this.zkHost = zkHost;
    }

    public String getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(String maxConnections) {
        this.maxConnections = maxConnections;
    }

    public String getMaxConnectionsPerHost() {
        return maxConnectionsPerHost;
    }

    public void setMaxConnectionsPerHost(String maxConnectionsPerHost) {
        this.maxConnectionsPerHost = maxConnectionsPerHost;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public String getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(String socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public String getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(String connTimeout) {
        this.connTimeout = connTimeout;
    }
}
