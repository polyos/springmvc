package com.kan.common.solr;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zxw on 2016/5/19.
 */
public class SolrUtils {

    /**
     * 需要建一个collection别名和2个互相切换的collection
     * @param solrClient
     * @param collection
     * @param swap1
     * @param swap2
     * @param rebuildIndex
     * @throws IOException
     * @throws SolrServerException
     */
    public static void rebuild(SolrClient solrClient, String collection, String swap1, String swap2, RebuildIndex rebuildIndex) throws IOException, SolrServerException {

        CollectionAdminRequest.ClusterStatus  clusterStatus = new CollectionAdminRequest.ClusterStatus();
        clusterStatus.setCollectionName(swap1);
        NamedList<Object> list = solrClient.request(clusterStatus, collection);
        Object obj = list.get("cluster");
        SimpleOrderedMap<Object> map = (SimpleOrderedMap<Object>)obj;
        Map<String, String> alias = (Map<String, String>)map.get("aliases");
        String target = null;
        if(alias != null) {
            target = alias.get(collection);
        }
        if(target == null){
            target = swap2;
        }
        if(swap1.equals(target)){
            //swap2 rebuild
            rebuildIndex.rebuild(solrClient, swap2);
            CollectionAdminRequest.CreateAlias createAlias = new CollectionAdminRequest.CreateAlias();
            createAlias.setAliasName(collection);
            createAlias.setAliasedCollections(swap2);
            solrClient.request(createAlias);
        }else{
            //swap1 rebuild
            rebuildIndex.rebuild(solrClient, swap1);
            CollectionAdminRequest.CreateAlias createAlias = new CollectionAdminRequest.CreateAlias();
            createAlias.setAliasName(collection);
            createAlias.setAliasedCollections(swap1);
            solrClient.request(createAlias);
        }

    }

    public static void main(String [] args){
        SolrClientFactory solrClientFactory = new SolrClientFactory();
        solrClientFactory.setZkHost("192.168.199.17:2181");
        solrClientFactory.setMaxConnections("1000");
        solrClientFactory.setMaxConnectionsPerHost("1000");
        SolrClient solrClient = solrClientFactory.getCloudSolrClient();

        try {
            SolrUtils.rebuild(solrClient, "test", "test1", "test2", new RebuildIndex() {
                @Override
                public void rebuild(final SolrClient solrClient, final String collection) throws IOException, SolrServerException {

                    solrClient.deleteByQuery(collection, "*:*");
                    final List<SolrInputDocument> list = new ArrayList<>();

                    for(int i=1; i<=5000; i++) {
                        SolrInputDocument document = new SolrInputDocument();
                        document.addField("id", i + "");
                        document.addField("title", "adfasdfas" + i);
                        document.addField("type", "2223" + i);
                        document.addField("place", "33333333" + i);
                        document.addField("content", "453214132413" + i);
                        document.addField("url", "412432133" + i);
                        list.add(document);

                    }

                    try {
                        UpdateResponse response = solrClient.add(collection, list);
                    } catch (SolrServerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    solrClient.commit(collection);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
        try {
            solrClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
