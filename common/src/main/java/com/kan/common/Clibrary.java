package com.kan.common;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * Created by zxw on 2015/12/11.
 */
public interface Clibrary extends Library {
    String getString();

    String getDefault();
}
