package com.kan.common.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kan.common.rest.exception.RestException;
import org.apache.commons.lang3.math.NumberUtils;
import org.jaudiotagger.tag.vorbiscomment.util.Base64Coder;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by ZXW on 2014/11/12.
 */
public class StringUtils {

	public static String toCamelCase(String input) {
		StringBuilder sb = new StringBuilder();
		final char delim = '_';
		char value;
		boolean capitalize = false;
		for (int i = 0; i < input.length(); ++i) {
			value = input.charAt(i);
			if (value == delim) {
				capitalize = true;
			} else if (capitalize) {
				sb.append(Character.toUpperCase(value));
				capitalize = false;
			} else {
				sb.append(value);
			}
		}
		return sb.toString();
	}

	public static String toJson(Object obj) {
		ObjectMapper om = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			om.writeValue(sw, obj);
			String str = sw.toString();
			sw.close();
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T toObject(String data, Class<T> clazz) {
		ObjectMapper om = new ObjectMapper();
		om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			return om.readValue(data, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T toObject(String data, TypeReference<T> tr) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(data, tr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String cutStr(String str, int limit) {
		if (str != null && str.length() > limit) {
			return str.substring(0, limit) + "…";
		} else {
			return str;
		}
	}

	/**
	 * description: 字符串转list，空串返回null
	 *
	 * @param str
	 * @param split
	 * @return
	 *
	 * @author don
	 * @date 2016年6月24日 下午2:02:35
	 */
	public static List<Integer> StringToList(String str, String split) {
		if (str == null || "".equals(str)) {
			return null;
		} else {
			List<Integer> ids = new LinkedList<Integer>();
			if (str.contains(split)) {
				String[] strs = str.split(split);
				for (String st : strs) {
					if (NumberUtils.isNumber(st)) {
						ids.add(Integer.parseInt(st));
					}
				}
			} else {
				if (NumberUtils.isNumber(str)) {
					ids.add(Integer.parseInt(str));
				}
			}
			if (ids.size() > 0) {
				return ids;
			} else {
				return null;
			}

		}
	}

	/**
	 * description SQL防注入验证
	 * 
	 * @param title
	 * @return boolean 非注入SQL返回true
	 * @author don
	 * @time 2015年3月18日 下午1:44:40
	 */
	public static boolean isRightSQL(String title) {
		if (title != null) {
			title = title.toLowerCase();// 统一转为小写
			String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|*|%|chr|mid|master|truncate|"
					+ "char|declare|sitename|net user|xp_cmdshell|;|or|-|+|,|like'|and|exec|execute|insert|create|drop|"
					+ "table|from|grant|use|group_concat|column_name|"
					+ "information_schema.columns|table_schema|union|where|select|delete|update|order|by|count|*|"
					+ "chr|mid|master|truncate|char|declare|or|;|-|--|+|,|like|//|/|%|#";// 过滤掉的sql关键字，可以手动添加
			String[] badStrs = badStr.split("\\|");
			for (int i = 0; i < badStrs.length; i++) {
				if (title.indexOf(badStrs[i]) >= 0) {
					return false;
				}
			}

		}
		return true;
	}

	/**
	 * 将一个 JavaBean 对象转化为一个 Map
	 * 
	 * @param bean
	 *            要转化的JavaBean 对象
	 * @return 转化出来的 Map 对象
	 * @throws IntrospectionException
	 *             如果分析类属性失败
	 * @throws IllegalAccessException
	 *             如果实例化 JavaBean 失败
	 * @throws InvocationTargetException
	 *             如果调用属性的 setter 方法失败
	 */
	public static Map<String, Object> BeanToMap(Object bean) throws IntrospectionException,
			IllegalAccessException,
			InvocationTargetException {
		Class<? extends Object> type = bean.getClass();
		Map<String, Object> returnMap = new HashMap<String, Object>();
		BeanInfo beanInfo = Introspector.getBeanInfo(type);

		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (int i = 0; i < propertyDescriptors.length; i++) {
			PropertyDescriptor descriptor = propertyDescriptors[i];
			String propertyName = descriptor.getName();
			if (!propertyName.equals("class")) {
				Method readMethod = descriptor.getReadMethod();
				Object result = readMethod.invoke(bean, new Object[0]);
				if (result != null) {
					returnMap.put(propertyName, result);
				}
			}
		}
		return returnMap;
	}

	/**
	 * description: string --> url编码
	 * 
	 * @param str
	 *            第一位为需要转码字符，第二位为转码后编码
	 *
	 * @author don
	 * @date 2015年12月22日 下午7:41:07
	 */
	public static String URLEncode(String... str) {
		try {
			if (str == null) {
				return "";
			} else if (str.length == 1) {
				return URLEncoder.encode(str[0], "UTF-8");
			} else {
				return URLEncoder.encode(str[0], str[1]);
			}
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * description: url编码 --> string
	 * 
	 * @param str
	 *            第一位为需要解码字符，第二位为解码后编码
	 * @return
	 *
	 * @author don
	 * @date 2016年2月24日 下午8:59:04
	 */
	public static String URLDecode(String... str) {
		try {
			if (str == null) {
				return "";
			} else if (str.length == 1) {
				return URLDecoder.decode(str[0], "UTF-8");
			} else {
				return URLDecoder.decode(str[0], str[1]);
			}
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 密码验证 (正确返回true[boolean],错误抛出RestException)
	 * 
	 * @param password
	 *
	 * @param params
	 * @return
	 * @throws RestException
	 *
	 * @author don
	 * @date 2016年4月8日 下午2:04:49
	 */
	public static boolean validePassword(String password, int... params) throws RestException {
		if (password == null || password.trim().length() == 0) {
			throw RestException.Error(432, "密码不能为空");
		}
		int length = params.length == 0 ? 8 : params[0];
		if (password.trim().length() < length) {
			throw RestException.Error(432, "密码长度不能少于" + length + "位");
		}
		if (password.matches("^\\d*$")) {
			throw RestException.Error(432, "密码不能为纯数字");
		}
		if (password.matches("^[a-zA-Z]*$")) {
			throw RestException.Error(432, "密码不能为纯字母");
		}
		return true;
	}

	private static int compare(String str, String target) {
		int d[][]; // 矩阵
		int n = str.length();
		int m = target.length();
		int i; // 遍历str的
		int j; // 遍历target的
		char ch1; // str的
		char ch2; // target的
		int temp; // 记录相同字符,在某个矩阵位置值的增量,不是0就是1
		if (n == 0) {
			return m;
		}
		if (m == 0) {
			return n;
		}
		d = new int[n + 1][m + 1];
		for (i = 0; i <= n; i++) { // 初始化第一列
			d[i][0] = i;
		}

		for (j = 0; j <= m; j++) { // 初始化第一行
			d[0][j] = j;
		}

		for (i = 1; i <= n; i++) { // 遍历str
			ch1 = str.charAt(i - 1);
			// 去匹配target
			for (j = 1; j <= m; j++) {
				ch2 = target.charAt(j - 1);
				if (ch1 == ch2) {
					temp = 0;
				} else {
					temp = 1;
				}

				// 左边+1,上边+1, 左上角+temp取最小
				d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + temp);
			}
		}
		return d[n][m];
	}

	private static int min(int one, int two, int three) {
		return (one = one < two ? one : two) < three ? one : three;
	}

	/**
	 * 获取两字符串的相似度
	 *
	 * @param str
	 * @param target
	 * @return
	 */
	public static float getSimilarityRatio(String str, String target) {
		return 1 - (float) compare(str, target) / Math.max(str.length(), target.length());
	}

	/**
	 * description: http接口的参数加密
	 *
	 * @param original
	 *            需要加密的原文
	 * @param params
	 *            [0]:深度，最小值为2，缺省1
	 * @return
	 *
	 * @author don
	 * @date 2016年4月18日 下午7:28:21
	 */
	public static String encrypt(String original, int... params) {
		if (org.apache.commons.lang3.StringUtils.isBlank(original)) {
			return "";
		}
		int depth;
		if (params.length == 0 || params[0] <= 0) {
			depth = 1;
		} else {
			depth = params[0];
		}
		String result = original;
		for (int i = 0; i < depth; i++) {
			result = Base64Coder.encode(result);
		}
		result = URLEncode(result);
		return Base64Coder.encode(result);
	}

	/**
	 * description: http接口的参数密文解密
	 *
	 * @param cipher
	 *            需要解密的密文
	 * @param params
	 *            [0]:深度，最小值为2，缺省1
	 * @return
	 *
	 * @author don
	 * @date 2016年4月18日 下午7:38:55
	 */
	public static String decryption(String cipher, int... params) {
		if (org.apache.commons.lang3.StringUtils.isBlank(cipher)) {
			return "";
		}
		int depth;
		if (params.length == 0 || params[0] <= 0) {
			depth = 1;
		} else {
			depth = params[0];
		}
		String result = URLDecode(new String(Base64Coder.decode(cipher)));
		for (int i = 0; i < depth; i++) {
			result = new String(Base64Coder.decode(result));
		}
		return result;
	}

}
