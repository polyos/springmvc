package com.kan.common.utils;

import com.kan.common.Paginator;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zxw on 2015/7/30.
 */
public class BeanUtils {

    /**
     * 将一个 JavaBean 对象转化为一个 Map
     *
     * @param bean
     *            要转化的JavaBean 对象
     * @return 转化出来的 Map 对象
     * @throws IntrospectionException
     *             如果分析类属性失败
     * @throws IllegalAccessException
     *             如果实例化 JavaBean 失败
     * @throws InvocationTargetException
     *             如果调用属性的 setter 方法失败
     */
    public static Map<String, Object> BeanToMap(Object bean)
            throws IntrospectionException, IllegalAccessException,
            InvocationTargetException {
        Class<? extends Object> type = bean.getClass();
        Map<String, Object> returnMap = new HashMap<String, Object>();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);

        PropertyDescriptor[] propertyDescriptors = beanInfo
                .getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean, new Object[0]);
                if (result != null) {
                    returnMap.put(propertyName, result);
                }
            }
        }
        return returnMap;
    }

    public static Object mapToBean(Map<String, Object> map, Class<?> clazz) throws IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException, IOException {
        Object obj = clazz.newInstance();
        BeanInfo beanInfo = Introspector.getBeanInfo(clazz);

        PropertyDescriptor[] propertyDescriptors = beanInfo
                .getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (map.containsKey(propertyName)) {
                Class type = descriptor.getPropertyType();
                Object value = map.get(propertyName);
                if(value == null){
                    continue;
                }
                Object newValue = null;
                if(type.isArray()) {
                    if(List.class.isAssignableFrom(value.getClass())){
                        List org = (List)value;
                        Object newArr = Array.newInstance(type.getComponentType(), org.size());
                        for(int x=0; x<org.size(); x++){
                            Object o = org.get(x);
                            if(Map.class.isAssignableFrom(o.getClass())){
                                ParameterizedType pt = (ParameterizedType)descriptor.getWriteMethod().getGenericParameterTypes()[0];
                                Type t = pt.getActualTypeArguments()[0];
                                Object val = mapToBean((Map) o, (Class) t);
                                Array.set(newArr, x, val);
                            }else{
                                Array.set(newArr, x, o);
                            }
                        }
                        newValue = newArr;
                    }else {
                        int length = Array.getLength(value);
                        Object newArr = Array.newInstance(type.getComponentType(), length);
                        for (int x = 0; x < length; x++) {
                            Object o = Array.get(value, x);
                            if (Map.class.isAssignableFrom(o.getClass())) {
                                ParameterizedType pt = (ParameterizedType) descriptor.getWriteMethod().getGenericParameterTypes()[0];
                                Type t = pt.getActualTypeArguments()[0];
                                Object val = mapToBean((Map) o, (Class) t);
                                Array.set(newArr, x, val);
                            } else {
                                Array.set(newArr, x, o);
                            }
                        }
                        newValue = newArr;
                    }
                }else if(List.class.isAssignableFrom(type)){
                    List list = new ArrayList();
                    List org = (List)value;
                    for(Object o : org){
                        if(Map.class.isAssignableFrom(o.getClass())){
                            ParameterizedType pt = (ParameterizedType)descriptor.getWriteMethod().getGenericParameterTypes()[0];
                            Type t = pt.getActualTypeArguments()[0];
                            Object val = mapToBean((Map) o, (Class) t);
                            list.add(val);
                        }else{
                            list.add(o);
                        }
                    }
                    newValue = list;
                }else if (type == Boolean.TYPE || type == Boolean.class) {        // boolean
                    newValue = value;
                } else if (type == Character.TYPE || type == Character.class) { // char
                    newValue = value;
                } else if (type == Byte.TYPE || type == Byte.class) {    // byte
                    newValue = value;
                } else if (type == Short.TYPE || type == Short.class) {   // short
                    newValue = value;
                } else if (type == Integer.TYPE || type == Integer.class) { // int
                    newValue = value;
                } else if (type == Long.TYPE || type == Long.class) {    // long
                    newValue = value;
                } else if (type == Float.TYPE || type == Float.class) {   // float
                    newValue = value;
                } else if (type == Double.TYPE || type == Double.class) {  // double
                    newValue = value;
                }else if(type == String.class){
                    newValue = value;
                }else if(Map.class.isAssignableFrom(type)){
                    newValue = value;
                } else {
                    if(Map.class.isAssignableFrom(value.getClass())){
                        Object val = mapToBean((Map)value, type);
                        newValue = val;
                    }else{
                        throw new IOException("Can't write: "+obj+" as "+type);
                    }
                }
                if(descriptor.getWriteMethod() != null) {
                    org.apache.commons.beanutils.BeanUtils.copyProperty(obj, descriptor.getName(), newValue);
                    //descriptor.getWriteMethod().invoke(obj, newValue);
                }
            }
        }
        return  obj;
    }

    public static void main(String [] args) throws NoSuchMethodException, InvocationTargetException, IOException, IntrospectionException, InstantiationException, IllegalAccessException {
        //Ttt tt = new Ttt();
        //System.out.println(list.getClass().getMethod("get", int.class));
        //Object s = new String[2];
        //System.out.println(Array.getLength(s));
        Ttt tt = new Ttt();
        tt.setStr("123213123");
        List<String> l = new ArrayList<>();
        l.add("111");
        l.add("222");
        l.add("333");
        List<Paginator> p = new ArrayList<>();
        p.add(new Paginator(1));
        p.add(new Paginator(2));
        tt.setpList(p);
        tt.setStrList(l);
        String s = StringUtils.toJson(tt);
        Map t1 = StringUtils.toObject(s, Map.class);
        Ttt t2 = (Ttt)mapToBean(t1, Ttt.class);
        System.out.println(t2);
    }

    public static class Ttt{
        private String str;
        private List<String> strList;
        private List<Paginator> pList;

        public String getStr() {
            return str;
        }

        public void setStr(String str) {
            this.str = str;
        }

        public List<String> getStrList() {
            return strList;
        }

        public void setStrList(List<String> strList) {
            this.strList = strList;
        }

        public List<Paginator> getpList() {
            return pList;
        }

        public void setpList(List<Paginator> pList) {
            this.pList = pList;
        }

        @Override
        public String toString() {
            return "Ttt{" +
                    "str='" + str + '\'' +
                    ", strList=" + strList +
                    ", pList=" + pList +
                    '}';
        }
    }

}
