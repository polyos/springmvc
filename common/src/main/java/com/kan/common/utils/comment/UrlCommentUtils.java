package com.kan.common.utils.comment;

import com.kan.common.SpringContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zxw on 2016/5/5.
 */
public class UrlCommentUtils {

    public static void toFile(){
        String str = getComments();
        try {
            FileUtils.write(new File("d:/urlComments.txt"), str, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getComments(){
        final StringBuilder sb = new StringBuilder();
        ApplicationContext applicationContext = SpringContext.getApplicationContext();
        Map<String, Object> map = applicationContext
                .getBeansWithAnnotation(Controller.class);
        for (Object obj : map.values()) {
            RequestMapping mapping = AnnotationUtils.findAnnotation(
                    obj.getClass(), RequestMapping.class);
            final String[] m;
            if (mapping != null) {
                m = mapping.value();
            } else {
                m = new String[] { "" };
            }
            ReflectionUtils.doWithMethods(obj.getClass(),
                    new ReflectionUtils.MethodCallback() {
                        public void doWith(Method method) {
                            RequestMapping mapping = AnnotationUtils
                                    .findAnnotation(method, RequestMapping.class);
                            Description desc = AnnotationUtils.findAnnotation(method, Description.class);

                            if (mapping != null) {
                                for (String mm : m) {
                                    List<String> urls = addUrls(mapping, mm);
                                    for(String url : urls){
                                        if(desc != null) {
                                            sb.append(url + "\t" + StringUtils.join(mapping.method()) + "\t" + desc.value() + "\t" + desc.type() + "\t" + desc.type2() + "\t" + desc.type3()).append("\n");
                                        }else{
                                            sb.append(url + "\t" + StringUtils.join(mapping.method()) + "\t\t\t\t").append("\n");
                                        }
                                    }
                                }
                            }
                        }
                    }, ReflectionUtils.USER_DECLARED_METHODS);
        }
        return sb.toString();
    }

    private static List<String> addUrls(RequestMapping mapping, String pre) {
        List<String> urls = new ArrayList<>();
        String[] mappedPatterns = mapping.value();
        if (mappedPatterns != null && mappedPatterns.length > 0) {
            for (String mappedPattern : mappedPatterns) {
                if (!mappedPattern.startsWith("/")) {
                    mappedPattern = "/" + mappedPattern;
                }
                if (pre != null) {
                    mappedPattern = pre + mappedPattern;
                }
                urls.add(mappedPattern);
            }
        }
        return urls;
    }

}
