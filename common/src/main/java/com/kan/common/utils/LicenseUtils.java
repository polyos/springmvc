package com.kan.common.utils;

import com.kan.common.Clibrary;
import com.kan.common.SpringContext;
import com.kan.common.rest.RestClient;
import com.sun.jna.Native;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.*;
import java.util.Date;

/**
 * Created by zxw on 2015/12/10.
 */
public class LicenseUtils {

    public synchronized static void init(final DispatcherServlet dispatcherServlet){
        String os = System.getProperty("os.name");
        if(os.toLowerCase().contains("win")){
            return;
        }
        String v = null;
        String defaultDate = null;
        try {
            Clibrary INSTANTCE = (Clibrary) Native.loadLibrary("test1", Clibrary.class);
            v = INSTANTCE.getString();
            defaultDate = INSTANTCE.getDefault();
        }catch (Throwable t){
            t.printStackTrace();
            SpringContext.getBean(RestClient.class).close();
            dispatcherServlet.destroy();
            return;
        }
        final String a = v;
        final String d = defaultDate;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                while(true) {
                    String file = LicenseUtils.class.getResource("/").getPath() + "LICENSE";
                    File f = new File(file);
                    FileReader fr = null;
                    try {
                        String date = d;
                        if(f.exists()){
                            fr = new FileReader(f);
                            BufferedReader br = new BufferedReader(fr);
                            String line = br.readLine();
                            String str = EncryptUtils.Decrypt3DES(line, a);
                            String [] args = str.split("&");
                            String s = args[0];
                            date = args[1];
                            String md5 = DigestUtils.md5Hex(a + "&"+args[1]);
                            if(!md5.equals(s)){
                                dispatcherServlet.destroy();
                            }
                        }
                        Date dd = DateUtils.parseDate(date, "yyyyMMdd");
                        if(dd.getTime() < System.currentTimeMillis()){
                            dispatcherServlet.destroy();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        dispatcherServlet.destroy();
                    }finally {
                        if(fr != null){
                            try {
                                fr.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    try {
                        Thread.sleep(12 * 3600 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    public synchronized static void update(String value){
        String file = LicenseUtils.class.getResource("/").getPath() + "LICENSE";
        PrintWriter fileWriter = null;
        try {
            fileWriter = new PrintWriter(file);
            fileWriter.println(value);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                fileWriter.close();
            }
        }
    }

}
