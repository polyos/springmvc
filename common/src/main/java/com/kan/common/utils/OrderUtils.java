package com.kan.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * description: 订单工具类
 * 
 * @author don
 * @date 2016年4月14日 下午7:55:21
 *
 */
public class OrderUtils {

	private final static String SIGN_PRODUCT_SEPARATER = "_";
	private final static String SIGN_SKU_SEPARATER = ">";

	/**
	 * description: 获取订单创建签名
	 *
	 * @param userId
	 * @param skuJson
	 *            sku的json数据
	 *            eg:{productId:1,skuId:3,num:1},{productId:1,skuId:4,num:2}
	 * @return
	 *
	 * @author don
	 * @date 2016年4月14日 下午7:57:31
	 */
	public static String getOrderSign(int userId, String skuJson) {
		long start = System.currentTimeMillis();
		StringBuffer sign = new StringBuffer();
		sign.append(userId);

		System.out.println(System.currentTimeMillis() - start);
		List<HashMap<String, Integer>> json = StringUtils.toObject(skuJson, new TypeReference<List<HashMap<String, Integer>>>() {
		});
		System.out.println(System.currentTimeMillis() - start);
		if (json == null) {
			return "";
		}
		System.out.println(System.currentTimeMillis() - start);

		// 整理 （结果：{3=[12, 5], 5=[6]}）
		TreeMap<Integer, List<Integer>> productMap = new TreeMap<>();
		for (HashMap<String, Integer> treeMap : json) {
			Integer productId = treeMap.get("productId");
			List<Integer> skuList = productMap.get(productId);
			if (skuList == null) {
				skuList = new ArrayList<>();
				skuList.add(treeMap.get("skuId"));
			} else {
				skuList.add(treeMap.get("skuId"));
			}
			productMap.put(productId, skuList);
		}

		System.out.println(System.currentTimeMillis() - start);
		// 内层SKU排序（结果：{3=[5, 12], 5=[6]}）
		for (Integer productId : productMap.keySet()) {
			List<Integer> skuList = productMap.get(productId);
			Collections.sort(productMap.get(productId));
			sign.append(SIGN_PRODUCT_SEPARATER + productId);
			for (Integer skuId : skuList) {
				sign.append(SIGN_SKU_SEPARATER + skuId);
			}
		}
		System.out.println(System.currentTimeMillis() - start);
		// eg :1_3>5>12_5>6
		return sign.toString();
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		String sign = getOrderSign(86,
				"[{\"skuId\":5,\"num\":1,\"productId\":3},{\"skuId\":6,\"num\":1,\"productId\":3},{\"skuId\":12,\"num\":1,\"productId\":3}]");
		System.out.println(System.currentTimeMillis() - start);
		System.out.println(sign);
	}
}
