package com.kan.common.utils;

import java.io.File;
import java.io.IOException;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.TagException;

/**
 * description: 音频处理工具
 * @author don
 * @date 2015年12月18日 下午2:31:02
 *
 */
public class AudioProcessUtils {

	/**
	 * description:  获取mp3文件时长
	 * @param mp3File
	 * @return
	 *
	 * @author don
	 * @throws InvalidAudioFrameException
	 * @throws ReadOnlyFileException
	 * @throws TagException
	 * @throws IOException
	 * @throws CannotReadException
	 * @date 2015年12月18日 下午2:33:25
	 */
	public static int getMp3Second(File mp3File) throws CannotReadException,
			IOException,
			TagException,
			ReadOnlyFileException,
			InvalidAudioFrameException {
		MP3File f = (MP3File) AudioFileIO.read(mp3File);
		MP3AudioHeader audioHeader = (MP3AudioHeader) f.getAudioHeader();
		return audioHeader.getTrackLength();
	}
}
