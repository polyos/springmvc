package com.kan.common.utils.image;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {

	// 图片上传最大大小 byte
	public static final int MAX_IMAGE_SIZE = 200 * 1024;
	// 项目海报上传最大大小 byte
	public static final int MAX_IMAGE_SIZE_PROJECT = 500 * 1024;
	//项目推荐海报
	public static final int RECOMMEND_HEIGHT = 175*2;
	public static final int RECOMMEND_WIDTH= 370*2;
	// 图片压缩 宽高
	public static final int COMPRESSION_WIDTH = 960;
	public static final int COMPRESSION_HEIGHT = 640;

	public static final int COMPRESSION_WIDTH_PROJECT = 1280;
	public static final int COMPRESSION_HEIGHT_PROJECT = 720;

	// 缩略图默认裁剪尺寸
	public static final int DEAFAULT_WIDTH = 420;
	public static final int DEAFAULT_HEIGHT = 420;
	// 缩略图后缀
	public static final String SUFFIX = "_TN";

	public static void main(String[] args) {
		// toFile(cut(getByte("d:/abc.jpg"),100, 100, 400 ,400),
		// "d:/cut_abc.jpg");
//		List<FontMark> list = new ArrayList<>();
//		FontMark mark = new FontMark();
//		mark.setFontName("宋体");
//		mark.setColor(Color.BLACK);
//		mark.setFontSize(20);
//		mark.setFontStyle(Font.PLAIN);
//		mark.setText("我是水印1");
//		mark.setX(0);
//		mark.setY(0);
//		list.add(mark);
//
//		FontMark mark1 = new FontMark();
//		mark1.setFontName("宋体");
//		mark1.setColor(Color.WHITE);
//		mark1.setFontSize(20);
//		mark1.setFontStyle(Font.PLAIN);
//		mark1.setText("12345678901234567890");
//		mark1.setX(100);
//		mark1.setY(100);
//		list.add(mark1);
//
//		// toFile(resize(getByte("C:\\Users\\mayun\\Desktop\\upload\\1 (6).jpg"),
//		// 400, 400), "C:\\Users\\mayun\\Desktop\\upload\\new2.jpg");
//
//		getWHSize(getByte("C:\\Users\\mayun\\Desktop\\upload\\1 (6).jpg"));

		toFile(addBlur(getByte("d:/1.jpg"), 40, 800, 640), "d:/11.jpg");

	}

	/**
	 * description:  获取图片的长宽
	 * @param bytes
	 *            [width,height]
	 * @return
	 *
	 * @author don
	 * @date 2015年9月11日 下午1:46:55
	 */
	public static int[] getWHSize(byte[] bytes) {
		int[] result = new int[2];

		BufferedImage sourceImg;
		try {
			sourceImg = ImageIO.read(new ByteArrayInputStream(bytes));
			result[0] = sourceImg.getWidth();
			result[1] = sourceImg.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static byte[] mark(byte[] bytes, List<FontMark> fontMarkList) {
		ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		Thumbnails.Builder builder = Thumbnails.of(bai).scale(1).outputQuality(0.9f);
		for (final FontMark mark : fontMarkList) {
			builder.watermark(new Position() {
				@Override
				public Point calculate(int enclosingWidth,
						int enclosingHeight,
						int width,
						int height,
						int insetLeft,
						int insetRight,
						int insetTop,
						int insetBottom) {
					return new Point(mark.getX(), mark.getY());
				}
			}, ImageUtils.fontImage(mark.getText(), mark.getFontName(), mark.getFontStyle(), mark.getFontSize(), mark.getColor()), 1f);
		}
		try {
			builder.toOutputStream(bao);
			return bao.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bai.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bao.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static byte[] cut(byte[] bytes, int x1, int y1, int x2, int y2, int outputWidth, int outputHeight) {
		int width = x2 - x1;
		int height = y2 - y1;
		ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		try {
			Thumbnails.of(bai).sourceRegion(x1, y1, width, height).size(outputWidth, outputHeight).outputQuality(0.9f).toOutputStream(bao);
			return bao.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bai.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bao.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static byte[] cut(byte[] bytes, int x1, int y1, int x2, int y2) {
		int width = x2 - x1;
		int height = y2 - y1;
		ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		try {
			Thumbnails.of(bai).sourceRegion(x1, y1, width, height).scale(1).outputQuality(0.9f).toOutputStream(bao);
			return bao.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bai.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bao.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static byte[] resize(byte[] bytes, int width, int height) {
		ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		try {
			Thumbnails.of(bai).size(width, height).outputQuality(0.9f).toOutputStream(bao);
			return bao.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bai.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bao.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static int getSize(byte[] bytes) {
		ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
		try {
			BufferedImage image = ImageIO.read(bai);
			image.getWidth();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			bai.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static BufferedImage fontImage(String pressText, String fontName, int fontStyle, int fontSize, Color color) {
		Font font = new Font(fontName, fontStyle, fontSize);
		FontMetrics fm = sun.font.FontDesignMetrics.getMetrics(font);
		BufferedImage image = new BufferedImage(fm.stringWidth(pressText), fm.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();
		g.setColor(color);
		int ascent = fm.getAscent();
		g.setFont(font);
		g.drawString(pressText, 0, ascent);
		g.dispose();
		return image;
	}

	private static byte[] getByte(String file) {
		FileInputStream fileInputStream = null;
		ByteArrayOutputStream bao = null;
		try {
			fileInputStream = new FileInputStream(file);
			bao = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int length = 0;
			while ((length = fileInputStream.read(b)) != -1) {
				bao.write(b, 0, length);
			}
			return bao.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (fileInputStream != null)
					fileInputStream.close();
				if (bao != null)
					bao.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void toFile(byte[] bytes, String file) {
		FileOutputStream fileOutputStream = null;
		ByteArrayInputStream bai = null;
		try {
			fileOutputStream = new FileOutputStream(file);
			bai = new ByteArrayInputStream(bytes);
			byte[] b = new byte[1024];
			int length = 0;
			while ((length = bai.read(b)) != -1) {
				fileOutputStream.write(b, 0, length);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bai != null)
					bai.close();
				if (fileOutputStream != null)
					fileOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static byte[] addBlur(byte[] bytes, int radius, int targetWidth, int targetHeight) {
		int[] res = getWHSize(bytes);
		int w = res[0];
		int h = res[1];

		byte[] cut;

		if(h/w < targetHeight/targetWidth){
			//width太宽
			int tempW = (int)((double)targetWidth/targetHeight*h);
			int offsetW = (w-tempW)/2;
			cut = cut(bytes, offsetW, 0, offsetW + tempW, h, targetWidth, targetHeight);
		}else{
			h = (int)((double)targetHeight/targetWidth*w);
			cut = cut(bytes, 0, 0, w, h, targetWidth, targetHeight);
		}

		ByteArrayInputStream bais = null;
		ByteArrayOutputStream baos = null;
		try {
			if(cut != null) {
				baos = new ByteArrayOutputStream();
				bais = new ByteArrayInputStream(cut);
				BufferedImage img = ImageIO.read(bais);
				doBlur(img, radius);
				ImageIO.write(img, "jpeg", baos);
				return baos.toByteArray();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(bais != null) {
				try {
					bais.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(baos != null){
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	private static BufferedImage doBlur(BufferedImage bitmap, int radius) throws IOException, InterruptedException {
		if (radius < 1) {
			return (null);
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int[] pix = new int[w * h];
		bitmap.getRGB(0, 0, w, h, pix, 0, w);

		int wm = w - 1;
		int hm = h - 1;
		int wh = w * h;
		int div = radius + radius + 1;

		int r[] = new int[wh];
		int g[] = new int[wh];
		int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		int vmin[] = new int[Math.max(w, h)];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < h; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = pix[yi + Math.min(wm, Math.max(i, 0))];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);
				rbs = r1 - Math.abs(i);
				rsum += sir[0] * rbs;
				gsum += sir[1] * rbs;
				bsum += sir[2] * rbs;
				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}
			}
			stackpointer = radius;

			for (x = 0; x < w; x++) {

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min(x + radius + 1, wm);
				}
				p = pix[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[(stackpointer) % div];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi++;
			}
			yw += w;
		}
		for (x = 0; x < w; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * w;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max(0, yp) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs(i);

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += w;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < h; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (x == 0) {
					vmin[y] = Math.min(y + r1, hm) * w;
				}
				p = x + vmin[y];

				sir[0] = r[p];
				sir[1] = g[p];
				sir[2] = b[p];

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[stackpointer];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi += w;
			}
		}

		bitmap.setRGB(0, 0, w, h, pix, 0, w);
		return (bitmap);
	}
}