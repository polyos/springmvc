package com.kan.common.utils.comment;

import java.lang.annotation.*;

/**
 * Created by zxw on 2016/5/5.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Description {

    /**
     * 接口描述
     * @return
     */
    String value() default "";

    /**
     * 接口类型
     * @return
     */
    String type() default "";

    /**
     * 接口类型2
     * @return
     */
    String type2() default "";

    /**
     * 接口类型3
     * @return
     */
    String type3() default "";
}
