package com.kan.common.spring.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;

/**
 * Created by zxw on 2016/5/6.
 */
public class MutliCache implements Cache {

    private final int expire;
    private final String name;
    private com.puff.cache.Cache multiLevelCache;

    public MutliCache(int expire, String name, com.puff.cache.Cache multiLevelCache) {
        this.expire = expire;
        this.name = name;
        this.multiLevelCache = multiLevelCache;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getNativeCache() {
        return multiLevelCache;
    }

    @Override
    public ValueWrapper get(Object key) {
        ValueWrapper wrapper = null;
        Object value = multiLevelCache.get(key.toString());
        if (value != null) {
            wrapper = new SimpleValueWrapper(value);
        }
        return wrapper;
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        Object value = multiLevelCache.get(key.toString());
        return (T)value;
    }

    @Override
    public void put(Object key, Object value) {
        if(expire <= 0) {
            multiLevelCache.put(key.toString(), value);
        }else{
            multiLevelCache.put(key.toString(), value, expire);
        }
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        Object existingValue = multiLevelCache.get(key.toString());
        if (existingValue == null) {
            multiLevelCache.put(key.toString(), value);
            return null;
        } else {
            return new SimpleValueWrapper(existingValue);
        }
    }

    @Override
    public void evict(Object key) {
        multiLevelCache.remove(key.toString());
    }

    @Override
    public void clear() {
        multiLevelCache.clear();
    }
}
