package com.kan.common.spring.cache;

import com.puff.cache.CacheClient;
import org.springframework.cache.Cache;
import org.springframework.cache.transaction.AbstractTransactionSupportingCacheManager;

import java.util.*;

/**
 * Created by zxw on 2016/5/6.
 */
public class MutliCacheManager extends AbstractTransactionSupportingCacheManager {

    private Map<String, Integer> expireMap = new HashMap<>();
    private CacheClient cacheClient;

    @Override
    protected Collection<? extends Cache> loadCaches() {
        if(cacheClient == null){
            throw new IllegalArgumentException("cacheClient is null");
        }

        List<Cache> cacheList = new ArrayList<>();
        for(String key : expireMap.keySet()){
            Cache cache = new MutliCache(expireMap.get(key), key, cacheClient.getCacheManager().buildCache(key));
            cacheList.add(cache);
        }
        return cacheList;
    }

    public void setExpireMap(Map<String, Integer> configMap) {
        this.expireMap = configMap;
    }

    public void setCacheClient(CacheClient cacheClient) {
        this.cacheClient = cacheClient;
    }
}
