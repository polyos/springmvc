package com.kan.common.spring;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by zxw on 2016/4/27.
 */
public class JsonMessageConverter extends MappingJackson2HttpMessageConverter {

    Map<Class, Boolean> map = new HashMap<>();

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        if(clazz.equals(String.class)){
            return false;
        }
        lock.readLock().lock();
        try {
            Boolean b = map.get(clazz);
            if (b != null) {
                return b;
            }
        }finally {
            lock.readLock().unlock();
        }
        lock.writeLock().lock();
        try {
            boolean bool = super.canWrite(clazz, mediaType);
            map.put(clazz, bool);
            return bool;
        }finally {
            lock.writeLock().unlock();
        }
    }
}
