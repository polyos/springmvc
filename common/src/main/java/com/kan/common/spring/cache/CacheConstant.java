package com.kan.common.spring.cache;

/**
 * description: 缓存key
 * <p/>
 * key命名规则： 项目名_子项目名_业务（可多级）
 *
 * @author don
 * @date 2016年1月6日 上午11:10:44
 */
public class CacheConstant {


    /***************************************
     * 缓存域 start
     ************************************************************************************/
    // 周边
    public static final String CACHE_NAME_SUPPORT = "showSupportCache";
    // 机构
    public static final String CACHE_NAME_ORGAN = "organCache";
    // 商城
    public static final String CACHE_NAME_SHOP = "shopCache";
    // 直播
    public static final String CACHE_NAME_LIVE = "liveCache";
    // marketing
    public static final String CACHE_NAME_MARKET = "marketCache";
    // show
    public static final String CACHE_NAME_SHOW = "showCache";
    // user
    public static final String CACHE_NAME_USER = "userCache";
    // live_web
    public static final String CACHE_NAME_LIVE_WEB = "liveWebCache";
    // phone_web
    public static final String CACHE_NAME_PHONE_WEB = "phoneWebCache";
    // shop_console
    public static final String CACHE_NAME_SHOP_CONSOLE = "shopConsoleCache";
    // msg_hanlder
    public static final String CACHE_NAME_MSG = "msgCache";
    // app_api
    public static final String CACHE_NAME_APP = "appApi";
    public static final String CACHE_NAME_APP_BUFFER = "bufferCache";
    public static final String CACHE_NAME_APP_AUTH = "authCache";
    // console
    public static final String CACHE_NAME_CONSOLE = "consoleCache";
    // 登陆
    public static final String CACHE_NAME_LOGIN="loginCache";

    //一天内连续登陆错误次数
    public static final String LOGIN_CONSOLE_ERROR_COUNT = "LOGIN_CONSOLE_ERROR_COUNT_";
    //一天内同一IP连续登陆错误次数
    public static final String LOGIN_CONSOLE_ERROR_IP_COUNT = "LOGIN_CONSOLE_ERROR_IP_COUNT_";

    // privilege console
    public static final String CACHE_NAME_PRIVILEGE_CONSOLE = "priConsoleCache";
    // 登陆
    public static final String CACHE_NAME_PRIVILEGE_LOGIN="priLoginCache";

    //一天内连续登陆错误次数
    public static final String PRIVILEGE_LOGIN_CONSOLE_ERROR_COUNT = "PRIVILEGE_LOGIN_CONSOLE_ERROR_COUNT_";
    //一天内同一IP连续登陆错误次数
    public static final String PRIVILEGE_LOGIN_CONSOLE_ERROR_IP_COUNT = "PRIVILEGE_LOGIN_CONSOLE_ERROR_IP_COUNT_";

    //用户登录的系统ID
    public static final String PRIVILEGE_USER_SYSTEM_ID = "PRIVILEGE_USER_SYSTEM_ID_";

    // privilege shop console
    public static final String CACHE_NAME_PRIVILEGE_CONSOLE_SHOP = "priShopConsoleCache";
    // 登陆
    public static final String CACHE_NAME_PRIVILEGE_LOGIN_SHOP="priShopLoginCache";

    //一天内连续登陆错误次数
    public static final String PRIVILEGE_LOGIN_CONSOLE_ERROR_COUNT_SHOP = "PRIVILEGE_LOGIN_CONSOLE_ERROR_COUNT_SHOP_";
    //一天内同一IP连续登陆错误次数
    public static final String PRIVILEGE_LOGIN_CONSOLE_ERROR_IP_COUNT_SHOP = "PRIVILEGE_LOGIN_CONSOLE_ERROR_IP_COUNT_SHOP_";

    /*************************************** 缓存域 end**************************************************************************************/


    /***************************************
     * 缓存时间 start
     ***********************************************************************************/
    // app_api缓存时间
    public static final int APP_CACHE_TIME = 5 * 60;

    // asl缓存时间
    public static final int ASL_CACHE_TIME = 60 * 60;

    /*************************************** 缓存时间 end*************************************************************************************/


    /**
     *	缓存key
     */

    /***************************************
     * 周边项目  start
     ************************************************************************************/

    //周边视频
    public static final String SUPPORT_VIDEO = "support_video_";

    // 项目评分,+projectId:项目平均分；+projectId+'_'+userId:用户对项目评分，永久存储
    public static final String SUPPORT_PROJECT_SCORE = "support_project_score_";

    /*************************************** 周边项目 end**************************************************************************************/


    /***************************************
     * 机构项目 start
     *************************************************************************************/

    //机构登陆账户,+id/organId / +account
    public static final String ORGAN_ACCOUNT = "organ_account_";

    //机构信息账户
    public static final String ORGAN_INFO_APP = "ORGAN_INFO_APP_";
    
    //机构信息账户vo
    public static final String ORGAN_INFO_VO = "ORGAN_INFO_VO_";
    
    // 机构错误登录次数
    public static final String ORGAN_LOGIN_ERROR_COUNT = "ORGAN_LOGIN_ERROR_COUNT_";

    /*************************************** 机构项目 end**************************************************************************************/


    /***************************************
     * 商城项目 start
     **************************************************************************************/

    // 商城场次售票状态
    public static final String SHOP_SESSION_TICKET_STATUS = "shop_session_ticket_status_";

    // 商品分类列表
    public static final String SHOP_CATEGORY_LIST = "shop_category_list_";

    // 商品分类列表
    public static final String SHOP_SKU_BASE_LIST = "SHOP_SKU_BASE_list_";

    // 商品
    public static final String SHOP_PRODUCT = "shop_product_";

    // 商品备注信息(+productId)
    public static final String SHOP_PRODUCT_NOTE = "shop_product_note_";

    // 订单库存验证缓存
    public static final String SHOP_ORDER_VALIDATE = "SHOP_ORDER_VALIDATE_";

    // 订单详情(+orderId/orderNo)
    public static final String SHOP_ORDER = "SHOP_ORDER_";

    // 订单的红包信息（+orderNo）
    public static final String SHOP_REDPACKET = "SHOP_REDPACKET_";

    // productId查询商品列表（+productId）
    public static final String SHOP_PRODUCT_LIST = "SHOP_PRODUCT_LIST_";

    /*************************************** 商城项目 enb**************************************************************************************/


    /***************************************
     * live项目 start
     **************************************************************************************/

    // 今日 liveshow
    public static final String LIVE_FIRSTLIVE = "live_firstlive_";

    // liveshow(+sessionId)
    public static final String LIVE_LIVESHOW = "LIVE_LIVESHOW_";

    // SimpleSession
    public static final String LIVE_SIMPLESESSION = "LIVE_SIMPLESESSION_";

    // user packet 用户碎片包
    public static final String LIVE_USER_PACKET = "LIVE_USER_PACKET_";

    /*************************************** live项目 enb**************************************************************************************/


    /***************************************
     * marketing项目 start
     **************************************************************************************/

    // CashAccount(+userId)
    public static final String MARKET_CASH_ACCOUNT = "MARKET_CASH_ACCOUNT_";

    // pointAccount(+userId)
    public static final String MARKET_POINT_ACCOUNT = "MARKET_POINT_ACCOUNT_";

    // 平台参数(+key)
    public static final String MARKET_PLAT_PROPERTIES = "MARKET_PLAT_PROPERTIES_";

    // 分享设置数据（+objectId_type）
    public static final String MARKET_M_SHARE = "MARKET_M_SHARE_";


    /*************************************** marketing项目 enb**************************************************************************************/


    /***************************************
     * show项目 start
     **************************************************************************************/

    // 演出场次(+sessionId)
    public static final String SHOW_SESSION = "SHOW_SESSION_";

    // 演出项目(+projectId)
    public static final String SHOW_PROJECT = "SHOW_PROJECT_";

    // 自营订单(+sessionId)
    public static final String SHOW_ORDER = "SHOW_ORDER_";

    //经纬度、城市名
    public static final String SHOW_REGIONLIST = "SHOW_REGIONLIST_";

    //经纬度
    public static final String SHOW_LATLNG = "SHOW_LATLNG_";

    // 封面信息(+projectId)
    public static final String SHOW_POSTER = "SHOW_POSTER_";

    // 场馆列表
    public static final String SHOW_STADIUM_LIST = "SHOW_STADIUM_LIST_";
    
    // 出票模板
    public static final String SHOW_TICKET_TEMPLATE = "SHOW_TICKET_TEMPLATE_";


    /*************************************** show项目 enb**************************************************************************************/


    /***************************************
     * user项目 start
     **************************************************************************************/

    // 用户收货地址
    public static final String USER_ADDRESS = "USER_ADDRESS_";
    // 用户默认地址
    public static final String USER_ADDRESS_DEFAULT = "USER_ADDRESS_DEFAULT_";
    // 地址-省列表
    public static final String USER_AREA_PROVINCE = "USER_AREA_PROVINCE_";
    // 地址-code查询下一级
    public static final String USER_AREA_NEXT_LEVEL = "USER_AREA_NEXT_LEVEL_";
    // 地址(+code)
    public static final String USER_AREA = "USER_AREA_";
    // 用户关注项目(+projectId)
    public static final String USER_PROJECT_FOLLOW_NUM = "PROJECT_FOLLOW_NUM_";
    // 用户关注艺人(+artistId)
    public static final String USER_ARTIST_FOLLOW_NUM = "USER_ARTIST_FOLLOW_NUM_";

    /*************************************** user项目 enb**************************************************************************************/


    /***************************************
     * liveWeb项目 start
     **************************************************************************************/

    // 封面图
    public static final String LIVEWEB_COVER_GAUSS_CACHE = "COVER_GAUSS_CACHE_KEY_LIVEWEB_";

    // simplesession
    public static final String LIVEWEB_SIMPLE_SESSION = "LIVEWEB_SIMPLE_SESSION_";

    // 场次与用户的交互统计数据
    public static final String LIVEWEB_SESSIONNUMS = "LIVEWEB_SESSIONNUMS_";

    // 项目与用户的交互统计数据
    public static final String LIVEWEB_PROJECTNUMS = "LIVEWEB_PROJECTNUMS_";

    /*************************************** liveWeb项目 enb**************************************************************************************/


    /***************************************
     * msg_hanlder项目 start
     **************************************************************************************/
    // 项目与用户的交互统计数据
    public static final String MSG_VALIDATE = "MSG_VALIDATE_";

    /*************************************** msg_hanlder项目 enb**************************************************************************************/


    /***************************************
     * app_api项目 start
     **************************************************************************************/

    // 订单预处理信息
    public static final String APP_SHOP_ORDER_PRE = "APP_SHOP_ORDER_PRE_";

    // app地图页面的统计数据分页
    public static final String APP_LIVE_REGIONMAP_PAGE = "APP_LIVE_REGIONMAP_PAGE_";

    // app地图页面的场次坐标列表
    public static final String APP_SHOW_MAPLOCATION_LIST = "APP_SHOW_MAPLOCATION_LIST_";

    // app获取场馆（+sessionId）
    public static final String APP_SHOW_STADIUM = "APP_SHOW_STADIUM_";

    // SimpleSession（+sessionId）
    public static final String APP_SHOW_SIMPLESESSION = "APP_SHOW_SIMPLESESSION_";

    // session base
    public static final String APP_SHOW_BASE_SESSION = "APP_SHOW_BASE_SESSION1_";

    // video page
    public static final String APP_SS_VIDEO_PAGE = "APP_SS_VIDEO_PAGE_";

    // 场次与用户的交互统计数据（+sessionId）
    public static final String APP_USER_SESSIONNUMS = "APP_USER_SESSIONNUMS_";

    // 获取用户点过赞的场次id列表
    public static final String APP_USER_PRAISES = "APP_USER_PRAISES_";

    // 缓存的文件信息
    public static final String APP_UPLOADFILE = "APP_UPLOADFILE_";

    // 用户信息
    public static final String APP_USER_BUFFER = "APP_USER_BUFFER_";

    // 上传回调地址
    public static final String APP_OSS_CALL_BACK = "APP_OSS_CALL_BACK_";
    
    // 资讯分享参数
    public static final String APP_MSHARE = "APP_MSHARE_";

    /*************************************** app_api项目 enb**************************************************************************************/
   
    
    
    /*************************************** phone_web项目 start**************************************************************************************/
    // 微信的api_token
    public static final String PHONE_WX_API_TOKEN = "PHONE_WX_API_TOKEN_";
    // 微信的Jsapi_Ticket
    public static final String PHONE_WX_JSAPI_TICKET = "PHONE_WX_JSAPI_TICKET_";
    // 红包分享的mShare
    public static final String PHONE_REDPACKET_MSHARE = "PHONE_REDPACKET_MSHARE_";
    
    /*************************************** phone_web项目 enb**************************************************************************************/
}
