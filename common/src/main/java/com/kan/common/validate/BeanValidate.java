/**
 * 
 */
package com.kan.common.validate;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class BeanValidate {

	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private static Validator validator = factory.getValidator();

	public static void validate(Object object) {
		Set<ConstraintViolation<Object>> constraintViolations = validator
				.validate(object);
		for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
			throw new ValidateException(constraintViolation.getMessage() + " ["
					+ constraintViolation.getPropertyPath() + "]");
		}

	}

	public static String validateToMsg(Object object) {
		Set<ConstraintViolation<Object>> constraintViolations = validator
				.validate(object);

		for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
			return constraintViolation.getMessage();
		}

		return "";
	}
}
