package com.kan.common.buffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by zxw on 2016/4/22.
 */
public class BlockingQueueDataBuffer<T> implements DataBuffer<T> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private ArrayBlockingQueue<T> queue;
    private ExecutorService es;
    private ExecutorService handleExec;
    private Timer timer;
    private DataBufferHandler<T> handler;

    private int batchNum = 500;
    private int batchInterval = 2000;

    private int handleNum = 1;

    private AtomicInteger count = new AtomicInteger(0);
    private AtomicBoolean exec = new AtomicBoolean(false);

    private boolean isClosed = false;

    private ReadWriteLock closingLock = new ReentrantReadWriteLock();

    public BlockingQueueDataBuffer(int batchNum, int batchInterval, int handleNum){
        this.batchNum = batchNum;
        this.batchInterval = batchInterval;
        this.handleNum = handleNum;
    }

    public BlockingQueueDataBuffer(int batchNum){
        this.batchNum = batchNum;
    }

    public BlockingQueueDataBuffer(){}

    @Override
    public void put(T obj) {
        closingLock.readLock().lock();
        try {
            if (isClosed) {
                logger.warn("data buffer closed");
                return;
            }
            try {
                queue.put(obj);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //判断size大于bufferSize执行批处理
            if (queue.size() >= batchNum) {
                if (!exec.get()) {
                    exec.set(true);
                    es.execute(new Runnable() {
                        @Override
                        public void run() {
                            batch();
                        }
                    });
                }
            }
        }finally {
            closingLock.readLock().unlock();
        }
    }

    public void batch(){
        T t = null;
        final List<T> list = new ArrayList<>();
        while((t = queue.poll()) != null){
            list.add(t);
        }
        exec.set(false);
        if(list.size() == 0){
            return;
        }
        count.incrementAndGet();
        handleExec.execute(new Runnable() {
            @Override
            public void run() {
                handler.handle(list);
                count.decrementAndGet();
            }
        });
    }

    @Override
    public void setHandler(DataBufferHandler<T> handler) {
        this.handler = handler;
    }

    @Override
    public void open() {
        queue = new ArrayBlockingQueue<>(batchNum*3);
        es = Executors.newSingleThreadExecutor();
        handleExec = Executors.newFixedThreadPool(handleNum);
        timer = new Timer("blocking queue data buffer");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                batch();
            }
        }, batchInterval, batchInterval);
    }

    @Override
    public void close(boolean await) {
        closingLock.writeLock().lock();
        try {
            isClosed = true;
            if (es != null) {
                es.shutdown();
            }
            if (timer != null) {
                timer.cancel();
            }
            batch();
            if (await) {
                while (count.get() != 0) {
                    logger.info("data buffer queue has " + count.get() + " items.");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (handleExec != null) {
                handleExec.shutdown();
            }
        }finally {
            closingLock.writeLock().unlock();
        }
    }

    @Override
    public void close() {
        close(true);
    }
}
