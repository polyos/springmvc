package com.kan.common.buffer;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by zxw on 2016/4/22.
 */
public class Test {

    public static void main(String [] args){
        DataBuffer<String> dataBuffer = new BlockingQueueDataBuffer<>();
        final List<Integer> l = new Vector<>();
        dataBuffer.setHandler(new DataBufferHandler<String>() {
            @Override
            public void handle(List<String> list) {
                int i = list.size();
                l.add(i);

            }
        });
        dataBuffer.open();
        long ll = System.currentTimeMillis();
        for(int i=0; i< 10000; i++){
            dataBuffer.put(""+i);
        }
        dataBuffer.close(true);
        System.out.println("-----------------------------" + (System.currentTimeMillis() - ll));
        int m = 0;
        for(int i : l){
            System.out.println(i);
            m = m + i;
        }
        System.out.println(m);
    }

}
