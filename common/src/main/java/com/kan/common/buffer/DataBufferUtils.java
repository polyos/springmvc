package com.kan.common.buffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by zxw on 2016/4/22.
 */
public class DataBufferUtils<T> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private int bufferSize = 50;
    private int maxBufferSize = 5000;
    private Timer timer;
    private ArrayBlockingQueue<T> queue = new ArrayBlockingQueue<>(maxBufferSize);
    private ExecutorService es = Executors.newFixedThreadPool(1);

    public void put(T obj){
        try {
            queue.put(obj);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //判断size大于bufferSize执行批处理
        if(queue.size() >= bufferSize){
            es.execute(new Runnable() {
                @Override
                public void run() {
                    batch();
                }
            });
        }
    }

    public void batch(){
        T t = null;
        while((t = queue.poll()) != null){
            System.out.println(t.toString());
        }
    }

    public static void main(String [] args){
        DataBufferUtils<String> d = new DataBufferUtils<>();
        for(int i=0; i<100000; i++) {
            d.put(""+i);
        }
    }

}
