package com.kan.common.buffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 数据缓冲
 * Created by zxw on 2016/4/22.
 */
public interface DataBuffer<T> {

    /**
     * 添加数据
     * @param obj
     */
    public void put(T obj);

    /**
     * 设置批量处理对象
     * @param handler
     */
    public void setHandler(DataBufferHandler<T> handler);

    /**
     * 打开缓冲区
     */
    public void open();

    /**
     * 关闭缓冲区 是否等待结束
     */
    public void close(boolean await);

    /**
     * 关闭缓冲区 等待结束
     */
    public void close();


}
