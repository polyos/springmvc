package com.kan.common.buffer;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by zxw on 2016/4/22.
 */
public class Test1 {

    public static void main(String [] args) throws InterruptedException {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(1000);
        for(int i=0; i<10000; i++){
            System.out.println(i);
            queue.put("" + i);
        }
    }

}
