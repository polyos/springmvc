package com.kan.common.buffer;

import com.kan.common.SpringContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by zxw on 2016/4/22.
 */
public class RedisDataBuffer<T extends Serializable> implements DataBuffer<T> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Class entityClass;

    private StringRedisTemplate stringRedisTemplate;
    private ExecutorService es;
    private ExecutorService handleExec;
    private Timer timer;
    private DataBufferHandler<T> handler;

    private int batchNum = 500;
    private int batchInterval = 2000;

    private int handleNum = 1;

    private AtomicInteger count = new AtomicInteger(0);
    private AtomicBoolean exec = new AtomicBoolean(false);

    private static final String prefix = "data:buffer:";
    private String redisKey;

    private boolean isClosed = false;

    private ReadWriteLock closingLock = new ReentrantReadWriteLock();

    public RedisDataBuffer(String redisKey, int batchNum, int batchInterval, int handleNum){
        this.batchNum = batchNum;
        this.batchInterval = batchInterval;
        this.handleNum = handleNum;
        this.redisKey = redisKey;
    }

    public RedisDataBuffer(int batchNum){
        this.batchNum = batchNum;
    }

    public RedisDataBuffer(String redisKey){
        this.redisKey = redisKey;
    }

    private RedisDataBuffer(){}

    @Override
    public void put(final T obj) {
        closingLock.readLock().lock();
        try {
            if (isClosed) {
                logger.warn("data buffer closed");
                return;
            }
            stringRedisTemplate.execute(new RedisCallback<Object>() {
                @Override
                public Object doInRedis(RedisConnection connection) throws DataAccessException {
                    try {
                        byte[] key = (prefix + redisKey).getBytes();
                        connection.lPush(key, serialize(obj));
                        long len = connection.lLen(key);
                        //判断size大于bufferSize执行批处理
                        if (len >= batchNum) {
                            if (!exec.get()) {
                                exec.set(true);
                                es.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        batch();
                                    }
                                });
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
        }finally {
            closingLock.readLock().unlock();
        }
    }

    public void batch(){
        T t = null;
        final List<T> list = new ArrayList<>();
        stringRedisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                byte[] key = (prefix + redisKey).getBytes();
                byte[] bytes = null;
                while ((bytes = connection.rPop(key)) != null) {
                    try {
                        T t = deserialize(bytes);
                        list.add(t);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        });
        exec.set(false);
        if(list.size() == 0){
            return;
        }
        count.incrementAndGet();
        handleExec.execute(new Runnable() {
            @Override
            public void run() {
                handler.handle(list);
                count.decrementAndGet();
            }
        });
    }

    @Override
    public void setHandler(DataBufferHandler<T> handler) {
        this.handler = handler;
    }

    @Override
    public void open() {
        stringRedisTemplate = SpringContext.getBean(StringRedisTemplate.class);
        es = Executors.newSingleThreadExecutor();
        handleExec = Executors.newFixedThreadPool(handleNum);
        timer = new Timer("blocking queue data buffer");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                batch();
            }
        }, batchInterval, batchInterval);
    }

    @Override
    public void close(boolean await) {
        closingLock.writeLock().lock();
        try {
            isClosed = true;
            if (es != null) {
                es.shutdown();
            }
            if (timer != null) {
                timer.cancel();
            }
            batch();
            if (await) {
                while (count.get() != 0) {
                    logger.info("data buffer queue has " + count.get() + " items.");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (handleExec != null) {
                handleExec.shutdown();
            }
        }finally {
            closingLock.writeLock().unlock();
        }
    }

    @Override
    public void close() {
        close(true);
    }

    private byte[] serialize(T t) throws IOException {
        if(entityClass == null){
            entityClass = t.getClass();
        }
        if(t == null){
            return null;
        }
        Class<?> type = t.getClass();
        if (type == Boolean.TYPE || type == Boolean.class) {        // boolean
            return new byte[]{(Boolean)t ? (byte)1 : (byte)0};
        } else if (type == Character.TYPE || type == Character.class) { // char
            ByteBuffer buffer = ByteBuffer.allocate(2);
            buffer.putChar((Character)t);
            return buffer.array();
        } else if (type == Byte.TYPE || type == Byte.class) {    // byte
            return new byte[]{(Byte)t};
        } else if (type == Short.TYPE || type == Short.class) {   // short
            ByteBuffer buffer = ByteBuffer.allocate(2);
            buffer.putShort((Short)t);
            return buffer.array();
        } else if (type == Integer.TYPE || type == Integer.class) { // int
            ByteBuffer buffer = ByteBuffer.allocate(4);
            buffer.putInt((Integer)t);
            return buffer.array();
        } else if (type == Long.TYPE || type == Long.class) {    // long
            ByteBuffer buffer = ByteBuffer.allocate(8);
            buffer.putLong((Long)t);
            return buffer.array();
        } else if (type == Float.TYPE || type == Float.class) {   // float
            ByteBuffer buffer = ByteBuffer.allocate(4);
            buffer.putFloat((Float)t);
            return buffer.array();
        } else if (type == Double.TYPE || type == Double.class) {  // double
            ByteBuffer buffer = ByteBuffer.allocate(8);
            buffer.putDouble((Double)t);
            return buffer.array();
        }else if(type == String.class){
            return ((String)t).getBytes(Charset.forName("UTF-8"));
        }else{
            return javaSerialize(t);
        }
    }

    private T deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        if(bytes == null){
            return null;
        }
        Class<?> type = entityClass;
        if (type == Boolean.TYPE || type == Boolean.class) {        // boolean
            return (T)(bytes[0] == 1 ? Boolean.TRUE : Boolean.FALSE);
        } else if (type == Character.TYPE || type == Character.class) { // char
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Character.valueOf(buffer.getChar());
        } else if (type == Byte.TYPE || type == Byte.class) {    // byte
            return (T)Byte.valueOf(bytes[0]);
        } else if (type == Short.TYPE || type == Short.class) {   // short
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Short.valueOf(buffer.getShort());
        } else if (type == Integer.TYPE || type == Integer.class) { // int
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Integer.valueOf(buffer.getInt());
        } else if (type == Long.TYPE || type == Long.class) {    // long
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Long.valueOf(buffer.getLong());
        } else if (type == Float.TYPE || type == Float.class) {   // float
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Float.valueOf(buffer.getFloat());
        } else if (type == Double.TYPE || type == Double.class) {  // double
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            return (T)Double.valueOf(buffer.getDouble());
        }else if(type == String.class){
            return (T)new String(bytes, Charset.forName("UTF-8"));
        }else{
            return (T)javaDeserialize(bytes);
        }
    }

    private byte[] javaSerialize(Serializable obj) throws IOException {
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            return baos.toByteArray();
        } finally {
            if(oos != null){
                oos.close();
            }
            if(baos != null){
                baos.close();
            }
        }
    }

    private Object javaDeserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = null;
        ObjectInputStream ois = null;
        try {
            bais = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bais);
            return ois.readObject();
        } finally {
            if(ois != null){
                ois.close();
            }
            if(bais != null){
                bais.close();
            }
        }
    }
}
