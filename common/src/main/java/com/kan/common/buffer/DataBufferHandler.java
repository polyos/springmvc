package com.kan.common.buffer;

import java.util.List;

/**
 * Created by zxw on 2016/4/22.
 */
public interface DataBufferHandler<T> {

    public void handle(List<T> list);

}
