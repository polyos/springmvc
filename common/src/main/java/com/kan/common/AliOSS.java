package com.kan.common;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.*;
import com.kan.common.utils.Configuration;
import com.kan.common.utils.DateUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ZXW on 2015/3/19.
 */
public class AliOSS {

    private static String endpoint;
    private static String accessKeyId = "AUK6e4YTjLwegfT6";
    private static String accessKeySecret = "mhxunU3rcj3NBEgaEi5aZ6zZnLqNyl";
    private static String[] buckets;
    private static String[] urlMaps;
    private static boolean isInit = false;

    private static String mediaDomain = null;

    public static String generateKey() {
        String date = DateUtils.formatDate(new Date(), "yyyyMMdd");
        String key = UUID.randomUUID().toString();
        return date + "/" + key;
    }

    public static String getBucket(String key) throws IOException {
        checkInit();
        int position = Math.abs(key.hashCode()) % buckets.length;
        return buckets[position];
    }

    public static String getHostUrl(String key) throws IOException {
        checkInit();
        int position = Math.abs(key.hashCode()) % buckets.length;
        return urlMaps[position];
    }

    public static String getEndPoint() throws IOException {
        checkInit();
        return endpoint;
    }

    public static String getFileName(String key, String fileName) {
        String ext = null;
        if (fileName != null) {
            int index = fileName.lastIndexOf(".");
            if (index > 0) {
                ext = fileName.substring(index + 1);
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(key).append("_").append(buckets.length).append(".").append(ext);
        return sb.toString();
    }

    public static String[] upload(String fileName, byte[] file) throws IOException {
        checkInit();
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String ext = null;
        if (fileName != null) {
            int index = fileName.lastIndexOf(".");
            if (index > 0) {
                ext = fileName.substring(index + 1);
            }
        }
        try {
            StringBuilder sb = new StringBuilder();
            String key = generateKey();
            int position = Math.abs(key.hashCode()) % buckets.length;
            sb.append(key).append("_").append(buckets.length).append(".").append(ext);
            client.putObject(buckets[position], sb.toString(), new ByteArrayInputStream(file));
            return new String[]{urlMaps[position] + sb.toString(), buckets[position]};
        } finally {
            client.shutdown();
        }
    }

    public static String[] upload(String fileName, String filePath) throws IOException {
        checkInit();
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String ext = null;
        if (fileName != null) {
            int index = fileName.lastIndexOf(".");
            if (index > 0) {
                ext = fileName.substring(index + 1);
            }
        }
        try {
            StringBuilder sb = new StringBuilder();
            String key = generateKey();
            int position = Math.abs(key.hashCode()) % buckets.length;
            sb.append(key).append("_").append(buckets.length).append(".").append(ext);
            client.putObject(buckets[position], sb.toString(), new File(filePath));
            return new String[]{urlMaps[position] + sb.toString(), buckets[position]};
        } finally {
            client.shutdown();
        }
    }

    public static String[] upload(String fileName, long fileSize, InputStream inputStream) throws IOException {
        checkInit();
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        String ext = null;
        if (fileName != null) {
            int index = fileName.lastIndexOf(".");
            if (index > 0) {
                ext = fileName.substring(index + 1);
            }
        }
        try {
            StringBuilder sb = new StringBuilder();
            String key = generateKey();
            int position = Math.abs(key.hashCode()) % buckets.length;
            ObjectMetadata om = new ObjectMetadata();
            om.setContentLength(fileSize);
            sb.append(key).append("_").append(buckets.length).append(".").append(ext);
            client.putObject(buckets[position], sb.toString(), inputStream, om);
            return new String[]{urlMaps[position] + sb.toString(), buckets[position]};
        } finally {
            client.shutdown();
        }
    }

    public static byte[] download(String fileId) throws IOException {
        checkInit();
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            for (int i = 0; i < buckets.length; i++) {
                if (fileId.contains(urlMaps[i])) {
                    String key = fileId.replaceAll(urlMaps[i], "");
                    OSSObject obj = client.getObject(buckets[i], key);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    IOUtils.copyLarge(obj.getObjectContent(), baos);
                    return baos.toByteArray();
                }
            }
            OSSObject obj = client.getObject(new URL(fullUrl(fileId)), new HashMap<String, String>());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copyLarge(obj.getObjectContent(), baos);
            return baos.toByteArray();
            //return FastDFS.download(fileId);
        } finally {
            client.shutdown();
        }
    }

    public static void delete(String fileId) throws IOException {
        checkInit();
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            boolean isOSS = false;
            for (int i = 0; i < buckets.length; i++) {
                if (fileId.contains(urlMaps[i])) {
                    String key = fileId.replaceAll(urlMaps[i], "");
                    client.deleteObject(buckets[i], key);
                    isOSS = true;
                    break;
                }
            }
            if (!isOSS) {
                //FastDFS.delete(fileId);
                client.deleteObject("hk-0-4", fullUrl(fileId));
            }
        } finally {
            client.shutdown();
        }
    }

    private static void checkInit() throws IOException {
        if (!isInit) {
            if (endpoint == null) {
                endpoint = Configuration.getProperty("oss.endpoint");
            }
            if (buckets == null || urlMaps == null) {
                String bucketsStr = Configuration.getProperty("oss.buckets");
                String urlMapsStr = Configuration.getProperty("oss.url.maps");
                if (bucketsStr != null && urlMapsStr != null) {
                    buckets = bucketsStr.split(",");
                    urlMaps = urlMapsStr.split(",");
                }
            }
            if (endpoint == null) {
                throw new IOException("请在配置文件中添加oss.endpoint参数");
            }
            if (buckets == null || urlMaps == null) {
                throw new IOException("请在配置文件中添加oss.buckets和oss.url.maps参数");
            }
            if (buckets.length != urlMaps.length) {
                throw new IOException("oss.buckets和oss.url.maps参数以逗号分隔，数量保持一致");
            }
            isInit = true;
        }
    }

    public static String fullUrl(String url) {
        if (mediaDomain == null) {
            mediaDomain = Configuration.getProperty("media.domain");
        }
        if (mediaDomain == null) {
            return url;
        }
        if (null == url || url.trim().length() == 0) {
            return "";
        }
        if (url.startsWith("http://") || url.startsWith("https://")) {
            return url;
        }
        return mediaDomain + url;
    }

    public static void main(String[] args) throws IOException {
        // endpoint以杭州为例，其它region请按实际情况填写
        String endpoint = "oss-cn-beijing.aliyuncs.com";
// accessKey请登录https://ak-console.aliyun.com/#/查看
        String accessKeyId = "AUK6e4YTjLwegfT6";
        String accessKeySecret = "mhxunU3rcj3NBEgaEi5aZ6zZnLqNyl";
        String bucketName = "cgeel-1-1";
// 您的回调服务器地址，如http://oss-demo.aliyuncs.com或http://127.0.0.1:9090
        String callbackUrl = "http://zxw0208.vicp.cc:17177/api/upload_callback";

// 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        String content = "Hello OSS";
        String key = AliOSS.generateKey();
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key + ".txt",
                new ByteArrayInputStream(content.getBytes()));

// 上传回调参数
        Callback callback = new Callback();
        callback.setCallbackUrl(callbackUrl);
        callback.setCallbackHost("oss-cn-hangzhou.aliyuncs.com");
        Map<String, String> map = new HashMap<>();
        map.put("mimeType", "image/jpg");
        map.put("size", "123");
        callback.setCallbackBody("mimeType=image/jpg&size=123");
        callback.setCalbackBodyType(Callback.CalbackBodyType.URL);
        putObjectRequest.setCallback(callback);

        PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);

// 读取上传回调返回的消息内容
        byte[] buffer = new byte[1024];
        putObjectResult.getCallbackResponseBody().read(buffer);
// 一定要close，否则会造成连接资源泄漏
        putObjectResult.getCallbackResponseBody().close();

// 关闭client
        ossClient.shutdown();
    }
}
